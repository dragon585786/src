import React, { Component } from 'react';
import 'typeface-roboto';

//components
import Landing from '../src/Components/layouts/Landing';
import Footer from '../src/Components/layouts/Footer';
import Navbar from '../src/Components/layouts/Navbar';
import Register from './Components/auth/Register/Register';
import Login from './Components/auth/Login/Login';
import resetPassword from './Components/auth/Login/resetPassword';


//router
import {
  BrowserRouter as Router, Route,
  Link,
  Redirect,
  withRouter
} from 'react-router-dom';


import './App.css';




const fakeAuth = {
  isAuthenticated: true,
  authenticate(cb) {
    this.isAuthenticated = true
    setTimeout(cb, 100) // fake async
  },
  signout(cb) {
    this.isAuthenticated = false
    setTimeout(cb, 100) // fake async
  }
}



const PrivateRoute = ({ component: Component, ...rest }) => (

  <Route {...rest} render={(props) => (
    fakeAuth.isAuthenticated === true
      ? <Component {...props} />
      : <Redirect to='/' />
  )} />
)

console.log(PrivateRoute,'hello');





class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          {/* <Navbar /> */}
          {/* <Route exact path="/" component={Landing}  /> */}
          <PrivateRoute path='/dashboard' component={Landing} />
          {/* <div className="container" > */}
          <Route exact path="/Register" component={Register} />
          <Route exact path="/" component={Login} />
          <Route exact path="/resetPassword" component={resetPassword} />

          {/* </div> */}
          <Footer />
        </div>
      </Router>

    );
  }
}

export default App;
