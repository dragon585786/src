import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';


import routes from '../landingComponents/Routes/Routes'
import { MenuList, MenuItem } from '@material-ui/core';
import { Link, withRouter } from 'react-router-dom';
import Users from '../landingComponents/Users/Users';
import RawMaterial from '../landingComponents/Rawmaterials/Rawmaterials';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';



const drawerWidth = 300;

const styles = theme => ({

  fab: {
    width: '38px',
    height: '30px'
  },
  rootsearch: {
    padding: '4px 26px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  item: {
    marginTop: '10px',
    marginBottom: '5px',
    padding: '4px 26px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },

  firstsearch: {
    margin: '30px'

  },
  searchinput: {
    width: '100%'
  },
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  active: {
    backgroundColor: theme.palette.action.selected
  }
});

class Landing extends React.Component {
  state = {
    open: true,
    Customroute: Users,
    searchgreeting: 'Users',
    customProp: {
      searchvalue: '',
      createNew: false,
      reset:'',
    }

  };


  handleDrawerOpen = () => {

    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleChange = event => {
    console.log('eventtt', event.target.value)
    let customProp = Object.assign({}, this.state.customProp);
    customProp['searchvalue'] = event.target.value
    this.setState({
      customProp
    });
  };


  addNew = () => {

    let customProp = Object.assign({}, this.state.customProp);
    customProp['createNew'] = !this.state.customProp.createNew
    this.setState({
      customProp
    });
  }


  setCustomRoute = (data) => {
    // console.log('cust', data)
    this.setState({
      Customroute: data.component,
      searchgreeting: data.navbarName
    });
  }

  // activeRoute = (routeName) => {
  //     console.log('in active route', routeName)

  //     return this.props.location.pathname.indexOf(routeName) > -1 ? true : false;
  // }



  render() {
    const { classes, theme } = this.props;
    const { open, Customroute, searchgreeting, customProp } = this.state
    console.log(this.state. customProp['createNew'],'addnew parent')

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: open,
          })}
          >
            <Toolbar disableGutters={!open}>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={this.handleDrawerOpen}
                className={classNames(classes.menuButton, open && classes.hide)}
                >
                  <MenuIcon />
                </IconButton>
                <Typography variant="h6" color="inherit" noWrap>
                  Mangalam
                </Typography>
              </Toolbar>
            </AppBar>
            <Drawer
              className={classes.drawer}
              variant="persistent"
              anchor="left"
              open={open}
              classes={{
                paper: classes.drawerPaper,
              }}
              >
                <div className={classes.drawerHeader}>
                  <IconButton onClick={this.handleDrawerClose}>
                    {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                  </IconButton>
                </div>
                <MenuList>
                  {routes.map((prop, key) => {
                    return (
                      <MenuItem key={key} onClick={() => this.setCustomRoute(prop)}  >
                        <ListItemIcon   >
                          <prop.icon />
                        </ListItemIcon>
                        <ListItemText  primary={prop.sidebarName} />
                      </MenuItem>
                    );
                  })}
                </MenuList>
              </Drawer>
              <main
                className={classNames(classes.content, {
                  [classes.contentShift]: open,
                })}
                >
                  <div className={classes.drawerHeader} />

                  <Grid container>

                                  <Grid item lg={10} md={9} sm={8} xs={7}>


                  <Paper className={classes.rootsearch} elevation={1}>
                    <InputBase value={customProp.searchvalue} name='search' onChange={this.handleChange} className={classes.searchinput} placeholder={'Search ' + searchgreeting} />
                    <IconButton className={classes.iconButton} aria-label="Search">
                      <SearchIcon />
                    </IconButton>
                    <Fab color="primary" aria-label="Add"  className={classes.fab} onClick={() => this.addNew()} >
                      <AddIcon onClick={() => this.addNew()}  />
                    </Fab>

                  </Paper>
                  </Grid>
                                      <Grid item lg={2} md={3} sm={4} xs={5}></Grid>
                                    </Grid>


                  <Router>
                    {/* <Route component={customroute} /> */}
                    <Route render={() => <Customroute searchresult={customProp} />} />
                  </Router>
                </main>
              </div>
            );
          }
        }

        Landing.propTypes = {
          classes: PropTypes.object.isRequired,
          theme: PropTypes.object.isRequired,
        };

        export default withStyles(styles, { withTheme: true })(Landing);
