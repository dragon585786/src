import React, { Component } from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import '../Dishes/Dishes.css'
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
// import { ServerAddr } from '../../../NetworkConfig'
import { serverip } from '../../../NetworkConfig'

import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import { ServerAddr } from '../../../NetworkConfig'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Modal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import DialogActions from '@material-ui/core/DialogActions';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';

import SvgMaterialIcons from '../Material-ui/DeleteIcon';
// import Select from 'react-select';
// import React, { Component } from 'react'
import Select from 'react-select'
// import Select from '@material-ui/core/Select';
// import { colourOptions } from '../data';

import UploadButton from '../UploadButton/UploadButton'
import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';



// >>>>>>> modal done


const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  icon: {
    margin: theme.spacing(2),
    float: 'right',
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
  },
});

const rows = [
  createData('Admin@gmail.com', 9922334455, 'A1239939', '08/29/2019', 'Admin'),
  createData('Manager@gmail.com', 8836363524, 'jfj82783I', '08/29/2019', 'Manager'),
  createData('Employee@gmail.com', 7782736454, 'jfj82783I', '08/29/2019', 'Employee'),
];



function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const access = window.localStorage.getItem('access');


const options = [
  { value: 1, label: 'Veg' },
  { value: 2, label: 'Non-Veg' },
  { value: 3, label: 'Eggeterian' },
  { value: 4, label: 'Jain' },
];

const options1 = [
  { value: 1, label: 'Soups' },
  { value: 2, label: 'Fresh Juice,Shakes & Mocktail' },
  { value: 3, label: 'Floating Snacks' },
  { value: 4, label: 'Starter Sweets' },
  { value: 5, label: 'Bengali Sweets' },
  { value: 6, label: 'Pure Ghee Sweets' },
  { value: 7, label: 'Vegetables' },
  { value: 8, label: ' Other Vegetables' },
  { value: 9, label: 'Roti & Parantha' },
  { value: 10, label: 'Raita' },
  { value: 11, label: 'Salads' },
  { value: 12, label: 'Ice Cream' },
  { value: 13, label: 'Snacks & Namkeen' },
  { value: 14, label: 'Live Counters' },
];


const options2 = [
  { value: 1, label: '' },
  { value: 2, label: 'Test' },
  { value: 3, label: 'Indian' },
  { value: 4, label: 'Czech' },
  { value: 5, label: 'Indian Sweets' },
  { value: 6, label: 'Chinese' },
];








class Dishes extends Component {

  state = {



    colourOptions: [],
    items: [],
    showLoader: false,
    expanded: null,

    categories: [],
    idURL: '',
    open8: false,
    mapIngredient: [],

    dishCategory: [],
    finalIngredient: [],

    readCuisines: [],
    cusineCategory: [],
    typeCategory: [],
    seasonCategory: [
      { 'season': 'All Season' },
      { 'season': 'Summer' },
      { 'season': 'Rainy' },
      { 'season': 'Winter' },
      { 'season': 'Spring' },
    ],
    newCategory: '',
    selected: '',

    units: [],
    expiries: [],
    searchValue: this.props.searchresult.searchvalue,
    addNew: this.props.searchresult.createNew,
    open: false,
    dish_type_list: [],
    newType: '',
    open1: false,
    open2: false,
    open3: false,
    open4: false,
    open5: false,
    open6: false,
    open7: false,




    open11: false,
    open4: false,
    cusines: [],
    cusine_name: '',
    newCusiness: '',
    newVendor: '',

    deleteId: '',
    max: 5,
    pages: 1,
    Ingredient: [],
    newServed_in: '',
    newServed: '',



    checkedItems: new Map(),
    editItem: {
      'name': '',
      'cost': '',
      'served': '',
      'served_in': '',
      'vendor': '',
      'type': '',
      'category': '',
      'cuisine': '',
      'season': '',
      'Ingredient': '',
      // 'newCusiness': '',

    },
    served_inCategory: [],
    servedCategory: [],
    vendorCategory: [],


    admin: true,
    manager: true,
    employee: true,

    checkedB: true,
    checkedA: true,
    weightRange: '',
    selectedOption: null,
    selectedOption1: null,
    selectedOption2: null,
  }
  componentDidMount() {
    this.readDish();
    this.readDishCategory();
    this.readTypeCategory();
    this.setState({ addNew: false });
    // this.addCusine();
    this.readServed_in();
    this.readServed();
    this.readVendor();

    this.readCuisine();
    // this.handleChange5();
    this.handleChange6();
    this.Ingredient();

  }

  handleChange6() {

    this.setState({
      checkedB: false
    });
  }

  handleChange5() {

    this.setState({
      checkedB: !this.state.checkedB,
    });
  }

  handleChange = panel => (event, expanded) => {
    console.log('event', event);
    console.log('expanded', expanded);
    console.log('panel', panel);


    this.setState({
      expanded: expanded ? panel : false,
    });
  };
  setInputValue = (data) => {
    console.log(data.dishName, 'abe')
    var input = {
      "dish_name": data.dishName,
      "cusine_name": data.cuisine.name,
      "type_dish": data.typeDish.id,
      "cuisine": data.cuisine.id,
      "category": data.category.id,
      "served": "hot",
      "serve_in": "bowl",
      "cost": 100,
      "vendor_available": true,
    }



    this.setState({
      editItem: input,
      deleteId: data.dishId
    });
  };

  editInputValue = event => {
    console.log('in edit input ', event);
    console.log('in edit inputdfd ', event.target.name, event.target.value);


    let editItem = this.state.editItem;
    const Key = event.target.name;
    const value = event.target.value;


    if (Key == 'newType') {

      this.setState({
        newType: event.target.value,
      });
      return
    }
    if (Key == 'newServed_in') {
      this.setState({
        newServed_in: event.target.value,
      })
    }
    if (Key == 'newServed') {
      this.setState({
        newServed: event.target.value,
      })
    }
    if (Key == 'newCusiness') {
      console.log('exfhgfgp')
      this.setState({
        newCusiness: event.target.value,
      });
      return
    }
    if (Key == 'newCategory') {

      this.setState({
        newCategory: event.target.value,
      });
      return
    }
    if (Key == 'newVendor') {

      this.setState({
        newVendor: event.target.value,
      });
      return
    }
    editItem[event.target.name] = event.target.value;
    this.setState({
      editItem
    });

    console.log('state', this.state.editItem)

  }

  handleChangekk(event) {
    const { maparr4 } = this.state;
    const Key = event.target.name;
    const value = event.target.value;
    console.log('val123', event.target.value, event.target.name)

    // if (Key == '') {
    //   console.log('val123',event.target.value)
    //   this.setState({
    //     itemid:event.target.value,
    //   });
    //   console.log('val123',event.target.value,this.state.itemid)
    //   return
    // }
    if (Key == 'mealtype') {
      const mealtype2 = this.state;
      this.setState({
        mealtype: event.target.value,
        mealtype2: event.target.value
      }, () => {
        console.log('log1', this.state.mealtype2)
        // this.setState({mealtype2})
      })


      return;
    }








    if (Key == 'cat') {

      this.setState({
        cat: event.target.value,
      });
      console.log('catval', this.state.cat)
      return;
    }






    if (Key == 'noDishes') {

      this.setState({
        noDishes: event.target.value,

      });
      console.log('DISH C', this.state.noDishes)
      return
    }

    if (Key == 'packagename') {

      this.setState({
        packagename: event.target.value,
      });
      return;
    }




  }

  handleId(event) {
    let object = JSON.parse(event.target.value);
    console.log(object.name, 'names');
    this.setState({
      [event.target.value]: object.id,
      // [event.target.value]:object.name
    });
    console.log(object.id, 'object');


    this.setState({
      maparr4: object.id,
    }, () => {
      console.log('mapdata2', this.state.maparr4);
    });

    this.setState({
      map123: object.name
    }, () => {
      console.log('map41', this.state.map123);
    });


  }





  deleteItem(pro, rows) {
    console.log(rows.value, rows.label, 'rows')

    console.log(pro, 'id')
    let e = [];
    console.log(this.state.mapIngredient, 'mapingredient')

    e = this.state.mapIngredient.splice(pro, 1);
    console.log(e, 'eeee')
    //here is adding part of coloroptions 
    let newl = { value: rows.value, label: rows.label }
    let datal = [...this.state.colourOptions, newl]
    this.setState({
      colourOptions: datal
    })
    console.log(this.state.colourOptions, 'color')


    console.log(this.state.mapIngredient, 'mapingredient aftere eee')

    this.setState({
      mapIngredient: this.state.mapIngredient
    })
    console.log(this.state.mapIngredient, 'mapingredient')









    // const { list, maparr, getmeal, maparr2, maparr4, mapar, sendarr } = this.state;
    // list.splice(i, 1);
    // console.log(list, 'list')
    // this.setState({
    //   list,
    //   maparr: list,
    //   maparr4: this.state.maparr4,
    // }, () => {
    //   console.log(maparr, 'list2')
    //   console.log(maparr4, 'list3');
    // });
    // maparr.map((item) => {
    //   return (
    //     this.setState({
    //       mapar: [...this.state.mapar, item.id]
    //     }, () => {
    //       console.log(this.state.mapar, 'map54')
    //     })
    //   )
    // });

    // this.delpdc(this.state.sendarr);
  }







  editInputValue1 = event => {
    console.log('in edit input ', event);

    let editItem = this.state.editItem;
    const Key = event.target.name;
    const value = event.target.value;
    if (Key == 'newCategory') {

      this.setState({
        newCategory: event.target.value,
      });
      return
    }

    if (Key == 'newType') {

      this.setState({
        newType: event.target.value,
      });
      return
    }

    if (Key == 'newcusines') {

      this.setState({
        newcusines: event.target.value,
      });
      return
    }

    if (Key == 'duration_term') {
      editItem['expiry'][event.target.name] = event.target.value;
      this.setState({
        editItem
      });
      return
    }

    editItem[event.target.name] = event.target.value;
    this.setState({
      editItem
    });

    console.log('state', this.state.editItem)

  }

  componentWillReceiveProps(nextProps) {
    // console.log('componentWillReceiveProps', nextProps);
    if (nextProps.searchresult.createNew) {
      this.setState({ addNew: nextProps.searchresult.createNew })

    }
    else {
      this.setState({ searchValue: nextProps.searchresult.searchvalue, })
      // this.getRawmaterials();
    }

  }







  postItem() {

    this.state.mapIngredient.map(option => {
      console.log(option.value, 'posttttststt')
      this.setState({
        finalIngredient: this.state.finalIngredient.push(option.value)
      })

    })
    console.log(this.state.finalIngredient, 'final waaalaaa')

    // var formData = new FormData();
    // formData.append('active', this.state.checkedB);
    // formData.append('name', this.state.editItem['name']);
    // // formData.append('cost', this.state.editItem['cost']);
    // formData.append('season', this.state.editItem['season']);
    // formData.append('cuisine', this.state.editItem['cuisine']);
    // formData.append('dish_type', this.state.editItem['type']);
    // formData.append('dish_category', this.state.editItem['category']);
    // formData.append('served', this.state.editItem['served']);
    // formData.append('served_in', this.state.editItem['served_in']);
    // formData.append('vendor', this.state.editItem['vendor']);
    // formData.append('ingredient', this.state.finalIngredient);
    var data = {
      'active': this.state.checkedB,
      'name': this.state.editItem['name'],
      'season': this.state.editItem['season'],
      'dish_type': this.state.editItem['type'],
      'dish_category': this.state.editItem['category'],
      'served': this.state.editItem['served'],
      'served_in': this.state.editItem['served_in'],
      'vendor': this.state.editItem['vendor'],
      'ingredient': this.state.finalIngredient,
      'cuisine': this.state.editItem['cuisine'],

    }


    // console.log('runbhuaa',formData);



    console.log(data, 'read formdata')

    // web/create-update-uniform-image/
    // console.log(...formData, 'formdata')
    // http://34.93.7.13:8002/media/cdn/upload/image/

    defaultServerAddr.post(`/dishes/create/dish/`, data, {
      headers: {
        'Content-Type': "application/json;",
        // Accept: "multipart/",


        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/media/cdn/upload/image/', res)
        if (res) {
          this.readDish();
          // console.log(...formData, 'read formdata')



        } else {
          alert(res.data.validation);
        }
      })

  }

  readDish() {
    defaultServerAddr.get(`/dishes/read/dish/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('imagedswaa', res)
        if (res) {
          console.log('web/read-setup-images/', res);
          this.setState({
            items: res.data['data']
          })

        } else {
          alert(res.data.validation);
        }
      })
  }
  readDishCategory() {
    var datas = {};
    defaultServerAddr.get(`${serverip}/dishes/read/meal/category/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('/dishes/read/meal/category/', res)
        if (res) {
          console.log('http://34.93.7.13:8002/read/expiry-dropdown/', res);
          this.setState({
            dishCategory: res.data['data']
          })
          console.log(this.state.dishCategory, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
  }
  Ingredient() {
    var datas = {};
    console.log(this.state.Ingredient, 'suewrterp')

    defaultServerAddr.get(`/ingredient/read/ingredient/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log(res, 'suewrterp')

        if (res) {
          // console.log('http://34.93.7.13:8002/read/expiry-dropdown/', res);
          this.setState({
            Ingredient: res.data['data']
          })
          this.selectIngredient();

          console.log(this.state.Ingredient, 'suewrterp')
        } else {
          alert(res.data.validation);
        }
      })
  }



  readTypeCategory() {
    var datas = {};
    defaultServerAddr.get(`/dishes/read/dishtype/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`

      }
    })
      .then(res => {
        console.log('exp', res)
        if (res) {
          console.log('http://34.93.7.13:8002/read/expiry-dropdown/', res);
          this.setState({
            typeCategory: res.data['data']
          })
          console.log(this.state.typeCategory, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
  }


  readCuisine() {
    // var datas = {};
    defaultServerAddr.get(`/dishes/read/cuisine/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('extyrtp', res)
        if (res) {
          console.log('http://34.ffg93.edrfdf7.13:800/read-cuisine/', res);
          this.setState({
            readCuisines: res.data['data']
          })
          console.log(this.state.readCuisines, 'swrterup')
        } else {
          alert(res.data.validation);
        }
      })
  }







  onOpenModal = () => {
    this.setState({ open: true });
    console.log(this.state.open, 'open')
  };
  onOpenModal3 = () => {
    this.setState({ open3: true });
    console.log(this.state.open, 'open')
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };
  onCloseModal3 = () => {
    this.setState({ open3: false });
  };

  onOpenModal1 = () => {
    this.setState({ open1: true });
    console.log(this.state.open1, 'open')
  };

  onOpenModal4 = () => {
    this.setState({ open4: true });
    console.log(this.state.open4, 'open')
  };
  onOpenModal5 = () => {
    this.setState({ open5: true });
    console.log(this.state.open5, 'open')
  };

  onCloseModal1 = () => {
    this.setState({ open1: false });
  };
  onCloseModal5 = () => {
    this.setState({ open5: false });
  };
  onCloseModal8 = () => {
    this.setState({ open8: false });
  };

  onOpenModal2 = () => {
    this.setState({ open2: true });
    console.log(this.state.open2, 'open')
  };
  onOpenModal8 = (rec) => {
    this.setState({
      open8: true,
      idURL: rec
    });
    console.log(this.state.open2, 'open')
  };
  onOpenModal7 = () => {
    this.setState({ open7: true });
    console.log(this.state.open2, 'open')
  };
  onOpenModal6 = () => {
    this.setState({ open6: true });
    console.log(this.state.open6, 'open')
  };

  onCloseModal2 = () => {
    this.setState({ open2: false });
  };
  onCloseModal6 = () => {
    this.setState({ open6: false });
  };
  onCloseModal7 = () => {
    this.setState({ open7: false });
  };



  handleClose = () => {

    this.setState({ open: false });
  }
  handleClose8 = () => {

    this.setState({ open8: false });
  }
  handleClose3 = () => {
    this.setState({ open3: false });
  }

  handleClose1 = () => {
    this.setState({ open1: false });
  }

  handleClose2 = () => {
    this.setState({ open2: false });
  }
  handleClose5 = () => {
    this.setState({ open5: false });
  }
  handleClose7 = () => {
    this.setState({ open7: false });
  }
  handleClose6 = () => {
    this.setState({ open6: false });
  }
  handleChanges = event => {
    // console.log('salimhan',event.target.value);
    // const uniqueNames = Array.from(new Set(this.state.arr));
    const uniqueNames = Array.from(new Set(this.state.arr));


    console.log('salimhan', event.target.name);

    // if(event.target.name === 'Admin@gmail.com'){
    //   this.setState({
    //     admin: !this.state.admin,
    //     arr: this.state.arr.concat('Admin'),
    //   });
    //   console.log('khan',this.state.admin);
    //   console.log('khan',this.state.arr);
    //   console.log('khann',uniqueNames);



    // }
    // if(this.state.admin === false){
    //   // var people = ["Bob", "Sally", "Jack"]
    //   // var toRemove = 'Admin';
    //   if(uniqueNames){
    //     var index = uniqueNames.indexOf("Admin");
    //     if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
    //       uniqueNames.splice(index, 1);
    //     }
    //     this.setState({
    //       uniqueNames: uniqueNames,
    //     });
    //   }

    //   console.log('bla bla',this.state.uniqueNames);
    //   return true;
    // }
    // if(event.target.name === 'Manager@gmail.com'){
    //   this.setState({
    //     manager: !this.state.manager,
    //     arr: this.state.arr.concat('Manager'),
    //   });
    //   console.log('khan',this.state.manager);
    //   console.log('khan',this.state.arr);
    // }
    // if(this.state.manager === false){
    //   // var people = ["Bob", "Sally", "Jack"]
    //   // var toRemove = 'Manager';
    //   if(uniqueNames){
    //     var index = uniqueNames.indexOf("Manager");
    //   if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
    //     uniqueNames.splice(index, 1);
    //   }
    //   this.setState({
    //     uniqueNames: uniqueNames,
    //   });

    //   }
    //   console.log('bla bla',this.state.uniqueNames);
    //   return true;


    // }

    // if(event.target.name === 'Employee@gmail.com'){
    //   this.setState({
    //     employee: !this.state.employee,
    //     arr: this.state.arr.concat('Employee'),
    //   });
    //   console.log('khan',this.state.employee);
    //   console.log('khan',this.state.arr);
    // }
    // if(this.state.employee === false){
    //   // var people = ["Bob", "Sally", "Jack"]
    //   // var toRemove = ;
    //   if(uniqueNames){
    //     var index = uniqueNames.indexOf("Employee");
    //   if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
    //     uniqueNames.splice(index, 1);
    //     this.setState({
    //       uniqueNames: uniqueNames,
    //     });
    //   }
    //   }
    //   console.log('bla bla',this.state.uniqueNames);
    //   return true;


    // }

  };

  deletett = () => {
    console.log('delete', this.state.uniqueNames);
    console.log('delete click hu raha hai');

  }


  onOpenModal11 = () => {
    this.setState({ open11: true });
    console.log(this.state.open, 'open')
  };

  onCloseModal11 = () => {
    this.setState({ open11: false });
  };


  addCategory() {
    console.log(this.state.category, 'find')
    const datas = {
      // "categoryDishId": null,
      "name": this.state.newCategory,
    }

    defaultServerAddr.post('/dishes/create/meal/category/', datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('ewewxp', res)
        if (res) {
          // console.log('http://34.93.7.13:8002/read/expiry-dropdown/', res);
          this.setState({
            newCategory: res.data['data']

          })
          this.readDishCategory();
          console.log(this.state.newcategorydata, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.newcategorydata, 'sup')
    this.setState({ open: false, });


  }



  addType() {
    console.log(this.state.newType, 'fgdf')
    const datas = {
      "name": this.state.newType,
    }

    console.log(datas, 'fgdf')

    defaultServerAddr.post(`/dishes/create/dishtype/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('expsfdsf', res)
        if (res) {
          // console.log('create-update-dish-type/', res);
          // this.setState({
          //   newcategorydata: res.data['data']

          // })
          this.readTypeCategory();
          // this.state.editItem['type_dish'] = this.state.newType;
          // this.setState({ editItem: this.state.editItem })
          // console.log(this.state.newcategorydata, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.newcategorydata, 'sup')
    this.setState({ open1: false, });


  }
  addServed_in() {
    console.log('exfhgfgp', this.state.newCusiness)

    const datas = {
      // "cuisineId": null,
      "name": this.state.newServed_in,
    }

    defaultServerAddr.post('/dishes/create/servedin/', datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('exfhgfgp', res)
        if (res) {
          // console.log('create-update-dish-type/', res);
          // this.setState({
          //   newcategorydata: res.data['data']

          // })
          this.readServed_in();
          // this.state.editItem['cusine'] = this.state.newType;
          // this.setState({ editItem: this.state.editItem })
          // console.log(this.state.newcategorydata, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.newcategorydata, 'sup')
    this.setState({ open5: false, });

  }
  readServed_in() {
    defaultServerAddr.get(`/dishes/read/servedin/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('extyrtp', res)
        if (res) {
          console.log('http://34.ffg93.edrfdf7.13:800/read-cuisine/', res);
          this.setState({
            served_inCategory: res.data['data']
          })

          console.log(this.state.readCuisines, 'swrterup')
        } else {
          alert(res.data.validation);
        }
      })
  }
  addServed() {
    console.log('exfhgfgp', this.state.newCusiness)

    const datas = {
      // "cuisineId": null,
      "name": this.state.newServed,
    }

    defaultServerAddr.post('/dishes/create/served/', datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('exfhgfgp', res)
        if (res) {
          // console.log('create-update-dish-type/', res);
          // this.setState({
          //   newcategorydata: res.data['data']

          // })
          this.readServed();
          // this.state.editItem['cusine'] = this.state.newType;
          // this.setState({ editItem: this.state.editItem })
          // console.log(this.state.newcategorydata, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.newcategorydata, 'sup')
    this.setState({ open6: false, });

  }
  readServed() {
    defaultServerAddr.get(`/dishes/read/served/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('extyrtp', res)
        if (res) {
          console.log('http://34.ffg93.edrfdf7.13:800/read-cuisine/', res);
          this.setState({
            servedCategory: res.data['data']
          })

          console.log(this.state.readCuisines, 'swrterup')
        } else {
          alert(res.data.validation);
        }
      })
  }

  clearFunction() {
    this.setState({ addNew: false });
  }


  getDishes() {


    defaultServerAddr.get('/dishes/read/dish/', {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('ex</Grid>dsfp', res)
        if (res) {
          // console.log('http://34.93.7.13:8002/read-all-dish/', res);
          this.setState({
            items: res['data']['data']
          })
          console.log(this.state.items, 'supss')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.items, 'itemss')

  }


  addCusine() {
    const datas = {

    }

    defaultServerAddr.post('http://34.93.7.13:8002/dishes/read/cuisine/', datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('exp', res)
        if (res) {
          // console.log('http://34.93.7.13:8002/read/expiry-dropdown/', res);
          this.setState({
            cusineCategory: res.data['data']
          })
          console.log(this.state.cuisine, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
  }
  addVendor() {
    const datas = {
      'name': this.state.newVendor
    }

    defaultServerAddr.post('/dishes/create/vendor/', datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('exp', res)
        if (res) {
          this.setState({
            open7: false
          })
          this.readVendor();

        } else {
          alert(res.data.validation);
        }
      })
  }
  readVendor() {
    defaultServerAddr.get(`/dishes/read/vendor/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('extyrtp', res)
        if (res) {
          console.log('http://34.ffg93.edrfdf7.13:800/read-cuisine/', res);
          this.setState({
            vendorCategory: res.data['data']
          })

          console.log(this.state.readCuisines, 'swrterup')
        } else {
          alert(res.data.validation);
        }
      })
  }




  readExpiryDropdown() {
    var datas;
    defaultServerAddr.post('http://34.93.7.13:8002/dishes/read/cuisine/', datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('exp', res)
        if (res) {
          // console.log('http://34.93.7.13:8002/read/expiry-dropdown/', res);
          this.setState({
            cusines: res.data['data']
          })
          console.log(this.state.expiries, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.expiries, 'sup')
  }


  addCusines() {
    console.log('exfhgfgp', this.state.newCusiness)

    const datas = {
      // "cuisineId": null,
      "name": this.state.newCusiness,
    }

    defaultServerAddr.post('/dishes/create/cuisine/', datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('exfhgfgp', res)
        if (res) {
          // console.log('create-update-dish-type/', res);
          // this.setState({
          //   newcategorydata: res.data['data']

          // })
          this.readCuisine();
          // this.state.editItem['cusine'] = this.state.newType;
          // this.setState({ editItem: this.state.editItem })
          // console.log(this.state.newcategorydata, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.newcategorydata, 'sup')
    this.setState({ open2: false, });

  }

  deleteDish(id) {
    console.log(this.state.deleteId, 'public')
    const datas = {
      "dishId": this.state.deleteId,

    }

    defaultServerAddr.post('http://34.93.7.13:8002/dishes/modify/dish/id/', datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('exp', res)
        if (res) {
          // console.log('http://34.93.7.13:8002/read/expiry-dropdown/', res);
          this.setState({
            newcategorydata: res.data['data']
          })
          this.getDishes();
          console.log(this.state.newcategorydata, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.newcategorydata, 'sup')
    this.setState({ open: false, });

    this.setState({ open11: false });
  }

  buttnext() {
    //   console.log('salimm');
    if (this.state.page === 10) {
      console.log('next end here');
      return true;
    }
    else {

      console.log('next plus one');
      this.setState({
        pages: this.state.pages + 1
      });
    };
  }

  buttprevious() {
    //   console.log('salimm');

    if (this.state.pages === 1) {
      console.log('previous button end here');

      return true;
    }
    else {
      console.log('previous minus one hua');
      this.setState({
        pages: this.state.pages - 1
      });
    }

  }


  handleChange11 = selectedOption => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption.value);

  };

  handleChange12 = selectedOption1 => {
    this.setState({ selectedOption1 });
    console.log(`Option selected:`, selectedOption1.value);

  };


  handleChange13 = selectedOption2 => {
    this.setState({ selectedOption2 });
    console.log(`Option selected:`, selectedOption2.value);

  };
  selectIngredient() {
    console.log(this.state.colourOptions, this.state.Ingredient, 'jhdad')

    this.state.Ingredient.map(pro => {
      let newl = { value: pro.id, label: pro.name }
      let datal = [...this.state.colourOptions, newl]
      this.setState({
        colourOptions: datal
      })
      console.log(this.state.colourOptions, this.state.Ingredient, 'jhdad')
    })
  }
  selectIngredientChange = (event) => {
    // event.preventDefault();
    // console.log("event.target", event.value,event.label);
    // this.setState({
    //   mapIngredient: []
    // })

    if (event) {
      let e = []
      e = event;
      console.log('adfsd', e)

      // this.state.mapIngredient = null;
      e.map(pro => {
        // console.log('adfsd',pro.value)
        console.log(this.state.mapIngredient, 'adfsd')

        // const items = ['a', 'b', 'c', 'd', 'e', 'f']
        // const valueToRemove = pro.value
        // const filteredItems = this.state.mapIngredient.filter(item => item !== valueToRemove)
        // this.setState({
        //   mapIngredient:filteredItems
        // })


        //  this.setState({ 
        //   mapIngredient: this.state.mapIngredient.concat([pro.value])
        // })

        // this.setState({
        //   mapIngredient: [...this.state.mapIngredient, pro.value]
        // })


        let newl = { label: pro.label, value: pro.value }
        let datal = [...this.state.mapIngredient, newl]
        this.setState({
          mapIngredient: datal
        })
      })

      // this.setState({
      //   selectIngredientChanges: [event]
      // })
      // const e = event;

      // this.setState({ 
      //   selectIngredientChanges: this.state.selectIngredientChanges.concat(['defds'])
      // })

      // this.setState({
      //   selectIngredientChanges: [...this.state.selectIngredientChanges, e]
      // })
    }

  }


  getData() {
    this.getDishes();
    this.readDishCategory();
    this.readTypeCategory();
    this.setState({ addNew: false });
    this.addCusine();
    this.handleChange5();
    this.handleChange6();
    this.Ingredient();
  }




  render() {
    const { selectedOption, selectedOption1, selectedOption2 } = this.state;
    const classes = this.state;
    const { open } = this.state;
    const { open1 } = this.state;
    const { open2 } = this.state;
    const { open3 } = this.state;
    const { open5 } = this.state;
    const { open6 } = this.state;
    const { open7 } = this.state;
    const { open8 } = this.state;

    // const { categories, units, expiries, editItem, expanded, list } = this.state;

    // const colourOptions = [
    //   { value: 'ocean', label: 'Ocean', color: '#00B8D9', isFixed: true },
    //   { value: 'blue', label: 'Blue', color: '#0052CC', isDisabled: true },
    //   { value: 'purple', label: 'Purple', color: '#5243AA' },
    //   { value: 'red', label: 'Red', color: '#FF5630', isFixed: true },
    //   { value: 'orange', label: 'Orange', color: '#FF8B00' },
    //   { value: 'yellow', label: 'Yellow', color: '#FFC400' },
    //   { value: 'green', label: 'Green', color: '#36B37E' },
    //   { value: 'forest', label: 'Forest', color: '#00875A' },
    //   { value: 'slate', label: 'Slate', color: '#253858' },
    //   { value: 'silver', label: 'Silver', color: '#666666' },
    // ];





    const colourOptions = this.state.colourOptions;
    const { open11 } = this.state;
    const { items, checkedA, checkedB } = this.state;
    // let label = [];

    const { categories, units, expiries, editItem, expanded, list } = this.state
    if (this.state.showLoader) {
      return (
        < CircularProgress className='loader' />
      )
    }

    var addnew;
    if (this.state.addNew) {
      addnew =
        <Paper style={{ marginTop: '15px', marginBottom: '10px', padding: '10px' }} >
          <Grid container spacing={16}  >

            <Grid item xs={3}>

              <TextField
                label="Dish name"
                name="name"
                value={editItem.name}
                onChange={this.editInputValue}

              />
            </Grid>

            <Grid item xs={3}>
              <TextField
                style={{ width: '90%' }}
                id="type_dish"
                select
                label="Dish Type"
                name='type'
                value={editItem.type}
                onChange={this.editInputValue}
              >

                <MenuItem onClick={() => { this.onOpenModal1() }} >
                  <span onClick={this.onOpenModal1}><span style={{ fontSize: '18px' }}>Create Type</span>   &nbsp;&nbsp;
                              <Icon className={classes.icon} color="primary">
                      add_circle
                              </Icon>
                  </span>


                </MenuItem>
                {this.state.typeCategory.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            {/*onClose={this.onCloseModal}*/}
            <Modal open={open1} onClose={this.onCloseModal1} center>
              <div className={classes.root}>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Type"
                        name="newType"
                        value={this.state.newType}
                        onChange={this.editInputValue}

                      />

                      <DialogActions>
                        <Button onClick={this.handleClose1} color="primary">
                          Cancel
                                  </Button>
                        <Button onClick={() => { this.addType() }} color="primary">
                          Create
                                  </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal>



            <Grid item xs={3}>
              <TextField
                style={{ width: '90%' }}
                id="category"
                select
                label="Category"
                name='category'
                value={editItem.category}
                onChange={this.editInputValue}
              >

                <MenuItem onClick={() => { this.onOpenModal() }} >
                  <span onClick={this.onOpenModal}><span style={{ fontSize: '18px' }}>Create category</span>   &nbsp;&nbsp;
                              <Icon className={classes.icon} color="primary">
                      add_circle
                              </Icon>
                  </span>


                </MenuItem>
                {this.state.dishCategory.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            {/*onClose={this.onCloseModal}*/}
            <Modal open={open} onClose={this.onCloseModal} center>
              <div className={classes.root}>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Category"
                        name="newCategory"
                        value={this.state.newCategory}
                        onChange={this.editInputValue}

                      />

                      <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                          Cancel
                                  </Button>
                        <Button onClick={() => { this.addCategory() }} color="primary">
                          Create
                                  </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal>

            <Grid item xs={3}>
              <TextField
                style={{ width: '90%' }}
                id="cuisine Type"
                select
                label="Cuisine"
                name='cuisine'
                value={editItem.cuisine}
                onChange={this.editInputValue}
              >

                <MenuItem onClick={() => { this.onOpenModal2() }} >
                  <span onClick={this.onOpenModal2}><span style={{ fontSize: '18px' }}>Create Cuisine</span>   &nbsp;&nbsp;
                              <Icon className={classes.icon} color="primary">
                      add_circle
                              </Icon>
                  </span>


                </MenuItem>
                {this.state.readCuisines.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            {/*onClose={this.onCloseModal}*/}
            <Modal open={open2} onClose={this.onCloseModal2} center>
              <div className={classes.root}>
                <Grid container spacing={16} >
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Cuisine"
                        name="newCusiness"
                        value={this.state.newCusiness}
                        onChange={this.editInputValue}

                      />



                      <DialogActions>
                        <Button onClick={this.handleClose2} color="primary">
                          Cancel
                                  </Button>
                        <Button onClick={() => { this.addCusines() }} color="primary">
                          Create
                                  </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal>



            {/* <Grid item xs={3}>

              <TextField
                label="Dish cost"
                name="cost"
                value={editItem.cost}
                onChange={this.editInputValue}

              />
            </Grid> */}
            <Grid item xs={3}>
              <TextField
                style={{ width: '90%' }}
                id="seoson Type"
                select
                label="Season"
                name='season'
                value={editItem.season}
                onChange={this.editInputValue}
              >

                {/* <MenuItem onClick={() => { this.onOpenModal2() }} >
                  <span onClick={this.onOpenModal2}><span style={{ fontSize: '18px' }}>Create Cuisine</span>   &nbsp;&nbsp;
                              <Icon className={classes.icon} color="primary">
                      add_circle
                              </Icon>
                  </span>


                </MenuItem> */}
                {this.state.seasonCategory.map((option, id) => (
                  <MenuItem key={id} value={option.season}>
                    {option.season}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            {/*onClose={this.onCloseModal}*/}
            {/* <Modal open={open2} onClose={this.onCloseModal2} center>
              <div className={classes.root}>
                <Grid container spacing={16} style={{ width: '400px', height: '100%' }}>
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New season"
                        name="newcusines"
                        value={this.state.newcusines}
                        onChange={this.editInputValue}

                      />



                      <DialogActions>
                        <Button onClick={this.handleClose2} color="primary">
                          Cancel
                                  </Button>
                        <Button onClick={() => { this.addCusines() }} color="primary">
                          Create
                                  </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal> */}

            <Grid item xs={3}>

              <TextField


                style={{ width: '90%' }}
                id="served Type"
                select
                label="Served"
                name='served'
                value={editItem.served}
                onChange={this.editInputValue}
              >
                <MenuItem onClick={() => { this.onOpenModal6() }} >
                  <span onClick={this.onOpenModal6}><span style={{ fontSize: '18px' }}>Create category</span>   &nbsp;&nbsp;
              <Icon className={classes.icon} color="primary">
                      add_circle
              </Icon>
                  </span>


                </MenuItem>
                {this.state.servedCategory.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            {/*onClose={this.onCloseModal}*/}
            <Modal open={open6} onClose={this.onCloseModal6} center>
              <div className={classes.root}>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Category"
                        name="newServed"
                        value={this.state.newServed}
                        onChange={this.editInputValue}

                      />

                      <DialogActions>
                        <Button onClick={this.handleClose6} color="primary">
                          Cancel
                  </Button>
                        <Button onClick={() => { this.addServed() }} color="primary">
                          Create
                  </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal>


            <Grid item xs={3}>

              <TextField


                style={{ width: '90%' }}
                id="served_in Type"
                select
                label="Served_in"
                name='served_in'
                value={editItem.served_in}
                onChange={this.editInputValue}
              >
                <MenuItem onClick={() => { this.onOpenModal5() }} >
                  <span onClick={this.onOpenModal5}><span style={{ fontSize: '18px' }}>Create category</span>   &nbsp;&nbsp;
                              <Icon className={classes.icon} color="primary">
                      add_circle
                              </Icon>
                  </span>


                </MenuItem>
                {this.state.served_inCategory.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            {/*onClose={this.onCloseModal}*/}
            <Modal open={open5} onClose={this.onCloseModal5} center>
              <div className={classes.root}>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Category"
                        name="newServed_in"
                        value={this.state.newServed_in}
                        onChange={this.editInputValue}

                      />

                      <DialogActions>
                        <Button onClick={this.handleClose5} color="primary">
                          Cancel
                                  </Button>
                        <Button onClick={() => { this.addServed_in() }} color="primary">
                          Create
                                  </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal>


            <Grid item xs={3}>
              <TextField
                style={{ width: '90%' }}
                id="category"
                select
                label="Dish Vendor"
                name='vendor'
                value={editItem.vendor}
                onChange={this.editInputValue}
              >

                <MenuItem onClick={() => { this.onOpenModal7() }} >
                  <span onClick={this.onOpenModal7}><span style={{ fontSize: '18px' }}>Create Vendor</span>   &nbsp;&nbsp;
                              <Icon className={classes.icon} color="primary">
                      add_circle
                              </Icon>
                  </span>


                </MenuItem>
                {this.state.vendorCategory.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            {/*onClose={this.onCloseModal}*/}
            <Modal open={open7} onClose={this.onCloseModal7} center>
              <div className={classes.root}>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Category"
                        name="newVendor"
                        value={this.state.newVendor}
                        onChange={this.editInputValue}

                      />

                      <DialogActions>
                        <Button onClick={this.handleClose7} color="primary">
                          Cancel
                                  </Button>
                        <Button onClick={() => { this.addVendor() }} color="primary">
                          Create
                                  </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal>
            <Grid item xs={9}>
              {/* <TextField
                style={{ width: '90%' }}
                id="cuisine Type"
                select
                label="Ingredient"
                name='Ingredient'
                value={editItem.Ingredient}
                onChange={this.editInputValue}
              > */}

              {/* <MenuItem 
                // onClick={() => { this.onOpenModal3() }} 
                >
                  <span ><span style={{ fontSize: '18px' }}>Create Ingredient</span>   &nbsp;&nbsp;
                              <Icon className={classes.icon} color="primary">
                      add_circle
                              </Icon>
                  </span>


                </MenuItem> */}
              {/* {this.state.Ingredient.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))} */}
              {/* </TextField> */}



              {/* <Grid item lg={5} md={5} sm={8} xs={12} style={{marginTop:'20px'}}> */}
              {/* <TextField
                style={{ width: '90%' }}

                // className="TextFieldWidth"
                id="Meal type"
                select
                label="Ingredient"
                name='cat'
                value={this.state.cat}
                onChange={event => { this.handleChangekk(event); this.handleId(event) }} */}



              <Select
                // defaultValue={[colourOptions[2], colourOptions[3]]}
                isMulti
                name="colors"
                options={colourOptions}
                className="basic-multi-select"
                classNamePrefix="select"
                value={this.state.selected}
                onChange={this.selectIngredientChange}
              />





              {/* <select className="select"
                name="colors"
                className="basic-multi-select"
                classNamePrefix="select"
                isMulti
                value={editItem.Ingredient}
              // onChange={this.createProjectChange}
              
                options={
                  this.state.Ingredient.map((owner, index) =>
                 {
                    <option value={owner.id}>{owner.name}</option>
                 })
                
                >
              </select> */}


              {/* > */}

              {/* <MenuItem  onClick={()=>{this.onOpenModal2()}} >
                                  <span onClick={this.onOpenModal2}><span >Create Category</span>   &nbsp;&nbsp;
                                  <Icon className={classes.icon} color="primary">
                                    add_circle
                                  </Icon>
                                </span>


                              </MenuItem> */}

              {/* 
                {this.state.Ingredient.map(option => (
                  <MenuItem key={option.id} value={JSON.stringify(option)} onClick={(event) => this.handleId(event)} >
                    {option.name}
                  </MenuItem>
                ))} */}



              {/* </TextField> */}
              {/* </Grid> */}
              {/* <Select options={options} /> */}

            </Grid>



            {/* <Grid item lg={2} md={2} sm={12} xs={12}>
              <Button onClick={(event) => this.createlist(event)} color="secondary" variant="contained" >ADD </Button>
            </Grid> */}




            {/*onClose={this.onCloseModal}*/}
            {/* <Modal open={open3} onClose={this.onCloseModal3} center>
              <div className={classes.root}>
                <Grid container spacing={16} >
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Cuisine"
                        name="newcusines"
                        value={this.state.newcusines}
                        onChange={this.editInputValue}

                      />



                      <DialogActions>
                        <Button onClick={this.handleClose3} color="primary">
                          Cancel
                                  </Button>
                        <Button
                          onClick={() => { this.addCusines() }}
                          color="primary">
                          Create
                                  </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal> */}
            {/* <Grid item xs={6}>

            </Grid> */}
            <Grid item xs={12} ><div className="switch"> <label>

              <Switch
                checked={this.state.checkedB}
                onClick={() => { this.handleChange5() }}
                value={this.state.checkedB}
                color="primary"
                label="Active/Inactive"
                inputProps={{ 'aria-label': 'primary checkbox' }}
              />Active/Inactive
            </label></div> </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12} >
              <Grid style={{ width: '80%' }}>


                <Table
                >

                  <TableBody>
                    {this.state.mapIngredient.map((row, i) => (
                      <TableRow key={i}>

                        <Grid xs={3}> <TableCell component="th" scope="row">
                          {row.label}
                        </TableCell>
                          <TableCell align="left">  <DeleteIcon
                            onClick={() => { this.deleteItem(i, row) }}
                          /></TableCell></Grid>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Grid>

            </Grid>




          </Grid>

          <Divider style={{ marginTop: '80px' }} />
          <ExpansionPanelActions className='expansionpanelactions' >
            <Button size="small" onClick={() => this.clearFunction()} >Cancel</Button>
            <Button size="small" color="primary" onClick={() => this.postItem()} >
              Create
                      </Button>
          </ExpansionPanelActions>

        </Paper>
    } else {
      addnew = null;
    }

    return (

      <div>

        <Grid container>

          <Grid item lg={10} md={9} sm={8} xs={7}></Grid>
          <Grid item lg={2} md={3} sm={4} xs={5}>
            <div style={{ marginTop: '-45px' }}>
              <UploadButton uploadname="dishes" disable={false} getData={this.getData.bind(this)} />

            </div>
          </Grid>
        </Grid>
        {addnew}

        <div className={classes.root}>
          <Paper className={classes.paper}>
            <Table className={classes.table} size="small" style={{ marginTop: "20px" }}>
              <TableHead>
                <TableRow>
                  <TableCell>Dish Name</TableCell>
                  <TableCell align="left">Dish Type</TableCell>
                  <TableCell align="left">Category</TableCell>
                  <TableCell align="left">Cuisine</TableCell>
                  <TableCell align="left">Active</TableCell>
                  <span onClick={() => this.deletett()}>
                    <Grid open={open2} onClose={this.onCloseModal2} >
                      <div className='delt'>
                        <SvgMaterialIcons />
                      </div>
                    </Grid>
                  </span>
                </TableRow>
              </TableHead>
              <TableBody  >
                {items.map(row => (
                  <TableRow key={row.name}>

                    {/* <TableCell  component="th" scope="row">
                                <input type="checkbox"  name={row.dishName} checked={this.state.checkedItems.get(row.name)}
                                 onChange={this.handleChanges}
                                 style={{marginTop:'10px',width:'20px', height:'15px'}} >
</input>                          <span onClick={()=>{this.onOpenModal2()}}>{row.name}</span>


                                </TableCell> */}
                    <TableCell align="left" onClick={() => { this.onOpenModal8(row.id) }}><input type="checkbox" name={row.name} checked={this.state.checkedItems.get(row.name)}
                      onChange={this.handleChanges}
                      style={{ marginTop: '10px', width: '20px', height: '15px' }} >
                    </input>{row.name}</TableCell>
                    <TableCell align="left" onClick={() => { this.onOpenModal8(row.id) }}>{row.dish_type.name}</TableCell>
                    <TableCell align="left" onClick={() => { this.onOpenModal8(row.id) }}>{row.dish_category.name}</TableCell>
                    <TableCell align="left" onClick={() => { this.onOpenModal8(row.id) }}>{row.cuisine.name}</TableCell>
                    <TableCell align="left">  <Switch
                      checked={this.state.checkedB}
                      onChange={this.handleChange6}
                      value={checkedB}
                      color="primary"
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    /></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>


          </Paper>

          <Dialog
            open={open8}
            onClose={this.handleClose8}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
            <DialogContent>
            <Grid container spacing={16}  >

<Grid item xs={3}>

  <TextField
    label="Dish name"
    name="name"
    value={editItem.name}
    onChange={this.editInputValue}

  />
</Grid>

<Grid item xs={3}>
  <TextField
    style={{ width: '90%' }}
    id="type_dish"
    select
    label="Dish Type"
    name='type'
    value={editItem.type}
    onChange={this.editInputValue}
  >

    <MenuItem onClick={() => { this.onOpenModal1() }} >
      <span onClick={this.onOpenModal1}><span style={{ fontSize: '18px' }}>Create Type</span>   &nbsp;&nbsp;
          <Icon className={classes.icon} color="primary">
          add_circle
          </Icon>
      </span>


    </MenuItem>
    {this.state.typeCategory.map(option => (
      <MenuItem key={option.name} value={option.id}>
        {option.name}
      </MenuItem>
    ))}
  </TextField>
</Grid>
{/*onClose={this.onCloseModal}*/}
<Modal open={open1} onClose={this.onCloseModal1} center>
  <div className={classes.root}>
    <Grid container spacing={16}>
      <Grid item xs={12}>
        <Grid item xs={12}>

          <TextField
            style={{ width: '80%', margin: '20px' }}
            label="Add New Type"
            name="newType"
            value={this.state.newType}
            onChange={this.editInputValue}

          />

          <DialogActions>
            <Button onClick={this.handleClose1} color="primary">
              Cancel
              </Button>
            <Button onClick={() => { this.addType() }} color="primary">
              Create
              </Button>
          </DialogActions>
        </Grid>
      </Grid>
    </Grid>
  </div>
</Modal>



<Grid item xs={3}>
  <TextField
    style={{ width: '90%' }}
    id="category"
    select
    label="Category"
    name='category'
    value={editItem.category}
    onChange={this.editInputValue}
  >

    <MenuItem onClick={() => { this.onOpenModal() }} >
      <span onClick={this.onOpenModal}><span style={{ fontSize: '18px' }}>Create category</span>   &nbsp;&nbsp;
          <Icon className={classes.icon} color="primary">
          add_circle
          </Icon>
      </span>


    </MenuItem>
    {this.state.dishCategory.map(option => (
      <MenuItem key={option.name} value={option.id}>
        {option.name}
      </MenuItem>
    ))}
  </TextField>
</Grid>
{/*onClose={this.onCloseModal}*/}
<Modal open={open} onClose={this.onCloseModal} center>
  <div className={classes.root}>
    <Grid container spacing={16}>
      <Grid item xs={12}>
        <Grid item xs={12}>

          <TextField
            style={{ width: '80%', margin: '20px' }}
            label="Add New Category"
            name="newCategory"
            value={this.state.newCategory}
            onChange={this.editInputValue}

          />

          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
              </Button>
            <Button onClick={() => { this.addCategory() }} color="primary">
              Create
              </Button>
          </DialogActions>
        </Grid>
      </Grid>
    </Grid>
  </div>
</Modal>

<Grid item xs={3}>
  <TextField
    style={{ width: '90%' }}
    id="cuisine Type"
    select
    label="Cuisine"
    name='cuisine'
    value={editItem.cuisine}
    onChange={this.editInputValue}
  >

    <MenuItem onClick={() => { this.onOpenModal2() }} >
      <span onClick={this.onOpenModal2}><span style={{ fontSize: '18px' }}>Create Cuisine</span>   &nbsp;&nbsp;
          <Icon className={classes.icon} color="primary">
          add_circle
          </Icon>
      </span>


    </MenuItem>
    {this.state.readCuisines.map(option => (
      <MenuItem key={option.name} value={option.id}>
        {option.name}
      </MenuItem>
    ))}
  </TextField>
</Grid>
{/*onClose={this.onCloseModal}*/}
<Modal open={open2} onClose={this.onCloseModal2} center>
  <div className={classes.root}>
    <Grid container spacing={16} >
      <Grid item xs={12}>
        <Grid item xs={12}>

          <TextField
            style={{ width: '80%', margin: '20px' }}
            label="Add New Cuisine"
            name="newCusiness"
            value={this.state.newCusiness}
            onChange={this.editInputValue}

          />



          <DialogActions>
            <Button onClick={this.handleClose2} color="primary">
              Cancel
              </Button>
            <Button onClick={() => { this.addCusines() }} color="primary">
              Create
              </Button>
          </DialogActions>
        </Grid>
      </Grid>
    </Grid>
  </div>
</Modal>



{/* <Grid item xs={3}>

<TextField
label="Dish cost"
name="cost"
value={editItem.cost}
onChange={this.editInputValue}

/>
</Grid> */}
<Grid item xs={3}>
  <TextField
    style={{ width: '90%' }}
    id="seoson Type"
    select
    label="Season"
    name='season'
    value={editItem.season}
    onChange={this.editInputValue}
  >

    {/* <MenuItem onClick={() => { this.onOpenModal2() }} >
<span onClick={this.onOpenModal2}><span style={{ fontSize: '18px' }}>Create Cuisine</span>   &nbsp;&nbsp;
          <Icon className={classes.icon} color="primary">
  add_circle
          </Icon>
</span>


</MenuItem> */}
    {this.state.seasonCategory.map((option, id) => (
      <MenuItem key={id} value={option.season}>
        {option.season}
      </MenuItem>
    ))}
  </TextField>
</Grid>
{/*onClose={this.onCloseModal}*/}
{/* <Modal open={open2} onClose={this.onCloseModal2} center>
<div className={classes.root}>
<Grid container spacing={16} style={{ width: '400px', height: '100%' }}>
<Grid item xs={12}>
<Grid item xs={12}>

  <TextField
    style={{ width: '80%', margin: '20px' }}
    label="Add New season"
    name="newcusines"
    value={this.state.newcusines}
    onChange={this.editInputValue}

  />



  <DialogActions>
    <Button onClick={this.handleClose2} color="primary">
      Cancel
              </Button>
    <Button onClick={() => { this.addCusines() }} color="primary">
      Create
              </Button>
  </DialogActions>
</Grid>
</Grid>
</Grid>
</div>
</Modal> */}

<Grid item xs={3}>

  <TextField


    style={{ width: '90%' }}
    id="served Type"
    select
    label="Served"
    name='served'
    value={editItem.served}
    onChange={this.editInputValue}
  >
    <MenuItem onClick={() => { this.onOpenModal6() }} >
      <span onClick={this.onOpenModal6}><span style={{ fontSize: '18px' }}>Create category</span>   &nbsp;&nbsp;
<Icon className={classes.icon} color="primary">
          add_circle
</Icon>
      </span>


    </MenuItem>
    {this.state.servedCategory.map(option => (
      <MenuItem key={option.name} value={option.id}>
        {option.name}
      </MenuItem>
    ))}
  </TextField>
</Grid>
{/*onClose={this.onCloseModal}*/}
<Modal open={open6} onClose={this.onCloseModal6} center>
  <div className={classes.root}>
    <Grid container spacing={16}>
      <Grid item xs={12}>
        <Grid item xs={12}>

          <TextField
            style={{ width: '80%', margin: '20px' }}
            label="Add New Category"
            name="newServed"
            value={this.state.newServed}
            onChange={this.editInputValue}

          />

          <DialogActions>
            <Button onClick={this.handleClose6} color="primary">
              Cancel
</Button>
            <Button onClick={() => { this.addServed() }} color="primary">
              Create
</Button>
          </DialogActions>
        </Grid>
      </Grid>
    </Grid>
  </div>
</Modal>


<Grid item xs={3}>

  <TextField


    style={{ width: '90%' }}
    id="served_in Type"
    select
    label="Served_in"
    name='served_in'
    value={editItem.served_in}
    onChange={this.editInputValue}
  >
    <MenuItem onClick={() => { this.onOpenModal5() }} >
      <span onClick={this.onOpenModal5}><span style={{ fontSize: '18px' }}>Create category</span>   &nbsp;&nbsp;
          <Icon className={classes.icon} color="primary">
          add_circle
          </Icon>
      </span>


    </MenuItem>
    {this.state.served_inCategory.map(option => (
      <MenuItem key={option.name} value={option.id}>
        {option.name}
      </MenuItem>
    ))}
  </TextField>
</Grid>
{/*onClose={this.onCloseModal}*/}
<Modal open={open5} onClose={this.onCloseModal5} center>
  <div className={classes.root}>
    <Grid container spacing={16}>
      <Grid item xs={12}>
        <Grid item xs={12}>

          <TextField
            style={{ width: '80%', margin: '20px' }}
            label="Add New Category"
            name="newServed_in"
            value={this.state.newServed_in}
            onChange={this.editInputValue}

          />

          <DialogActions>
            <Button onClick={this.handleClose5} color="primary">
              Cancel
              </Button>
            <Button onClick={() => { this.addServed_in() }} color="primary">
              Create
              </Button>
          </DialogActions>
        </Grid>
      </Grid>
    </Grid>
  </div>
</Modal>


<Grid item xs={3}>
  <TextField
    style={{ width: '90%' }}
    id="category"
    select
    label="Dish Vendor"
    name='vendor'
    value={editItem.vendor}
    onChange={this.editInputValue}
  >

    <MenuItem onClick={() => { this.onOpenModal7() }} >
      <span onClick={this.onOpenModal7}><span style={{ fontSize: '18px' }}>Create Vendor</span>   &nbsp;&nbsp;
          <Icon className={classes.icon} color="primary">
          add_circle
          </Icon>
      </span>


    </MenuItem>
    {this.state.vendorCategory.map(option => (
      <MenuItem key={option.name} value={option.id}>
        {option.name}
      </MenuItem>
    ))}
  </TextField>
</Grid>
{/*onClose={this.onCloseModal}*/}
<Modal open={open7} onClose={this.onCloseModal7} center>
  <div className={classes.root}>
    <Grid container spacing={16}>
      <Grid item xs={12}>
        <Grid item xs={12}>

          <TextField
            style={{ width: '80%', margin: '20px' }}
            label="Add New Category"
            name="newVendor"
            value={this.state.newVendor}
            onChange={this.editInputValue}

          />

          <DialogActions>
            <Button onClick={this.handleClose7} color="primary">
              Cancel
              </Button>
            <Button onClick={() => { this.addVendor() }} color="primary">
              Create
              </Button>
          </DialogActions>
        </Grid>
      </Grid>
    </Grid>
  </div>
</Modal>
<Grid item xs={9}>
  {/* <TextField
style={{ width: '90%' }}
id="cuisine Type"
select
label="Ingredient"
name='Ingredient'
value={editItem.Ingredient}
onChange={this.editInputValue}
> */}

  {/* <MenuItem 
// onClick={() => { this.onOpenModal3() }} 
>
<span ><span style={{ fontSize: '18px' }}>Create Ingredient</span>   &nbsp;&nbsp;
          <Icon className={classes.icon} color="primary">
  add_circle
          </Icon>
</span>


</MenuItem> */}
  {/* {this.state.Ingredient.map(option => (
<MenuItem key={option.name} value={option.id}>
{option.name}
</MenuItem>
))} */}
  {/* </TextField> */}



  {/* <Grid item lg={5} md={5} sm={8} xs={12} style={{marginTop:'20px'}}> */}
  {/* <TextField
style={{ width: '90%' }}

// className="TextFieldWidth"
id="Meal type"
select
label="Ingredient"
name='cat'
value={this.state.cat}
onChange={event => { this.handleChangekk(event); this.handleId(event) }} */}



  <Select
    // defaultValue={[colourOptions[2], colourOptions[3]]}
    isMulti
    name="colors"
    options={colourOptions}
    className="basic-multi-select"
    classNamePrefix="select"
    value={this.state.selected}
    onChange={this.selectIngredientChange}
  />





  {/* <select className="select"
name="colors"
className="basic-multi-select"
classNamePrefix="select"
isMulti
value={editItem.Ingredient}
// onChange={this.createProjectChange}

options={
this.state.Ingredient.map((owner, index) =>
{
<option value={owner.id}>{owner.name}</option>
})

>
</select> */}


  {/* > */}

  {/* <MenuItem  onClick={()=>{this.onOpenModal2()}} >
              <span onClick={this.onOpenModal2}><span >Create Category</span>   &nbsp;&nbsp;
              <Icon className={classes.icon} color="primary">
                add_circle
              </Icon>
            </span>


          </MenuItem> */}

  {/* 
{this.state.Ingredient.map(option => (
<MenuItem key={option.id} value={JSON.stringify(option)} onClick={(event) => this.handleId(event)} >
{option.name}
</MenuItem>
))} */}



  {/* </TextField> */}
  {/* </Grid> */}
  {/* <Select options={options} /> */}

</Grid>



{/* <Grid item lg={2} md={2} sm={12} xs={12}>
<Button onClick={(event) => this.createlist(event)} color="secondary" variant="contained" >ADD </Button>
</Grid> */}




{/*onClose={this.onCloseModal}*/}
{/* <Modal open={open3} onClose={this.onCloseModal3} center>
<div className={classes.root}>
<Grid container spacing={16} >
<Grid item xs={12}>
<Grid item xs={12}>

  <TextField
    style={{ width: '80%', margin: '20px' }}
    label="Add New Cuisine"
    name="newcusines"
    value={this.state.newcusines}
    onChange={this.editInputValue}

  />



  <DialogActions>
    <Button onClick={this.handleClose3} color="primary">
      Cancel
              </Button>
    <Button
      onClick={() => { this.addCusines() }}
      color="primary">
      Create
              </Button>
  </DialogActions>
</Grid>
</Grid>
</Grid>
</div>
</Modal> */}
{/* <Grid item xs={6}>

</Grid> */}
<Grid item xs={12} ><div className="switch"> <label>

  <Switch
    checked={this.state.checkedB}
    onClick={() => { this.handleChange5() }}
    value={this.state.checkedB}
    color="primary"
    label="Active/Inactive"
    inputProps={{ 'aria-label': 'primary checkbox' }}
  />Active/Inactive
</label></div> </Grid>
<Grid item lg={12} md={12} sm={12} xs={12} >
  <Grid style={{ width: '80%' }}>


    <Table
    >

      <TableBody>
        {this.state.mapIngredient.map((row, i) => (
          <TableRow key={i}>

            <Grid xs={3}> <TableCell component="th" scope="row">
              {row.label}
            </TableCell>
              <TableCell align="left">  <DeleteIcon
                onClick={() => { this.deleteItem(i, row) }}
              /></TableCell></Grid>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </Grid>

</Grid>




</Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose8} color="primary">
                Disagree
          </Button>
              <Button onClick={this.handleClose8} color="primary" autoFocus>
                Agree
          </Button>
            </DialogActions>
          </Dialog>


        </div>




        <Grid item xs={6} style={{ marginTop: '15px', }}>
          <div className='butt'>
            {this.state.pages <= 1 ? (null) : (<Button style={{ marginLeft: '10px' }} onClick={this.buttprevious.bind(this)} variant="outline-primary">Previous</Button>)}
          </div>
        </Grid>
        <Grid item xs={6} style={{
          float: 'right', marginRight: '68px',
          marginTop: '33px'
        }}>
          <div>
            {this.state.pages >= this.state.max ? (null) : (<span id="nam"><Button onClick={this.buttnext.bind(this)} id='possition' variant="outline-primary" style={{ marginTop: '-20px' }} >Next</Button></span>)}

          </div>
        </Grid>
      </div>
    )
  }
}
export default Dishes

