import React, {Fragment,Component} from 'react';


//import material ui Components

import TableCell from '@material-ui/core/TableCell';


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import Checkbox from '@material-ui/core/Checkbox';


// import material ui icons
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';



// import Css files



// import Components
import { ServerAddr } from '../../../NetworkConfig'
import PackageDisplayItemsSub from './PackageDisplayItemsSub';
import PackageDisplayItems from './PackageDisplayItems';
import PackageDisplayItemsUpdateAddMeal from './PackageDisplayItemsUpdateAddMeal';
import PackageDeleteModal from './PackageDeleteModal';


var updatepackrenderc = 0


class PackageDisplayItemsUpdate extends Component
{

  constructor(props)
  {
    super(props);
    this.state={
      id:'',
      name:'',
      modal:false,
      update:false,
      pdc_map:'',
      deleteModal:false,
      checked:false

    }


    this.handleUpdatesButton = this.handleUpdatesButton.bind(this);
    this.handleupdate = this.handleupdate.bind(this);
    this.DeletePackage= this.DeletePackage.bind(this);
    this.SubCheckedDelete = this.SubCheckedDelete.bind(this);
  }


async componentDidMount(){

  console.log("CDM Update");
  let  p =[]

  p =  this.props.items.pdc_map.map((i,k)=>{
    console.log("Item map func pdc",i.id);
      return i.id
    }



  )

  console.log("PackageDisplayItemsUpdate pdc_map state p ",p);

await this.setState({
    pdc_map:p
  })

  console.log("PackageDisplayItemsUpdate pdc_map state",this.state.pdc_map);


  this.setState({
    tchecked:this.props.checked
  })
}

async componentWillReceiveProps(nextProps)
{
let  p =[]

p=nextProps.items.pdc_map.map((i,k)=>{

    return i.id
  }

)


console.log("PackageDisplayItemsUpdate pdc_map state p ",p);
await this.setState({
    pdc_map:p
  })

  console.log("PackageDisplayItemsUpdate pdc_map state rp",this.state.pdc_map);









console.log("checked value in child ",this.props.items.name,this.props.checked,nextProps.checked );

//   if(nextProps.checked )
//   {
//     this.setState({
//       checked:true
//     })
//
//     console.log("Checked true from child props pack update",this.state.checked);
// }
// else if(!nextProps.checked != this.state.tchecked){
//   this.setState({
//     checked:false,
//     tchecked:nextProps
//   })
// }



if(this.props.CheckboxList.includes(this.props.items.id))
{
  this.setState({
    checked:true
  })
}
else{
  this.setState({
    checked:false
  })
}

}



componentWillReceiveProps(nextProps)
{
  if(nextProps.CheckboxList.includes(this.props.items.id))
  {
    this.setState({
      checked:true
    })
  }
  else{
    this.setState({
      checked:false
    })
  }
}


  handleUpdatesButton(e,name)
  {
    console.log('handleUpdatesButton',name);
    e.preventDefault();
    this.setState({
      [name]:!this.state[name],
    })

  }


handleupdate()
{
  this.setState({
    update:false,
    modal:false,
  })
}


async DeletePackage()
{
  // e.preventDefault();
  await ServerAddr.delete(`/packages/modify/packages/${this.props.items.id}`)
  .then(response => {
    console.log("Package deleted sucessfully");
  })
  .catch(error=>{
    console.log("Package delete error");
  })
  await this.props.GetDataToDisplay();
}


SubCheckedDelete(e)
{

  e.preventDefault();
if(e.target.checked)
{
  this.props.DeleteListGenerator("Individual",this.props.items.id,true)

}
else {
  this.props.DeleteListGenerator("Individual",this.props.items.id,false)

}

this.setState({
  checked:!this.state.checked
})
}

checkedFuction(){
}

render(){

  updatepackrenderc +=1

if(this.state.deleteModal)
{
  return (
    <PackageDeleteModal deleteFunction = {this.DeletePackage } open={this.state.deleteModal} toggleModal={this.handleUpdatesButton}/>
  )
}

  return(

    <Fragment>
    {
      // {this.state.pdc_map}

    }


        {/* {this.state.checked ? ("T"):("F")} */}
        <Checkbox value="checked" checked={this.state.checked} onChange={(e)=>{this.SubCheckedDelete(e)}}/>
        {/*
          <Grid container>
          <Grid item lg={4} md={4} sm={4} xs={4}>

          <EditIcon onClick={(e)=>{this.handleUpdatesButton(e,'modal')}}>
        </EditIcon>
        </Grid>

        <Grid item lg={4} md={4} sm={4} xs={4}>
          <DeleteIcon onClick={(e)=>{this.handleUpdatesButton(e,'deleteModal')}} className="PackageDeleteIcon_css"/>

        </Grid>

        <Grid item lg={4} md={4} sm={4} xs={4}>

      </Grid>

    </Grid>
         */}







  <Dialog
  open={this.props.EditMode}
  onClose={(e)=>{this.handleUpdatesButton(e,'modal')}}>

  <DialogTitle id="alert-dialog-title">
    {"Edit Package"}
  </DialogTitle>
  <DialogContent>
    <DialogContentText id="alert-dialog-description">
    </DialogContentText>
<PackageDisplayItems items={this.props.items} edit={true} update={this.state.update} handleupdate={this.handleupdate} GetDataToDisplay = {this.props.GetDataToDisplay} />
<PackageDisplayItemsUpdateAddMeal GetDataToDisplay = {this.props.GetDataToDisplay} pdc_map={this.state.pdc_map} items={this.props.items} mode="AddMeal"/>

  </DialogContent>
  <DialogActions>

  <Button onClick={(e)=>{this.handleUpdatesButton(e,'update')}} color="primary" variant="contained" autoFocus>
    Update
  </Button>
    <Button onClick={(e)=>{this.props.handleState(e,'EditMode')}} color="primary" variant="contained" autoFocus>
      Close
    </Button>
  </DialogActions>
  </Dialog>



</Fragment>
  )
}





}


export default PackageDisplayItemsUpdate;
