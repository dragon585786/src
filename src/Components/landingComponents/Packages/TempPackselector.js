import React,{Fragment,Component} from 'react' ;

//import material ui Components

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';


import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import Input from '@material-ui/core/Input';

import Typography from '@material-ui/core/Typography';

import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';

import Paper from '@material-ui/core/Paper';


// import material ui icons
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';




// import Css files



// import Components
import { ServerAddr } from '../../../NetworkConfig'



class TempPackselector extends Component
{
 constructor(props)
 {
   super(props);
   this.state={
     id:'',
     mealName:'',
     mealId:'',
     dishCount:'',
     mealList:[],
     modal:false
   }


   this.GetMealList = this.GetMealList.bind(this);
   this.handleChanges = this.handleChanges.bind(this);
   this.OpenModal = this.OpenModal.bind(this);
   this.PostMealToPackage = this.PostMealToPackage.bind(this);

 }

 componentDidMount(){
   console.log("TempPackselector cdm",this.props);
   this.setState({
     mealId:this.props.value
   })

   console.log("TempPackselector mid",this.state.mealId);

   this.GetMealList()
 }


async GetMealList(){

  console.log("PackageDisplayItemsUpdateAddMeal getting list ");
  await ServerAddr.get('/dishes/read/meal/category')
  .then(response => {
    console.log("Meal List response",response.data.data);
    this.setState({
      mealList:response.data.data
    })
  })
  .catch(error=>{
    console.log("Meal list error",error);
  })


}


async handleChanges(e,name)
{

  console.log("TempPackselector handleChanges",e.target.value);


  if(name==="mealId" && e.target.value ===0)
  {
    await this.OpenModal()
  }
  else {
    await  this.setState({
        [name]:e.target.value
      })
  }
  if(name==="mealId" )
  {
      var mname = this.state.mealList.find((i,k)=>{
        if(i.id === e.target.value)
        {
          return i.name
        }
        else {
          return null
              }
      })
    await this.props.onChange2(e,mname)
  }
}


OpenModal(e)
{
  console.log("PackageDisplayItemsUpdateAddMeal openmodel");
  this.setState({
    modal: !this.state.modal,
    mealName:''
  })
  console.log("PackageDisplayItemsUpdateAddMeal openmodel state",this.state.modal);


}

async createNewMeal(e)
{
  e.preventDefault();
  await ServerAddr.post(`/dishes/create/meal/category/`,{
    name:this.state.mealName
  })
  .then (  response =>{
    console.log("CreateMeal sucessfully add meal",response.data.data.id);
    this.setState({
      mealId:response.data.data.id
    })
    console.log("New meal id ",this.state.mealId);
    var event ={target:{value:response.data.data.id}}
     this.props.onChange2(event)

  })
  .catch(error =>{
    console.log("CreateMeal error");
  })

  await this.GetMealList()
this.OpenModal()

}



async PostMealToPackage(e)
{
    e.preventDefault();
    let cpack = false

  var pdc

  await ServerAddr.post('/packages/create/pdcmap/',{
      meal_type:this.state.mealId,
      dish_count:this.state.dishCount
    })
    .then(  response =>{
      console.log("Meal posted sucessfully",response.data," status code ",response.data.status_code);
      if(parseInt(response.data.status_code) === 201)
      {
        cpack = true
      }
       pdc = this.props.pdc_map
      console.log("Meal posted sucessfully new pdc b",pdc,"props",this.props.pdc_map);
      pdc.push(response.data.data.id)
      console.log("Meal posted sucessfully new pdc",pdc,this.props.items.id);


    })
    .catch(error=>{
      console.log("Meal posted error",error);
    })


if(cpack)
{
  console.log("Making package");
  await ServerAddr.patch(`/packages/modify/packages/${this.props.items.id}`,{
    id:this.props.items.id,
    name:this.props.items.name,
    pdc_map:pdc
  })
  .then(response =>{
    console.log("New package created",response);
    this.setState({
      id:'',
      mealName:'',
      mealId:'',
      dishCount:'',
    })
  })
  .catch(error=>{
    console.log("new package create error",error);
  })

}
this.props.GetDataToDisplay()
}




  render(){


    const list  = this.state.mealList.map((i,k)=>{
    return(
      <MenuItem key={k} value={i.id}  >
        {i.name}
      </MenuItem>

    )
    })

    return(
      <Fragment>
        {/* {this.state.modal ? ("T"):("F")}
        {this.props.pdc_map}

        // {this.state.mealId}
        */}

        <TextField select value={this.state.mealId} name="mealId" onChange={(e)=>{this.handleChanges(e,'mealId')}} className={this.props.propclassName}>
          <MenuItem value={0}>


            <strong >Create Category</strong>
            <Icon  color="primary" className="PackageCreateMealIcon">
              add_circle
            </Icon>


          </MenuItem>

          {list}
        </TextField>





  <Dialog
  open={this.state.modal}


  onClose={(e)=>{this.handleUpdatesButton(e,'modal')}}>

  <DialogTitle id="alert-dialog-title">
    {"Add Meal"}
  </DialogTitle>

  <DialogActions>

    <Grid container style={{marginLeft:'30px'}}>
      <Grid item lg={12} md={12} sm={12} xs={12}>

        <Grid container>
          <Grid item lg={4} md={4} sm={4} xs={4}>
            Meal:
          </Grid>


          <Grid item lg={8} md={8} sm={8} xs={8}>
            <Input type="text" onChange={(e)=>{this.handleChanges(e,'mealName')}} value={this.state.mealName}>

            </Input>
          </Grid>

        </Grid>

      </Grid>


      <Grid container >
        <Grid item lg={12} md={12} sm={12} xs={12} className="ADDMealButtonsGrid_css">

        <Button variant="contained" color="primary" onClick={(e)=>{this.createNewMeal(e)}} className="AddMealButton_css">Add</Button>
        <Button variant="contained" color="primary" onClick={(e)=>{this.OpenModal(e)}} className="AddMealButton_css">Cancel</Button>

      </Grid>
      </Grid>
    </Grid>

  </DialogActions>
  </Dialog>




        </Fragment>
    );
  }
}

export default TempPackselector;
