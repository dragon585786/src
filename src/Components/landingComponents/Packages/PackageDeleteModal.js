import React,{Fragment,Component} from 'react' ;

//import material ui Components

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Button from '@material-ui/core/Button';


import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import Input from '@material-ui/core/Input';

import Typography from '@material-ui/core/Typography';

import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';

import Paper from '@material-ui/core/Paper';


// import material ui icons
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';




// import Css files



// import Components
import { ServerAddr } from '../../../NetworkConfig'




class PackageDeleteModal extends Component
{
  render(){

    return(


        <Dialog
        open={this.props.open}


        onClose={(e)=>{this.props.toggleModal(e,'deleteModal')}}>

        <DialogTitle id="alert-dialog-title">
          {"Delete Confirmation."}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <Icon className="DeleteConfirmWarningIcon">warning</Icon>
            Continue to delete.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" color="primary" onClick={()=>{this.props.deleteFunction()}}>Yes</Button>
          <Button variant="contained" color="primary" onClick={(e)=>{this.props.toggleModal(e,'deleteModal')}}>No</Button>

        </DialogActions>
        </Dialog>


    )
  }
}

export default PackageDeleteModal;
