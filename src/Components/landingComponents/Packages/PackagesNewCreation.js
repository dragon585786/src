import React,{Fragment,Component} from 'react';

//import material ui Components

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Button from '@material-ui/core/Button';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import Input from '@material-ui/core/Input';

import Typography from '@material-ui/core/Typography';

import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';

import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';


// import material ui icons
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';




// import Css files
import './Packages.css'



// import Components
import { ServerAddr } from '../../../NetworkConfig'
import TempPackselector from './TempPackselector'


var pdc = []

class PackagesNewCreation extends Component{

  constructor(props){
    super(props);
    this.state={
      dialog:false,
      packageName:'',
      mealname:'',
      mealId:'',
      dishcount:'',
      tempList:[]


    }

    this.handleChange = this.handleChange.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
  }


handleChange(e,mname)
{

    console.log("%c New Createion Changes Called ",'color:purple',e.target.value,e.target.name,mname);
    e.preventDefault();

    this.setState({
      [e.target.name]:e.target.value,
    })
if(mname)
{
  this.setState({

    mealName:mname.name
  })

}


}


async handleAdd(e,boolean,val)
{
e.preventDefault();

if(boolean)
{

  var tempList = [...this.state.tempList,{name:this.state.mealName,count:this.state.dishCount}]
  this.setState({
    tempList
  })

  await ServerAddr.post('/packages/create/pdcmap/',{
      meal_type:this.state.mealId,
      dish_count:this.state.dishCount
    })
    .then(  response =>{
      console.log("Meal posted sucessfully",response.data," status code ",response.data.status_code);


      console.log("Meal posted sucessfully new pdc b",pdc);
      pdc.push(response.data.data.id)
      console.log("Meal posted sucessfully new pdc",pdc);


    })
    .catch(error=>{
      console.log("Meal posted error",error);
    })


}

else {
  var tempList = this.state.tempList
  var index  = tempList.findIndex(i => i.name === val)
  console.log("Index is ",index , 'of',val);

}


}



  render(){




    const TempList  = this.state.tempList.map((i,k)=>{
      return(
        <Grid container>
          <Grid item lg={4} md={4} sm={4} xs={4}>
            {i.name}
          </Grid>

          <Grid item lg={4} md={4} sm={4} xs={4}>
            {i.count}
          </Grid>

          <Grid item lg={2} md={2} sm={2} xs={2}>
            <DeleteIcon onClick={(e)=>{this.handleDeletetemp(e)}}/>

            </Grid>

        </Grid>
      )

    })
    return(
    <Fragment>
      {this.props.open ? ("T pn"):("F pn")}

      <Paper  >
        <Grid container className="papermainGrid" >

          <Typography className="HeadingAddNew" variant="h4">Add New Package.</Typography>


            <Grid item lg={12} md={12} sm={12} xs={12} className="PackageNameGrid">
              <TextField

                label="Package name"
                id="simple-start-adornment"
                name="packageName"
                value={this.state.packageName}
                onChange={(event) =>this.handleChange(event)}

              />
          </Grid>

          <Grid item lg={5} md={5} sm={8} xs={12} style={{marginTop:'20px'}}>
            <TempPackselector propclassName="propsCssClassForTemppackSelector"  onChange2={this.handleChange} value={this.state.mealId}/>
          </Grid>




          {/*onClose={this.onCloseModal}*/}





          <Grid  item lg={5} md={5} sm={8} xs={12} style={{marginTop:'20px'}}>
            <TextField
              className="TextFieldWidth"
            label="No of Dishes"
            name="dishCount"
            type="number"
            value={this.state.dishCount}
            onChange={(event) => this.handleChange(event)}

          />
        </Grid>

        <Grid  item lg={2} md={2} sm={12} xs={12}>
          <Button  onClick={(event) => this.handleAdd(event,true)} color="secondary" variant="contained" >ADD </Button>
        </Grid>


{TempList}
        <Grid item lg={12} md={12} sm={12} xs={12} >
          <Grid style={{width:'80%'}}>


          {/* <Table
            >

              <TableBody>
                {list.map((row,i) => (
                  <TableRow key = {`row-${i}`}>
                    <TableCell component="th" scope="row">
                      {row.cat}
                    </TableCell>
                    <TableCell align="left">{row.noDishes}</TableCell>
                    <TableCell align="left">  <DeleteIcon  onClick={this.deleteItem.bind(this, i) } /></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table> */}
          </Grid>

          </Grid>


          <Grid item lg={12} md={12} sm={12} xs={12} className="AddNewPkgButtonGrid">
            <Button size="small"  color="primary" variant="contained" style={{marginRight:'20px'}} onClick={() => this.clearFunction()} >Cancel</Button>
            <Button size="small" color="primary" variant="contained" onClick={() =>this.createPackg()}>
              Create
            </Button>
          </Grid>








        </Grid>

        <Divider  />


      </Paper>




              <Dialog open={this.state.dialog} onClose={this.onCloseDialog2} center>
                  <Grid container spacing={16} style={{width:'400px',height:'100%'}}>
                    <Grid item xs={12}>
                      <Grid item xs={12}>

                        <TextField
                          style={{width:'80%',margin:'20px'}}
                          label="Add New Meal"
                          name="mealtype"
                          value={this.state.mealtype}
                          onChange ={(e)=> this.handleChange2(e)}
                        />



                        <DialogActions>
                          <Button  onClick={this.handleClose2} color="primary">
                            Cancel
                          </Button>
                          <Button onClick={()=>this.postlist()} color="primary">
                            Create
                          </Button>
                        </DialogActions>
                      </Grid>
                    </Grid>
                  </Grid>
              </Dialog>




    </Fragment>
    )
  }
}

export default PackagesNewCreation;
