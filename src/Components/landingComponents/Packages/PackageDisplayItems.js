import React, {Fragment,Component} from 'react';

//import material ui Components

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Button from '@material-ui/core/Button';

import Input from '@material-ui/core/Input';

import Typography from '@material-ui/core/Typography';

import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Switch from '@material-ui/core/Switch';


// import material ui icons
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';




// import Css files



// import Components
import { ServerAddr } from '../../../NetworkConfig'
import PackageDisplayItemsSub from './PackageDisplayItemsSub';



const access = window.localStorage.getItem('access');



class PackageDisplayItems extends Component
{

  constructor(props)
  {
    super(props);
    this.state={
      id:'',
      name:'',
      modal:false,
      Active:false
    }


    this.handleChange =this.handleChange.bind(this);
    this.UpdateData =this.UpdateData.bind(this);
    this.handleActive = this.handleActive.bind(this);


  }

  componentDidMount(){
    console.log("PackageDisplayItemsSub CDM");
     console.log("PackageDisplayItems props",this.props.items);

     this.setState({
          id:this.props.items.id,
          name:this.props.items.name,
          modal:false,
        })
    console.log("PackageDisplayItems state",this.state);

  }
//
// shouldComponentUpdate(nextProps, nextState) {
//   console.log("PackageDisplayItems SCU old props",this.props.items,' /n next props',nextProps.items,' /n old state',this.state,' /nnext state',nextState);
//   // if(nextProps !== this.props)
//   // {
//   //   return true
//   // }
//   // else {
//   //   return false
//   // }
//
// }
componentWillReceiveProps(nextProps)
{
  console.log("PackageDisplayItems CWRP",this.props.update,nextProps.update);
  if(nextProps.update)
  {
    this.UpdateData()
  }




}


async UpdateData()
{
  console.log("PackageDisplayItems update data");

await  ServerAddr.patch(`/packages/modify/packages/${this.state.id}`,{
    id:this.state.id,
    name:this.state.name
  })
  await this.props.GetDataToDisplay()
  await this.props.handleupdate()
}

handleChange(e,name)
{
  this.setState({
    [name]:e.target.value
  })
}


 handleActive(e)
{
   this.setState({
    Active:e.target.checked
  },()=>{
    console.log('Active state',this.state.Active)
  })

  console.log("%c Active ",'color:yellow;background:black',this.state.id,this.state.Active,e.target.checked);
}


render(){

    const SubItemsMeal_type =this.props.items.pdc_map.map((dish,k) =>{
      return(
    <Fragment key={k}>

        {this.props.edit? (<Grid className="SubItemsMealNameTypoGrid EditSubItemGrid"><Typography className="SubItemsMealTypo_type">{dish.meal_type.name}</Typography></Grid>):(<Grid>{dish.meal_type.name}</Grid>)}


      </Fragment>

      )
          })


    const SubItemsCount =this.props.items.pdc_map.map((dish,k) =>{
      return(
    <Fragment key={k}>

          <PackageDisplayItemsSub dish={dish} edit={this.props.edit} update={this.props.update} handleupdate={this.props.handleupdate} GetDataToDisplay = {this.props.GetDataToDisplay}/>
      </Fragment>

      )
          })


const subitems = this.props.items.pdc_map.map(
(dish,k)=>{
  return(
    <Fragment key={k}>


                    <PackageDisplayItemsSub dish={dish} edit={this.props.edit} update={this.props.update} handleupdate={this.props.handleupdate} GetDataToDisplay = {this.props.GetDataToDisplay} items={this.props.items}/>




      </Fragment>
    )
  }
)



  return(
<Fragment key={this.props.key}>


{
  // <TableCell   align="left">{this.state.id}</TableCell>

}

{this.props.edit ? (<Grid container spacing={5} className="MainSubItemGrid">

  <Grid item lg={5} md={5} sm={5} xs={5} className="SubItemsPacknameTypo_css">
    <Typography >Package name: </Typography>
  </Grid>


    <Grid item lg={7} md={7} sm={7} xs={7} className="PackageUpdateNameInput">
      <Input value={this.state.name} onChange={(e)=>{this.handleChange(e,'name')}} ></Input>
    </Grid>





    <Grid item lg={7} md={7} sm={7} xs={7} className="SubItems_css">
      <Typography><strong>Category</strong> </Typography>
    </Grid>


      <Grid item lg={5} md={5} sm={5} xs={5} className="SubItemsCountLabel_css" >
        <Typography><strong>Count</strong> </Typography>
      </Grid>
<Grid container className="SubItemsGrid_css">
{subitems}
</Grid>




</Grid>



):(

<Fragment>

  <TableCell   align="left" onClick={(e)=>{this.props.handleState(e,'EditMode')}}>

    {this.props.items.name}

  </TableCell>

  <TableCell  align="left" onClick={(e)=>{this.props.handleState(e,'EditMode')}}>
    {SubItemsMeal_type}

  </TableCell>

  <TableCell  align="center" onClick={(e)=>{this.props.handleState(e,'EditMode')}}>
    {SubItemsCount}

  </TableCell>
  <TableCell align="center">
    <Switch
            checked={this.state.Active}
            onChange={(e)=>{this.handleActive(e)}}
            value="Active"
            inputProps={{ 'aria-label': 'secondary checkbox' }}
          />
  </TableCell>




</Fragment>


)}












</Fragment>
  )
}

}

export default PackageDisplayItems;
