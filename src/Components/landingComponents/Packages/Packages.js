//jshint esversion:6
import React, { Component , Fragment} from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import '../Packages/Packages.css';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import {serverip} from '../../../NetworkConfig.js';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
//import { defaultServerAddr } from '../../../NetworkConfig.js';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Modal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import DialogActions from '@material-ui/core/DialogActions';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';



import UploadButton from '../UploadButton/UploadButton';
import PackageDisplayItems from './PackageDisplayItems';
import PackageDisplayItemsUpdate from './PackageDisplayItemsUpdate';
import PackagesNewCreation from './PackagesNewCreation';
import PackageWrapper1 from './PackageWrapper1'
const access = window.localStorage.getItem('access');




var renderC = 0
var listtoDelete = []

var selectAllDelete = []
const styles = theme =>  ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  icon: {
    margin: theme.spacing(2),
    float:'right',
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
  },
});
function createData(name, calories) {
  return { name, calories};
}

function createData1(protein,category){
  return {protein,category}
}

const rows = [
  createData('Biryani',30),
  createData('Ice cream sandwich',150),
  createData('Eclair',100),
  createData('Cupcake',50),
  createData('Gingerbread',150),
];



const ranges1 = [
  {
    value: 'Starters',
    label: 'Starters',
  },
  {
    value: 'Main Course',
    label: 'Main Course',
  },
  {
    value: 'Desserts',
    label: 'Desserts',
  },
  {
    value: 'Lunch',
    label: 'Lunch',
  },
];

const package1 = [
  createData1('Breakfast1',4),
  createData1('Lunch1',5),
  createData1('Dinner1',2),
];

class Packages extends Component {

  state = {
    items: [],
    showLoader: false,
    expanded: null,
    editItem: {
      'name':'',
      'max_dishes':'',
    },
    searchValue: this.props.searchresult.searchvalue,
    addNew: this.props.searchresult.createNew,
    open: false,
    max:5,
    pages:1,
    open3:false,
    weightrange:'',
    newType:'',
    open1:false,
    open10:false,
    noDishes:'',
    val:'',
    packagename:'',
    list:[],
    package3:[],
    persons:[],
    dishcount:[],
    data3:'',
    cat:'',
    newCategory:'',
    open2:false,
    mealtype2:'',
    mealtype:'',
    getmeal:[],
    getmeal2:[],
    getmeal3:[],
    getmeal6:[],
    itemid:'',
    itemid2:'',
    createp:'',
    mealid:'',
    json1:'',
    getmeal4:'',
    maparr:[],
    readpack:[],
    dish_count:[],
    maparr2:[],
    cat1:'',
    maparr3:[],
    maparr4:'',
    map123:'',
    finalarr:[],
    sendarr:[],
    maparr7:[],
    mapar:[],
    final1:[],



    checked:false,
    EditMode:false,
    listtoDelete:[],
    DeleteList : []
  }
  componentDidMount() {
    this.setState({ addNew: false,
      searchValue:null,
    });
    this.readPckg();
    this.displayPackg();


  }
  onOpenModal2 = () => {
    this.setState({ open2: true });
    console.log(this.state.open2,'open');
  };




  onCloseModal2 = () => {
    this.setState({ open2: false });
  };


  handleClose2 = () => {
    this.setState({ open2: false });
  }

  handleClose3 = () => {
    this.setState({ open3: false });
  }

  onOpenModal10 = () => {
    this.setState({ open10: true });
    console.log(this.state.open2,'open')
  };



  onOpenModal10 = () => {
    this.setState({ open10: true });
    console.log(this.state.open2,'open')
  };

  handleClose10 = () => {
    this.setState({ open10: false });
  }

  onCloseModal10 = () => {
    this.setState({ open10: false });
  };




  handleChange = panel => (event, expanded) => {
    console.log('event', event);
    console.log('expanded', expanded);
    console.log('panel', panel);


    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  handleChange2(event) {
    const { maparr4} = this.state;
    const Key = event.target.name;
    const value = event.target.value;
    console.log('val123',event.target.value,event.target.name)


    if(Key == 'mealtype'){
      const mealtype2 = this.state;
      this.setState({
        mealtype:event.target.value,
        mealtype2:event.target.value
      },()=>{
        console.log('log1',this.state.mealtype2)
        // this.setState({mealtype2})
      })


      return;
    }








    if (Key == 'cat') {

      this.setState({
        cat:event.target.value,
      });
      console.log('catval',this.state.cat)
      return;
    }






    if (Key == 'noDishes') {

      this.setState({
        noDishes:event.target.value,

      });
      console.log('DISH C',this.state.noDishes)
      return
    }

    if (Key == 'packagename') {

      this.setState({
        packagename:event.target.value,
      });
      return;
    }




  }



  readPckg(){
    const datas1 = {

    };

    defaultServerAddr.get(`${serverip}/dishes/read/meal/category`,
      datas1, {
        headers: {
          Accept: "application/json, text/plain, */*",
          'Content-Type': "application/json;",
          "Authorization" : `Bearer ${access}`
        }}
      )
      .then(res => {

        const getmeal = res.data.data;

        this.setState({ getmeal });

        console.log(getmeal,'readp');

      })

    }







    componentWillReceiveProps(nextProps) {
      // console.log('componentWillReceiveProps', nextProps);
      if (nextProps.searchresult.createNew) {
        this.setState({ addNew: nextProps.searchresult.createNew })

      }
      else {
        this.setState({ searchValue: nextProps.searchresult.searchvalue, })
        // this.getRawmaterials();
      }

    }


    handleFinal(){
      const { finalarr } = this.state;

      this.setState({
        sendarr: [...this.state.sendarr,finalarr]
      },() => {
        console.log('finalaq',this.state.sendarr);
      })
    }














    onCloseModal1 = () => {
      console.log('modal1')
      this.setState({ open: false });
    };



    onOpenModal = () => {
      this.setState({ open: true });
      console.log(this.state.open,'open')
    };

    onCloseModal = () => {
      this.setState({ open: false });
    };




    handleClose = () => {
      this.setState({ open: false });
    }

    handleClose1 = () => {
      this.setState({ open: false });
    }








    clearFunction =() => {
      this.setState({ addNew: false });
    }


    addPackg = () => {

      const { noDishes,getmeal,finalarr,sendarr,final1 } = this.state;
      const data3={
        "dish_count":noDishes,
        "meal_type":this.state.maparr4
      }


      defaultServerAddr.post('/packages/create/pdcmap/',data3,{
        headers: {

          Accept: "application/json, text/plain, */*",
          'Content-Type': "application/json;",
          "Authorization" : `Bearer ${access}`
        },
      })
      .then(res => {
        console.log(res,'res45');
        this.setState({
          finalarr:res['data']['data'].id,
          dish_count:'',
          meal_type:''
        })
        if(res){
          this.handleFinal();
          return;
        }


      })

    }





    buttnext = () => {
      //   console.log('salimm');
      if(this.state.page === 10){
        console.log('next end here');
        return true;
      }
      else{

        console.log('next plus one');
        this.setState({
          pages: this.state.pages + 1
        });
      };
    }

    buttprevious = () => {
      //   console.log('salimm');

      if(this.state.pages === 1){
        console.log('previous button end here');

        return true;
      }
      else{
        console.log('previous minus one hua');
        this.setState({
          pages: this.state.pages - 1
        });
      }

    }




    postlist(){
      console.log(this.state.mealtype2,'package final log')

      var data2={
        "name":this.state.mealtype2,

      }
      defaultServerAddr.post(`${serverip}/dishes/create/meal/category/`, data2 ,{
        headers: {

          Accept: "application/json, text/plain, */*",
          'Content-Type': "application/json;",
          "Authorization" : `Bearer ${access}`

        },
      })
      .then(res => {
        console.log('res1',res)
        if (res) {
          // console.log('web/read-crockery-type/', res);
          this.readPckg();
          this.handleClose2();
        }})


      }

      createlist = (event) => {
        event.preventDefault();
        let list = this.state.list;
        list.push({
          'id':this.state.maparr4,
          'cat':this.state.map123,
          'noDishes':this.state.noDishes,
        });

        list[event.target.name] =[event.target.value];
        this.setState({
          list});
          console.log('addclick',list);

          this.setState({
            maparr4:'',
            map123:'',
            noDishes:'',
            cat:''
          })
          this.addPackg();
        }

        mapfn(){

          const { maparr,maparr7,mapar } = this.state;
          this.setState({
            maparr7:this.state.maparr},() => {
              console.log(maparr7,'mapda');
            });
            maparr7.map((item,id) => {
              return (
                this.setState({
                  mapar:[...this.state.mapar,item.id]},() => {
                    console.log('map56',this.state.mapar)
                  })
                )
              })

            }




            deleteItem(i){
              const { list,maparr,getmeal,maparr2,maparr4,mapar,sendarr } = this.state;
              list.splice(i, 1);
              console.log(list,'list')
              this.setState({ list,
                maparr:list,
                maparr4:this.state.maparr4,
              }, () => {
                console.log(maparr,'list2')
                console.log(maparr4,'list3');
              });
              maparr.map((item) => {
                return (
                  this.setState({
                    mapar:[...this.state.mapar,item.id]},() => {
                      console.log(this.state.mapar,'map54')
                    })
                  )
                });

                this.delpdc(this.state.sendarr);
              }






              createPackg(){
                const { maparr } = this.state;
                const data3={

                  "name":this.state.packagename,
                  "pdc_map":this.state.sendarr,  //change to pdcmap id
                }
                console.log("Posting new package",data3);
                defaultServerAddr.post(`${serverip}/packages/create/packages/`,data3,{
                  headers: {

                    Accept: "application/json, text/plain, */*",
                    'Content-Type': "application/json;",
                    "Authorization" : `Bearer ${access}`

                  },
                })
                .then(res => {
                  console.log('res12',res)
                  this.setState({
                    name:'',
                    sendarr:[],
                    dish_count:'',
                    meal_type:''
                  })
                  if(res){
                    this.displayPackg();
                    this.clearFunction();
                    return
                  }

                })
                this.setState({
                  getmeal2:[]
                })
              }

              async displayPackg(){
                console.log("Getting Package data");
                // this.setState({
                //   showLoader:true
                // })
                const datas2 = {

                };

                await defaultServerAddr.get(`${serverip}/packages/read/packages/`, datas2, {
                  headers: {
                    Accept: "application/json, text/plain, */*",
                    'Content-Type': "application/json;",
                    "Authorization" : `Bearer ${access}`
                  }})

                  .then(res => {

                    const readpack = res.data.data;
                    this.setState({
                      readpack
                    })
                    console.log('packages readpack',this.state.readpack)
                  })

                  // this.setState({
                  //   showLoader:false
                  // })

                }


                handleId(event){
                  let object = JSON.parse(event.target.value);
                  console.log(object.name,'names');
                  this.setState({
                    [event.target.value]:object.id,
                    // [event.target.value]:object.name
                  });
                  console.log(object.id,'object');


                  this.setState({
                    maparr4:object.id,
                  },() => {
                    console.log('mapdata2',this.state.maparr4);
                  },);

                  this.setState({
                    map123:object.name},() => {
                      console.log('map41',this.state.map123);
                    });


                  }

                  delpdc(sendid){
                    const { sendarr } = this.state;
                    console.log('delid',sendid);
                    //   defaultServerAddr.delete(`/packages/modify/packages/${sendid}`,{
                    //   headers: {

                    //   Accept: "application/json, text/plain, */*",
                    //   'Content-Type': "application/json;",
                    //   "Authorization" : `Bearer ${access}`

                    // },
                    //   })
                    //   .then(res => {
                    //      console.log('delpdc',res);
                    //   })

                  }
// ======================================================== MyFunctions ================================


  checkedDelete=  (e)=>
  {
    console.log("CheckBox Clicked");
      e.preventDefault();
      console.log("Checked value",e.target.checked);
      this.setState({
          checked: !this.state.checked
        })
        console.log("CheckBox Clicked",this.state.checked);

        selectAllDelete = this.state.readpack.map((i,k) =>{

          return i.id
        })

        console.log("Adding to List Delete Select All",selectAllDelete);
  }

AddDeleteList(a,mode)
{     if(mode)
  {
    listtoDelete.push(a)
    console.log("Adding to List Delete ",a,listtoDelete);

  }
  else {
    var index =listtoDelete.indexOf(a)
    listtoDelete.splice(index,1)
    console.log("Adding to list delete remove",a,listtoDelete);
  }
}

  handleState(e,name)
  {
      this.setState({
        [name]: !this.state[name]
      })
  }

  handleDeletePackage(e)
  {
    e.preventDefault();
    // if(this.state.checked)
    // {
    //   console.log("Deleting all packages");
    // }
    // else {
    //   console.log("Deleting selected packages",listtoDelete);
    // }




// this.DeleteListGenerator("All",0,this.state.checked)

console.log("%c DeleteListGenerator Deleting",'color:red;background:black',this.state.DeleteList);
  }



DeleteListGenerator(mode,id,Add)
  {
    if(Add)
    {
      if(mode === "All")
      {
        var DeleteList = this.state.DeleteList
        DeleteList = this.state.readpack.map((i,k) =>{
            if(! DeleteList.includes(i.id))
            {
              return i.id

            }


        })

        this.setState({
          checked:true,
          DeleteList
        })
      }
      if(mode ==="Individual")
      {
        var DeleteList = this.state.DeleteList

        DeleteList=[...DeleteList,id]

        this.setState({
          DeleteList
        })
      }
    }
    else {
      if(mode === "All")
      {
        var DeleteList = this.state.DeleteList

        DeleteList = []

        this.setState({
          checked:false,
          DeleteList
        })
      }
      if(mode ==="Individual")
      {
        var DeleteList = this.state.DeleteList

        let index  =  DeleteList.indexOf(id)
        DeleteList.splice(index,id)

        this.setState({
          DeleteList
        })
      }
    }

console.log(" %cDeleteListGenerator is ",' color: brown',mode,id,Add,this.state.DeleteList);
  }


  getData(){
    console.log("Hello");
    }




                  render() {

                    renderC += 1

                    console.log("Render count is ",renderC);
                    const classes = this.state;
                    const { open,mealtype,mealtype2,readpack,getmeal6,open10} = this.state;
                    const { open1 } = this.state;
                    const {open2,open3,getmeal3} =this.state;
                    const {open11 ,package3 } =this.state;
                    const { categories, units, expiries, editItem, expanded ,list} = this.state;
                    const { persons} = this.state;


                    const displayItems = readpack.map((i,k) => {
                      if( i.name.toLowerCase().indexOf(this.props.searchresult.searchvalue.toLowerCase()) ===0 )
                      {


                        return(

                          <Fragment key={k}>



                                          <PackageWrapper1 items={i} GetDataToDisplay ={this.displayPackg.bind(this)} checked={this.state.checked} EditMode={this.state.EditMode} handleState={this.handleState.bind(this)}  AddDeleteList = {this.AddDeleteList.bind(this)} CheckboxList = {this.state.DeleteList}  DeleteListGenerator={this.DeleteListGenerator.bind(this)}/>
                                        {/* <PackageDisplayItemsUpdate items={i} GetDataToDisplay ={this.displayPackg.bind(this)} checked={this.state.checked} EditMode={this.state.EditMode} handleState={this.handleState.bind(this)}/>
                                          <PackageDisplayItems items={i} edit={false} handleState={this.handleState.bind(this)}/> */}



                          </Fragment>
                        )
                      }
                    })







                    console.log(this.state.mealtype2,'final log body');
                    if (this.state.showLoader) {
                      return (
                        < CircularProgress className='loader' />
                      )
                    }



          // Addnew
                    var addnew;
                    if (this.props.searchresult.createNew) {
                      addnew =<PackagesNewCreation />
                      // <Paper  >
                      //   <Grid container className="papermainGrid" >
                      //
                      //     <Typography className="HeadingAddNew" variant="h4">Add New Package.</Typography>
                      //
                      //
                      //       <Grid item lg={12} md={12} sm={12} xs={12} className="PackageNameGrid">
                      //         <TextField
                      //
                      //           label="Package name"
                      //           id="simple-start-adornment"
                      //           name="packagename"
                      //           value={this.state.packagename}
                      //           onChange={(event) =>this.handleChange2(event)}
                      //
                      //         />
                      //     </Grid>
                      //
                      //     <Grid item lg={5} md={5} sm={8} xs={12} style={{marginTop:'20px'}}>
                      //       <TextField
                      //           className="TextFieldWidth"
                      //         id="Meal type"
                      //         select
                      //         label="Category"
                      //         name='cat'
                      //         value={this.state.cat}
                      //         onChange={event => { this.handleChange2(event); this.handleId(event)}}
                      //
                      //         >
                      //
                      //           <MenuItem  onClick={()=>{this.onOpenModal2()}} >
                      //             <span onClick={this.onOpenModal2}><span >Create Category</span>   &nbsp;&nbsp;
                      //             <Icon className={classes.icon} color="primary">
                      //               add_circle
                      //             </Icon>
                      //           </span>
                      //
                      //
                      //         </MenuItem>
                      //         {this.state.getmeal.map(option => (
                      //           <MenuItem key={option.id} value={JSON.stringify(option)} onClick={(event) => this.handleId(event) } >
                      //             {option.name}
                      //           </MenuItem>
                      //         ))}
                      //       </TextField>
                      //     </Grid>
                      //
                      //
                      //
                      //
                      //     {/*onClose={this.onCloseModal}*/}
                      //
                      //
                      //
                      //
                      //
                      //     <Grid  item lg={5} md={5} sm={8} xs={12} style={{marginTop:'20px'}}>
                      //       <TextField
                      //         className="TextFieldWidth"
                      //       label="No of Dishes"
                      //       name="noDishes"
                      //       type="number"
                      //       value={this.state.noDishes}
                      //       onChange={(event) => this.handleChange2(event)}
                      //
                      //     />
                      //   </Grid>
                      //
                      //   <Grid  item lg={2} md={2} sm={12} xs={12}>
                      //     <Button  onClick={(event) => this.createlist(event)} color="secondary" variant="contained" >ADD </Button>
                      //   </Grid>
                      //
                      //
                      //
                      //   <Grid item lg={12} md={12} sm={12} xs={12} >
                      //     <Grid style={{width:'80%'}}>
                      //
                      //
                      //     <Table
                      //       >
                      //
                      //         <TableBody>
                      //           {list.map((row,i) => (
                      //             <TableRow key = {`row-${i}`}>
                      //               <TableCell component="th" scope="row">
                      //                 {row.cat}
                      //               </TableCell>
                      //               <TableCell align="left">{row.noDishes}</TableCell>
                      //               <TableCell align="left">  <DeleteIcon  onClick={this.deleteItem.bind(this, i) } /></TableCell>
                      //             </TableRow>
                      //           ))}
                      //         </TableBody>
                      //       </Table>
                      //     </Grid>
                      //
                      //     </Grid>
                      //
                      //
                      //     <Grid item lg={12} md={12} sm={12} xs={12} className="AddNewPkgButtonGrid">
                      //       <Button size="small"  color="primary" variant="contained" style={{marginRight:'20px'}} onClick={() => this.clearFunction()} >Cancel</Button>
                      //       <Button size="small" color="primary" variant="contained" onClick={() =>this.createPackg()}>
                      //         Create
                      //       </Button>
                      //     </Grid>
                      //
                      //
                      //
                      //
                      //
                      //
                      //
                      //
                      //   </Grid>
                      //
                      //   <Divider  />
                      //
                      //
                      //
                      //   <Modal open={open2} onClose={this.onCloseModal2} center>
                      //     <div className={classes.root}>
                      //       <Grid container spacing={16} style={{width:'400px',height:'100%'}}>
                      //         <Grid item xs={12}>
                      //           <Grid item xs={12}>
                      //
                      //             <TextField
                      //               style={{width:'80%',margin:'20px'}}
                      //               label="Add New Meal"
                      //               name="mealtype"
                      //               value={this.state.mealtype}
                      //               onChange ={(e)=> this.handleChange2(e)}
                      //             />
                      //
                      //
                      //
                      //             <DialogActions>
                      //               <Button  onClick={this.handleClose2} color="primary">
                      //                 Cancel
                      //               </Button>
                      //               <Button onClick={()=>this.postlist()} color="primary">
                      //                 Create
                      //               </Button>
                      //             </DialogActions>
                      //           </Grid>
                      //         </Grid>
                      //       </Grid>
                      //     </div>
                      //   </Modal>
                      // </Paper>



                    } else {
                      addnew = null;
                    }







                    return (
                      <div>

                      {/* <PackagesNewCreation open={this.props.searchresult.createNew}/> */}

                        <Grid container>

                          <Grid item lg={10} md={9} sm={8} xs={7}></Grid>
                          <Grid item lg={2} md={3} sm={4} xs={5} >
                            <div style={{marginTop:'-45px'}}>
                              <UploadButton uploadname="packages" disable={true} getData={this.getData.bind(this)}/>

                            </div>
                          </Grid>
                        </Grid>

                        <Grid container>
                          {

                            // {this.props.searchresult.searchvalue}
                          }
                        </Grid>
                        {addnew}






                {
                  // MainTable
                }




                          {/*



                            <Table className={classes.table} size="small" style={{marginTop:"20px"}}>
                                <TableRow>

                                  <TableCell align="center"><Checkbox checked={this.state.checked} onChange={(e)=>{this.DeleteListGenerator("All",0,e.target.checked)}}/> </TableCell>

                                  <TableCell align="left"><Typography className="TableHeading"><strong>Package name</strong></Typography></TableCell>
                                  <TableCell align="left"><Typography className="TableHeading"><strong>Category</strong></Typography></TableCell>
                                  <TableCell align="center"><Typography className="TableHeading"><strong>Number Of Dishes</strong> </Typography>   </TableCell>
                                  <TableCell align="left"><Typography className="TableHeading"><strong>Active</strong> </Typography>   </TableCell>

                          {

                          // <TableCell align="center"><Typography className="TableHeading"><strong>Edit/Delete</strong></Typography></TableCell>
                          }


                                </TableRow>

                            </Table>





                            */
                          }


                          <Grid container >


                            <Checkbox checked={this.state.checked} onChange={(e)=>{this.DeleteListGenerator("All",0,e.target.checked)}} className="mainCheckBoxpackage"/>
                            <Grid item lg={11} md={11} sm={11} xs={11} className="GridInline">







                                      <DeleteIcon className="MainDeleteIconPackages" onClick={(e)=>this.handleDeletePackage(e)}/>






                        </Grid>

                            <Grid item lg={12} md={12} sm={12} xs={12} className="displayItemsGrid">
                              {displayItems}

                            </Grid>

                          </Grid>








                          <Modal open={open10} onClose={this.onCloseModal10} center style={{align:'center'}}>
                            <div >
                              <Grid item xs={12} container spacing={16} style={{height:'270px',width:'550px'}}>
                                <Grid item xs={2} style={{padding:'10px'}}>Meal Type:</Grid>
                                <Grid item xs={3} style={{marginTop:'-33px',padding:'10px'}}><TextField
                                  id="standard-name"


                                  width="80%"
                                  margin="normal"
                                /></Grid>
                                <Grid item xs={4} style={{padding:'10px'}}>Number of Dishes:</Grid>
                                <Grid item xs={2} style={{padding:'10px',marginTop:'-25px'}}><TextField
                                  id="standard-name"
                                  type="number"

                                  width="90%"
                                  margin="normal"
                                /></Grid>
                                <Grid item xs={4} style={{marginTop:'70px'}}>Package Name:</Grid><Grid item xs={4} style={{marginTop:'70px'}}><TextField
                                  id="standard-name"
                                  style={{marginTop:'-15px'}}
                                  value={this.props.proppackgname}
                                  width="70%"
                                  margin="normal"/></Grid><Grid item xs={4} style={{marginTop:'70px'}}></Grid>









                                  <Grid item xs={12} style={{float:'right',marginTop:'80px',right:'12px',bottom:'12px'}}>
                                    <DialogActions>
                                      <Button  onClick={this.handleClose10} color="primary">
                                        Cancel
                                      </Button>
                                      <Button  color="primary">
                                        Update
                                      </Button>
                                    </DialogActions>
                                  </Grid>
                                </Grid>
                              </div>
                            </Modal>







                          <Grid item xs={6} style={{ marginTop: '15px',}}>
                            <div className='butt'>
                              {this.state.pages<=1 ? (null) : (<Button style={{marginLeft:'10px'}} onClick={this.buttprevious.bind(this)} variant="outline-primary">Previous</Button>)}
                            </div>
                          </Grid>
                          <Grid item xs={6}  style={{float:'right' ,marginRight:'68px',
                            marginTop: '33px'}}>
                            <div>
                              {this.state.pages>=this.state.max ? (null):(<span id="nam"><Button onClick={this.buttnext.bind(this)} id='possition' variant="contained" style={{marginTop:'-20px'}} >Next</Button></span>)}

                            </div>
                          </Grid>

                        </div>
                      )
                    }
                  }
                  export default Packages
