import React,{Component,Fragment} from 'react';


//import material Component

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
import TableCell from '@material-ui/core/TableCell';



// Import Other componentWillReceiveProps
import PackageDisplayItems from './PackageDisplayItems';
import PackageDisplayItemsUpdate from './PackageDisplayItemsUpdate';
import PackagesNewCreation from './PackagesNewCreation';

//import css files

import './Packages.css'



class PackageWrapper1 extends Component
{
  constructor(props)
  { super(props)
    this.state={
      EditMode:false
    }

    this.handleState = this.handleState.bind(this)
  }


handleState(e)
{
  this.setState({
    EditMode:!this.state.EditMode
  })
}
  render()
  {

    return(
<Fragment>

<Grid container className="EachGrid">



    <PackageDisplayItemsUpdate items={this.props.items} GetDataToDisplay ={this.props.GetDataToDisplay} checked={this.props.checked} EditMode={this.state.EditMode} handleState={this.handleState} AddDeleteList={this.props.AddDeleteList} CheckboxList = {this.props.CheckboxList}  DeleteListGenerator={this.props.DeleteListGenerator}/>






  <Grid item lg={11} md={11} sm={10} xs={10}>


    <ExpansionPanel className="pakagesExpansionPanel">
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
              className="PackageWrapperExpansionPanelSummary"
            >
              <Typography >{this.props.items.name}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className="pakagesExpansionPanelDetails">

<Grid container>
  <Grid item lg={12} md={12} sm={12} xs={12}>

    <Table  size="small" >
      <TableBody>
        <TableRow >


          <TableCell align="left"><Typography className="TableHeading"><strong>Package name</strong></Typography></TableCell>
          <TableCell align="left"><Typography className="TableHeading"><strong>Category</strong></Typography></TableCell>
          <TableCell align="center"><Typography className="TableHeading"><strong>Number Of Dishes</strong> </Typography>   </TableCell>
          <TableCell align="center"><Typography className="TableHeading"><strong>Active</strong> </Typography>   </TableCell>

        </TableRow>
        <TableRow>
        <PackageDisplayItems items={this.props.items} edit={false} handleState={this.handleState}/>


        </TableRow>
        </TableBody>
    </Table>

  </Grid>



</Grid>


       </ExpansionPanelDetails>
     </ExpansionPanel>





</Grid>

</Grid>


                                  </Fragment>

    )
  }
}

export default PackageWrapper1;
