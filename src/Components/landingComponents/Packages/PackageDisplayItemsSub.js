import React, {Fragment,Component} from 'react';


//import material ui Components

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Input from '@material-ui/core/Input';

import Typography from '@material-ui/core/Typography';

import Grid from '@material-ui/core/Grid';


// import material ui icons
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';




// import Css files



// import Components
import { ServerAddr } from '../../../NetworkConfig'

import TempPackselector from './TempPackselector';



const access = window.localStorage.getItem('access');



class PackageDisplayItemsSub extends Component
{

  constructor(props)
  {
    super(props);
    this.state={
      pdc_id:'',
      meal_type:'',
      count:'',
      meal_typeID:'',
      tcount:'',
      tmeal_typeId:'',
      tmeal_type:'',
      Deleted:false
    }


        this.handleChange =this.handleChange.bind(this);
        this.UpdateData =this.UpdateData.bind(this);
        this.DeleteMeal = this.DeleteMeal.bind(this);
        this.TempDelete = this.TempDelete.bind(this);
  }

  componentDidMount(){
    console.log("PackageDisplayItemsSub props",this.props.dish);
    this.setState({
      pdc_id:this.props.dish.id,
      meal_type:this.props.dish.meal_type.name,
      tmeal_type:this.props.dish.meal_type.name,
      count:this.props.dish.dish_count,
      meal_typeID:this.props.dish.meal_type.id,
      tmeal_typeId:this.props.dish.meal_type.id,
      tcount:this.props.dish.dish_count
    })

  }
  componentWillReceiveProps(nextProps)
  {
    console.log(`PackageDisplayItemsSub CWRP ${this.props.dish.id}`, this.props,nextProps);
    if(nextProps.update)
    {
      this.UpdateData()
    }



if(this.state.tcount != nextProps.dish.dish_count)
{
  this.setState({
    count:nextProps.dish.dish_count
  })
}

if(this.state.tmeal_type != nextProps.dish.meal_type.name)
{
  this.setState({
    meal_type:nextProps.dish.meal_type.name
  })
}

if(this.state.tmeal_typeId != nextProps.dish.meal_type.id)
{
  this.setState({
    tmeal_typeId:nextProps.dish.meal_type.id
  })
}


    console.log("PackageDisplayItemsSub count",this.props.dish.dish_count);

  }




  async UpdateData()
  {
    console.log("PackageDisplayItems update data");

  await  ServerAddr.patch(`/packages/modify/pdcmap/${this.state.pdc_id}`,{
      id:this.state.pdc_id,
      dish_count:this.state.count,
      meal_type:this.state.meal_typeID

    })
    if(this.state.Deleted)
    {
      console.log("%c Deleting pdc map item",'color:green;background:red',this.state.Deleted,this.props.dish.meal_type.name,this.state.pdc_id);
      await this.DeleteMeal()

    }
    await this.props.GetDataToDisplay()
    await this.props.handleupdate()
  }


  handleChange(e,name)
  {
    console.log("handleChange parent name ",name,'event',e.target.value);
    this.setState({
      [name]:e.target.value
    })

  }

  async DeleteMeal()
  {
    // e.preventDefault();
    await ServerAddr.delete(`/packages/modify/pdcmap/${this.props.dish.id}`)
    .then(respone => {
      console.log("Meal delete sucessfully.");
    })
    .catch(error=>{
      console.log("Meal delete error");
    })
    await this.props.GetDataToDisplay()
    await this.props.handleupdate()

  }

  TempDelete(e){

    e.preventDefault()
this.setState({
  Deleted:true
})
  }



render(){






  return(
<Fragment>
{this.props.edit ? (
  <Fragment>

{this.state.Deleted ? ("") :(

  <Grid container >


      <Grid  item lg={7} md={7} sm={7} xs={7} >

        <Grid >

          {/* id {this.props.dish.meal_type.id} */}
          <TempPackselector onChange2={(e)=>{this.handleChange(e,'meal_typeID')}} value={this.props.dish.meal_type.id} />





      </Grid>
      </Grid>


  <Grid item lg={5} md={5} sm={5} xs={5}>

  <Grid container>

      <Grid item lg={6} md={6} sm={6} xs={6} className="PackageDisplayItemsSub_countGrid ">
        <Input value={this.state.count} onChange={(e)=>{this.handleChange(e,'count')}} className="SubItemsInput_css"></Input>

      </Grid>
    <Grid item lg={6} md={6} sm={6} xs={6} className="SubItemsDeleteGrid">
      <DeleteIcon onClick={(e)=>{this.TempDelete(e)}} className="PackageDeleteIcon_css editDeletIcon"/>
    </Grid>


  </Grid>

  </Grid>




  </Grid>
)}

</Fragment>

):(
<Grid container>
<Grid item lg={12} md={12} sm={12} xs={12} className="SubCountGrid">

  {this.props.dish.dish_count}
</Grid>

</Grid>)}




</Fragment>
  )
}

}

export default PackageDisplayItemsSub;
