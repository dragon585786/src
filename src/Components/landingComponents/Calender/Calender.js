import React, { Component } from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import './Calender.css'
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { ServerAddr } from '../../../NetworkConfig'
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Modal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import DialogActions from '@material-ui/core/DialogActions';
import {serverip} from '../../../NetworkConfig.js';
import '../Uniform/Uniform.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


import UploadButton from '../UploadButton/UploadButton'


const styles = theme =>  ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',

  },
  icon: {
    margin: theme.spacing(2),
    float:'right',
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
  },
});





class Calender extends React.Component {
  state = {
    items: [
      {
      'dates':'01-26-2019',
      'days':'Republic Day'
    },
    {
    'dates':'08-15-2019',
    'days':'Independence Day'
  },
    ],
    years:[{
      id:1,
      name:'2018',
    },
    {
      id:2,
      name:'2019',
    }
  ],
    showLoader: false,
    expanded: null,
    searchValue: this.props.searchresult.searchvalue,
    addNew: this.props.searchresult.createNew,
    open1: false,
    open11:false,
    datePick: new Date(),
    dayDesc:'',
    max:5,
    pages:1,
  };

  componentDidMount() {
    this.setState({ addNew: false,
      searchValue:null,
    });

  }
  clearFunction(){
    this.setState({ addNew: false });
  }


  componentWillReceiveProps(nextProps) {
    // console.log('componentWillReceiveProps', nextProps);
    if (nextProps.searchresult.createNew) {
      this.setState({ addNew: nextProps.searchresult.createNew })

    }
    else {
      this.setState({ searchValue: nextProps.searchresult.searchvalue, })
      // this.getRawmaterials();
    }

  }




  editInputValue = event => {
    console.log('in edit input ', event);

    let editItem = this.state.editItem;
    const Key = event.target.name;
    const value = event.target.value;
    console.log(value)


    if (Key == 'dayDesc') {
      this.setState({
        dayDesc:event.target.value,
      });
      return

      if(Key == 'days'){
        this.setState({
          days:event.target.value,
        })
      }

    }
  }


  handleClick() {
    this.setState({setOpen:true});
    console.log('kyabe',this.state.setOpen)
  }

  onOpenModal1 = () => {
    this.setState({ open1: true });
    console.log(this.state.open1,'open')
  };

  onCloseModal1 = () => {
    this.setState({ open1: false });
  };

  handleClose1 = () => {
    this.setState({ open1: false });
  }


  handleChange = panel => (event, expanded) => {
    console.log('event', event);
    console.log('expanded', expanded);
    console.log('panel', panel);


    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  setInputValue = (data) => {
console.log(this.state.days,'datas')
    var input = {
      days:this.state.days,
      dates:this.state.dates,
    }
    this.setState({ editItem: input,
      deleteUniform:data.uniformImageId

    });
    console.log(data.uniformImageId,'datato'  )
  };

  handleDateChange = date =>{
    console.log(new Date(), date,'date')
    this.setState({ startDate: date });
  }


  buttnext(){
    //   console.log('salimm');
    if(this.state.page === 10){
      console.log('next end here');
      return true;
    }
    else{

      console.log('next plus one');
      this.setState({
        pages: this.state.pages + 1
      });
    };
  }

  buttprevious(){
    //   console.log('salimm');

    if(this.state.pages === 1){
      console.log('previous button end here');

      return true;
    }
    else{
      console.log('previous minus one hua');
      this.setState({
        pages: this.state.pages - 1
      });
    }

  }


  getData(){

  }

  render() {
    const classes = this.state;
    const { open1 } = this.state;

    const { categories, units, expiries, editItem, expanded } = this.state
    if (this.state.showLoader) {
      return (
        < CircularProgress className='loader' />
      )
    }

    var addnew;
    if (this.state.addNew) {
      addnew =
      <Paper style={{ marginTop: '15px', marginBottom: '10px' }} >
        <Grid container spacing={16}  >

          <Grid item xs={1}>
            <h4 className="date-text">Date :</h4>
          </Grid>
          <Grid item xs={3}>

            <DatePicker
              className="date-picker-div"
              selected={this.state.datePick}
              onChange={this.handleDateChange}
            />
          </Grid>

          <Grid item xs={1}>
            <h4 className="date-text">Day:</h4>
          </Grid>
          <Grid item xs={3}>

            <TextField
              label="Day"
              name="name"
              value={this.state.dayDesc}
              onChange={this.editInputValue}
            />
          </Grid>



        </Grid>

        <Divider style={{marginTop:'30px'}} />
        <ExpansionPanelActions className='expansionpanelactions' >
          <Button size="small"  onClick={() => this.clearFunction()} >Cancel</Button>
          <Button size="small" color="primary" onClick={() => this.postItem()} >
            Create
          </Button>
        </ExpansionPanelActions>

      </Paper>
    } else {
      addnew = null;
    }

    return (
      <div>

        <Grid container>

              <Grid item lg={10} md={9} sm={8} xs={7}></Grid>
              <Grid item lg={2} md={3} sm={4} xs={5} >
                <div style={{marginTop:'-45px'}}>
                  <UploadButton uploadname="inventory" disable={true} getData={this.getData.bind(this)}/>

                </div>
              </Grid>
            </Grid>
        {addnew}
        {/* <p>{this.props.searchresult}</p> */}
        {this.state.items.map((prop, key) => {
          return (
            <ExpansionPanel key={key} expanded={expanded === key} onChange={this.handleChange(key)} >
              <ExpansionPanelSummary onClick={() => this.setInputValue(prop)} style={{ marginTop: '10px' }} expandIcon={<ExpandMoreIcon />}
              >
                <Grid container spacing={16}  >
                  <Grid item xs={3}>
                    <p className='para' > {prop.dates}</p>
                    <div className='categorystyle' >
                      <Chip style={{ height: '22px' }} label="Date" variant="outlined" />
                    </div>
                  </Grid>
                  <Grid item xs={9}>
                    <Paper className='dayspaper' >
                      <p> {prop.days} </p> </Paper>
                    </Grid>


                  </Grid>
                </ExpansionPanelSummary>
                <Divider />

                <ExpansionPanelDetails>

                  <Grid container spacing={16}  >
                    <Grid item xs={1}>
                      <h4 className="date-text">Date </h4>
                    </Grid>

                    <Grid item xs={3}>

                      <DatePicker
                        className="date-picker-div"
                        selected={this.state.datePick}
                        onChange={this.handleDateChange}
                      />
                    </Grid>

                    <Grid item xs={2}>
                      <TextField
                        label="Days"
                        name="days"
                        value={this.state.days}
                        onChange={this.editInputValue}

                      />
                    </Grid>

          </Grid>

        </ExpansionPanelDetails>
        <Divider />
        <ExpansionPanelActions className='expansionpanelactions' >

          <Button size="small" color="primary" onClick={() => this.postItem()} >
            Update
          </Button>


          <Button size="small" color="secondary" onClick={()=>{this.onOpenModal11()}}  >
            Delete
          </Button>



          {/* <Snackbar
            anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={open12}
          autoHideDuration={6000}
          onClose={this.handleClose12}
          ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id">Ingredient Deleted</span>}
        action={[

        <IconButton
        key="close"
        aria-label="Close"
        color="inherit"
        className={classes.close}
        onClick={this.handleClose12}
        >
        <CloseIcon />
      </IconButton>,
    ]}
  />*/}
</ExpansionPanelActions>
</ExpansionPanel>



);
})}
    <Grid item xs={6} style={{ marginTop: '15px',}}>
              <div className='butt'>
                {this.state.pages<=1 ? (null) : (<Button style={{marginLeft:'10px'}} onClick={this.buttprevious.bind(this)} variant="outline-primary">Previous</Button>)}
              </div>
              </Grid>
              <Grid item xs={6}  style={{float:'right' ,marginRight:'68px',
    marginTop: '33px'}}>
              <div>
                {this.state.pages>=this.state.max ? (null):(<span id="nam"><Button onClick={this.buttnext.bind(this)} id='possition' variant="outline-primary" style={{marginTop:'-20px'}} >Next</Button></span>)}

              </div>
              </Grid>
</div>
)
}
}
export default Calender
