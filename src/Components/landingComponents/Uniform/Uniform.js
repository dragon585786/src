import React, { Component } from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import '../Packages/Packages.css'
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { serverip } from '../../../NetworkConfig'

import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Modal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import DialogActions from '@material-ui/core/DialogActions';
// import {serverip} from '../../../NetworkConfig.js';
import '../Uniform/Uniform.css';
import { Slide } from 'react-slideshow-image';
import UploadButton from '../UploadButton/UploadButton';




const access = window.localStorage.getItem('access');



const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  icon: {
    margin: theme.spacing(2),
    float: 'right',
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
  },
});

const properties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  arrows: true,
  onChange: (oldIndex, newIndex) => {
    console.log(`slide transition from ${oldIndex} to ${newIndex}`);
  }
}



class Uniform extends React.Component {
  state = {
    items: [],
    showLoader: false,
    expanded: null,
    editItem: {
      'name': '',
      'images': '',
      'category': '',
      'image_type': '',
      'Description': '',
      'video': ''
    },
    searchValue: this.props.searchresult.searchvalue,
    addNew: this.props.searchresult.createNew,
    open1: false,
    open2: false,
    uniformCategory: [],
    uniformType: [],
    image: '',
    uniform: [],
    uniforms: [],
    myFile: [],
    createUniform: [],
    newCategory: '',
    addNewCategory: [],
    newType: '',
    open11: false,
    deleteUniform: '',
    inputImage: '',
    max: 5,
    pages: 1,
  };

  componentDidMount() {
    this.setState({
      addNew: false,
      searchValue: null,
    });
    this.readUniformType();
    this.readUnifrom();
    this.readUniformCategory();
  }
  clearFunction() {
    this.setState({ addNew: false });
  }


  componentWillReceiveProps(nextProps) {
    // console.log('componentWillReceiveProps', nextProps);
    if (nextProps.searchresult.createNew) {
      this.setState({ addNew: nextProps.searchresult.createNew })

    }
    else {
      this.setState({ searchValue: nextProps.searchresult.searchvalue, })
      // this.getRawmaterials();
    }

  }

  Slideshow = (items) => {
    return (
      <div className="slide-container">
        {console.log(items, 'slidee')}
        <Slide {...properties}>
          {
            items.map((paigee, key) => {

              return (<div className="each-slide">
                <div style={{ 'backgroundImage': `url(${paigee.thumbnail})` }}>
                  <span>{key + 1}</span>
                </div>
              </div>)
            })}

        </Slide>
      </div>
    )
  }



  editInputValue = event => {
    console.log('in edit input ', event);

    let editItem = this.state.editItem;
    const Key = event.target.name;
    const value = event.target.value;
    console.log(value)






    editItem[event.target.name] = event.target.value;
    this.setState({
      editItem
    });

    if (Key == 'image[]') {
      const data = new FormData();

      // for (var x = 0; x < event.target.files.length; x++) {

      for (const file of event.target.files) {
        data.append('image', file, file.name);
        console.log(file.name, 'formdatatata');
        this.postItem(data);

      }
      console.log(...data, 'finalformdatta ');

      console.log(this.state.image, 'image')
    }
    if (Key == 'image[[]]') {

      // this.setState({
      //   image: event.target.files[0],
      // })
      // console.log(event.target.files[0], 'imageee')
      // console.log(event.target.files[1], 'imageee')

      // console.log(event.target.files.length, 'imageee')
      const data = new FormData();

      // for (var x = 0; x < event.target.files.length; x++) {

      for (const file of event.target.files) {
        data.append('image', file, file.name);
        console.log(file.name, 'formdatatata');
        this.postItemUpdate(data);

      }
      console.log(...data, 'finalformdatta ');

      // }
      //add to list
      // var li = document.createElement('li');
      // li.innerHTML = 'File ' + (x + 1) + ':  ' + input.files[x].name;
      // list.append(li);
      // console.log(event.target.files[x], 'imageee')

      // this.setState({
      //   image: [event.target.files[x]],
      // }, () => { console.log(this.state.image, 'imageeeimage') });
      // let val = event.target.files[x];
      // var joined = this.state.image.concat(val);
      //  this.setState({ image: joined })




      // }

      console.log(this.state.image, 'imageeeimage')

    }
    if (Key == 'video') {
      this.setState({
        video: event.target.files[0],
      })
      console.log(this.state.image, 'image')
    }


    if (Key == 'newCategory') {
      this.setState({
        newCategory: event.target.value,
      });
      return
    }
    if (Key == 'Description') {
      this.setState({
        Description: event.target.value,
      });
      return
    }

    if (Key == 'newType') {
      this.setState({
        newType: event.target.value,
      });
      return
    }

    if (Key == 'inputImage') {
      this.setState({
        inputImage: event.target.files[0],
        image: this.state.inputImage,
      })

    }






    editItem[event.target.name] = event.target.value;
    this.setState({
      editItem
    });
    console.log(editItem, 'item', event.target.name, event.target.value)

  }





  createUniformCategory() {
    const datas = {
      // "uniformCategoryId": null,
      "name": this.state.newCategory,
    }
    defaultServerAddr.post(`${serverip}/uniform/create/uniform/category/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/uniform/create/setup/', res)
        if (res) {
          console.log('web/create-update-uniform-category/', res);
          this.setState({
            uniform: res.data.data['name']

          })

          this.readUniformCategory();


        } else {
          alert(res.data.validation);
        }
      })
    this.handleClose1()

  }



  readUniformCategory() {
    const datas = {
      // "uniformCategoryId": null,
      "name": ""
    }
    defaultServerAddr.get(`${serverip}/uniform/read/uniform/category/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/uniform/read/setup/mn', res)
        if (res) {
          console.log('web/read-uniform-category/', res);
          res.data.data.map((prop, id) => {
            console.log(prop, '/uniform/read/setup/')

          })
          this.setState({
            uniformCategory: res.data['data']
          })
          console.log('/uniform/read/setup/', this.state.uniformCategory)

        } else {
          alert(res.data.validation);
        }
      })
  }




  createUniformType() {
    const datas = {
      // "uniformTypeId": null,
      "name": this.state.newType,
    }
    defaultServerAddr.post(`${serverip}/uniform/create/uniform/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('exp', res)
        if (res) {
          console.log('web/create-update-uniform-type/', res);
          this.setState({
            uniforms: res.data['data']
          })
          this.readUniformType();
          this.handleClose2();
        } else {
          alert(res.data.validation);
        }
      })
  }
  // createUniformImage(){
  //   var formData = new FormData();
  //   formData.append('image', this.state.image);
  //   this.setState({image:null})
  //   defaultServerAddr.post(`http://34.93.7.13:8002/media/cdn/upload/image/`,  datas,{
  //     headers: {
  //       'Content-Type': "multipart/form-data;",
  //       // Accept: "multipart/form-data",

  //       "Authorization" : `Bearer ${access}`
  //     }})

  //     .then(res => {
  //       console.log('exp', res)
  //       if (res) {
  //         console.log('web/create-update-uniform-type/', res);
  //         this.setState({
  //           uniforms: res.data['data']
  //         })
  //         this.readUniformType();
  //         this.handleClose2();
  //       } else {
  //         alert(res.data.validation);
  //       }
  //     })
  // }

  readUniformType() {
    const datas = {
      // "uniformTypeId": null,
      "name": ""
    }
    defaultServerAddr.get(`${serverip}/uniform/read/uniform/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/uniform/read/setup//', res)
        if (res) {
          console.log('web/read-uniform-type/', res);
          this.setState({
            uniformType: res.data['data']
          })

        } else {
          alert(res.data.validation);
        }
      })
  }


  readUnifrom() {



    defaultServerAddr.get(`${serverip}/uniform/read/uniform/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('imagewaa', res)
        if (res) {
          console.log('web/read-setup-images/', res);
          this.setState({
            items: res.data['data']
          })

        } else {
          alert(res.data.validation);
        }
      })
  }



  postItem(data) {


    // var formData = new FormData();
    // formData.append('image', this.state.image);
    // formData.append('image_type',this.state.editItem['image_type']);
    // formData.append('category',this.state.editItem['category']);
    // formData.append('name',this.state.editItem['name']);
    // formData.append('description',this.state.editItem['Description']);
    // formData.append('video',this.state.editItem['video']);

    // console.log('runbhuaa',formData);



    // console.log(...formData,'read formdata')

    // web/create-update-uniform-image/
    // console.log(...formData, 'formdata')
    // http://34.93.7.13:8002/media/cdn/upload/image/

    defaultServerAddr.post(`http://34.93.7.13:8002/media/cdn/upload/image/`, data, {
      headers: {
        'Content-Type': "multipart/form-data;",
        // Accept: "multipart/form-data",

        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/media/cdn/upload/image/', res)
        if (res) {
          // console.log('web/create-update-uniform-image/', res.data.data.id);
          this.setState(prevState => ({
            createUniform: [...prevState.createUniform, res.data.data['id']]
          }))
          console.log('web/create-update-uniform-image/', this.state.createUniform);

          // this.postItemAll();
          // this.readUnifrom();


        } else {
          alert(res.data.validation);
        }
      })


  }


  postItemAll() {

    const data = {
      'image': this.state.createUniform,
      'type': this.state.editItem['image_type'],
      'category': this.state.editItem['category'],
      'name': this.state.editItem['name'],
      // 'description': this.state.editItem['Description'],

    }

    defaultServerAddr.post(`${serverip}/uniform/create/uniform/`, data, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)


          this.setState({ image: null })

          this.readUnifrom();


        } else {
          alert(res.data.validation);
        }
      })
  }

  postItemUpdate(data) {
    //   var formData = new FormData();
    // formData.append('image', this.state.image);
    // formData.append('image_type',this.state.editItem['image_type']);
    // formData.append('category',this.state.editItem['category']);
    // formData.append('name',this.state.editItem['name']);
    // formData.append('description',this.state.editItem['Description']);
    // formData.append('video',this.state.editItem['video']);



    // this.setState({image:null})

    // console.log(...formData,'read formdata')
    // const datas ={
    //   "image":this.state.image,
    //   "category":1,
    //   "image_type":1,
    //   "name":'test'
    // }
    // // web/create-update-uniform-image/
    // console.log(...formData,'formdata')
    // http://34.93.7.13:8002/media/cdn/upload/image/

    defaultServerAddr.post(`http://34.93.7.13:8002/media/cdn/upload/image/`, data, {
      headers: {
        'Content-Type': "multipart/form-data;",
        // Accept: "multipart/form-data",

        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/media/cdn/upload/image/', res)
        if (res) {
          // console.log('web/create-update-uniform-image/', res.data.data.id);
          this.setState(prevState => ({
            createUniform: [...prevState.createUniform, res.data.data['id']]
          }))
          console.log('web/create-update-uniform-image/', this.state.createUniform);

          // this.postItemAllUpdate(item);/////////////
          // this.readUnifrom();


        } else {
          alert(res.data.validation);
        }
      })



  }

  postItemAllUpdate(rec) {
    const data = {
      'image': this.state.createUniform,
      'type': this.state.editItem['image_type'],
      'category': this.state.editItem['category'],
      'name': this.state.editItem['name'],
      // 'description': this.state.editItem['Description'],

    }
    const id = rec;

    defaultServerAddr.patch(`/uniform/modify/uniform/${id}`, data, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)


          this.setState({ image: null })

          this.readUnifrom();


        } else {
          alert(res.data.validation);
        }
      })

  }
  deleteUniform(item) {

    const id = item;
    defaultServerAddr.delete(`/uniform/modify/uniform/${id}`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)



          this.readUnifrom();


        } else {
          alert(res.data.validation);
        }
        this.setState({ open11: false, });
        this.handleClick();
        this.readUnifrom();
      })

  }

  //     deleteUniformID(rec){
  //       var formData = new FormData();
  //     formData.append('image', this.state.createUniform);
  //     formData.append('type',this.state.editItem['image_type']);
  //     formData.append('category',this.state.editItem['category']);
  //     formData.append('name',this.state.editItem['name']);
  //     formData.append('description',this.state.editItem['Description']);
  //     // formData.append('video',this.state.editItem['video']);F
  //     console.log('runbhua',rec);
  //     const id = rec;

  //       defaultServerAddr.patch(`/uniform/modify/setup/${id}`,  formData,{
  //         headers: {
  //           Accept: "application/json, text/plain, */*",
  //           'Content-Type': "application/json;",
  //           "Authorization" : `Bearer ${access}`
  //         }})

  //         .then(res => {
  //           // this.setState({ addNew: false, image:''});

  //           if (res) {
  //           console.log('/media/cdn/upload/image///', res)



  //             this.readUnifrom();


  //           } else {
  //             alert(res.data.validation);
  //           }
  //         })

  //     }
  //     deleteUniform(){

  //       console.log(this.state.deleteUniform,'public')
  //       const datas = {
  //         "setupImageId":this.state.deleteUniform,

  //       }

  //       defaultServerAddr.post('web/delete-setup-image/', datas,{
  //         headers: {
  //           Accept: "application/json, text/plain, */*",
  // 'Content-Type': "application/json;",
  // "Authorization" : `Bearer ${access}`
  //         }})
  //         .then(res => {
  //           console.log('exp', res)
  //           if (res) {
  //             // console.log('web/read/expiry-dropdown/', res);
  //             this.setState({
  //               newcategorydata: res.data['data']
  //             })
  //             console.log(this.state.newcategorydata,'sup')
  //             this.readUnifrom();
  //           } else {
  //             alert(res.data.validation);
  //           }
  //         })
  //         console.log(this.state.newcategorydata,'sup')
  //         this.setState({ open11: false,  });
  //         this.handleClick();
  //       }

  handleClick() {
    this.setState({ setOpen: true });
    console.log('kyabe', this.state.setOpen)
  }

  onOpenModal1 = () => {
    this.setState({ open1: true });
    console.log(this.state.open1, 'open')
  };

  onCloseModal1 = () => {
    this.setState({ open1: false });
  };

  handleClose1 = () => {
    this.setState({ open1: false });
  }


  onOpenModal2 = () => {
    this.setState({ open2: true });
    console.log(this.state.open2, 'open')
  };

  onCloseModal2 = () => {
    this.setState({ open2: false });
  };

  handleClose2 = () => {
    this.setState({ open2: false });
  }

  onOpenModal11 = () => {
    this.setState({ open11: true });
    console.log(this.state.open11, 'open')
  };

  onCloseModal11 = () => {
    this.setState({ open11: false });
  };

  handleClose11 = () => {
    this.setState({ open11: false });
  }

  handleChange = panel => (event, expanded) => {
    console.log('event', event);
    console.log('expanded', expanded);
    console.log('panel', panel);


    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  setInputValue = (data) => {
    console.log('dataaa', data)
    var input = {
      name: data.name,
      images: data.image,
      category: data.category,
      image_type: data.type,
    }
    this.setState({
      editItem: input,
      deleteUniform: data.setupImageId

    });
    // console.log(data.setupImageId,'datato'  )
  };

  buttnext() {
    //   console.log('salimm');
    if (this.state.page === 10) {
      console.log('next end here');
      return true;
    }
    else {

      console.log('next plus one');
      this.setState({
        pages: this.state.pages + 1
      });
    };
  }

  buttprevious() {
    //   console.log('salimm');

    if (this.state.pages === 1) {
      console.log('previous button end here');

      return true;
    }
    else {
      console.log('previous minus one hua');
      this.setState({
        pages: this.state.pages - 1
      });
    }

  }
  getData() {
    this.setState({
      addNew: false,
      searchValue: null,
    });
    this.readUniformType();
    this.readUnifrom();
    this.readUniformCategory();
  }

  render() {
    const classes = this.state;
    const { open1 } = this.state;
    const { open2 } = this.state;
    const { open11 } = this.state;
    const { myFile } = this.state;
    console.log(this.state.items, 'testing')
    console.log(this.state.uniformCategory, 'cat')
    console.log(this.state.myFile, 'myfile');
    console.log(this.state.addNew, 'newadd')
    const { categories, units, expiries, editItem, expanded } = this.state
    if (this.state.showLoader) {
      return (
        < CircularProgress className='loader' />
      )
    }

    var addnew;
    if (this.state.addNew) {
      addnew =
        <Paper style={{ marginTop: '15px', marginBottom: '10px' }} >
          <Grid container spacing={16}  >

            <Grid item xs={4}>

              <TextField
                label="Uniform Name"
                name="name"
                value={editItem.name}
                onChange={this.editInputValue}

              />
            </Grid>


            <Grid item xs={3}>
              <TextField
                style={{ width: '100%' }}
                id="Uniform Type"
                select
                label="Uniform Category"
                name='category'
                value={editItem.category}
                onChange={this.editInputValue}
              >

                <MenuItem onClick={() => { this.onOpenModal1() }} >
                  <span onClick={this.onOpenModal1}><span style={{ fontSize: '18px' }}>Create Category</span>   &nbsp;&nbsp;
                                <Icon className={classes.icon} color="primary">
                      add_circle
                                </Icon>
                  </span>


                </MenuItem>
                {this.state.uniformCategory.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            {/*onClose={this.onCloseModal}*/}
            <Modal open={open1} onClose={this.onCloseModal1} center>
              <div className={classes.root}>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Category"
                        name="newCategory"
                        value={this.state.newCategory}
                        onChange={this.editInputValue}

                      />



                      <DialogActions>
                        <Button onClick={this.handleClose1} color="primary">
                          Cancel
                                    </Button>
                        <Button onClick={() => { this.createUniformCategory() }} color="primary">
                          Create
                                    </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal>




            {/*onClose={this.onCloseModal}*/}
            {/* <Modal open={open2} onClose={this.onCloseModal2} center>
                          <div className={classes.root}>
                            <Grid container spacing={16}>
                              <Grid item xs={12}>
                                <Grid item xs={12}>

                                  <TextField
                                    style={{width:'80%',margin:'20px'}}
                                    label="Add New Type"
                                    name="newType"
                                    value={this.state.newType}
                                    onChange={this.editInputValue}

                                  />



                                  <DialogActions>
                                    <Button  onClick={this.handleClose2} color="primary">
                                      Cancel
                                    </Button>
                                    <Button onClick={()=>{this.createUniformType()}} color="primary">
                                      Create
                                    </Button>
                                  </DialogActions>
                                </Grid>
                              </Grid>
                            </Grid>
                          </div>
                        </Modal> */}


            <Grid item xs={3} >
              <input
                // accept="image/*"
                // className={classes.input}
                style={{ display: 'none' }}
                id="contained-button-file"
                multiple
                // multiple=""
                type="file"
                name='image[]'
                value={editItem.image}
                onChange={this.editInputValue}
              /><label htmlFor="contained-button-file">
                <Button variant="contained" component="span"
                  // className={classes.button}
                  style={{ width: '65%', padding: '10px' }}
                >
                  Upload
    </Button>
              </label>

            </Grid>




            <Grid item xs={12} style={{ marginTop: '20px' }}>
              {/*<span className={` ${myFile.name === undefined ? '' : 'fileName'}`}>{myFile.name}</span>*/}

            </Grid>

          </Grid>

          <Divider style={{ marginTop: '0px' }} />
          <ExpansionPanelActions className='expansionpanelactions' >
            <Button size="small" onClick={() => this.clearFunction()} >Cancel</Button>
            <Button size="small" color="primary" onClick={() => this.postItemAll()}
            >
              Create
                      </Button>
          </ExpansionPanelActions>

        </Paper>
    } else {
      addnew = null;
    }

    return (
      <div>
        <Grid container>

          <Grid item lg={10} md={9} sm={8} xs={7}></Grid>
          <Grid item lg={2} md={3} sm={4} xs={5} >
            <div style={{ marginTop: '-45px' }}>
              <UploadButton uploadname="uniform" disable={false} getData={this.getData.bind(this)} />

            </div>
          </Grid>
        </Grid>
        {addnew}
        {/* <p>{this.props.searchresult}</p> */}
        {this.state.items.map((prop, key) => {
          return (
            <ExpansionPanel key={key} expanded={expanded === key} onChange={this.handleChange(key)} >
              <ExpansionPanelSummary onClick={() => this.setInputValue(prop)} style={{ marginTop: '10px' }} expandIcon={<ExpandMoreIcon />}
              >

                <Grid container spacing={16}  >
                  <Grid item xs={3} className="imgClass">
                    {/* <img src={serverip+prop.image} /> */}
                    {

                      this.Slideshow(prop.image)

                    }


                  </Grid>

                  <Grid item xs={4}>
                    <h4>Name</h4>
                    {/* <Paper className='dayspaper' > */}
                    <p>{prop.name}  </p>
                  </Grid>

                  <Grid item xs={4}>
                    <h4>Category</h4>
                    <p>{prop.category['name']}  </p>
                  </Grid>





                </Grid>


              </ExpansionPanelSummary>
              <Divider />

              <ExpansionPanelDetails>




                <Grid container spacing={16} >

                  <Grid item xs={3}>

                    <input
                      // accept="image/*"
                      // className={classes.input}
                      style={{ display: 'none' }}
                      id="contained-button-file"
                      multiple
                      // multiple=""
                      type="file"
                      name='image[[]]'
                      value={editItem.image}
                      onChange={this.editInputValue}
                    /><label htmlFor="contained-button-file">
                      <Button variant="contained" component="span"
                        // className={classes.button}
                        style={{ width: '65%', padding: '10px' }}
                      >
                        Upload
    </Button>
                    </label>

                  </Grid>
                  <Grid item xs={3}>

                    <TextField
                      label="Uniform Name"
                      name="name"
                      value={editItem.name}
                      onChange={this.editInputValue}

                    />
                  </Grid>

                  <Grid item xs={3}>
                    <TextField
                      style={{ width: '100%' }}
                      id="Uniform Type"
                      select
                      label="Uniform Category"
                      name='category'
                      value={editItem.category}
                      onChange={this.editInputValue}
                    >

                      <MenuItem onClick={() => { this.onOpenModal1() }} >
                        <span onClick={this.onOpenModal1}><span style={{ fontSize: '18px' }}>Create Category</span>   &nbsp;&nbsp;
                                      <Icon className={classes.icon} color="primary">
                            add_circle
                                      </Icon>
                        </span>


                      </MenuItem>
                      {this.state.uniformCategory.map(option => (
                        <MenuItem key={option.name} value={option.id}>
                          {option.name}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  {/*onClose={this.onCloseModal}*/}
                  <Modal open={open1} onClose={this.onCloseModal1} center>
                    <div className={classes.root}>
                      <Grid container spacing={16}>
                        <Grid item xs={12}>
                          <Grid item xs={12}>

                            <TextField
                              style={{ width: '80%', margin: '20px' }}
                              label="Add New Category"
                              name="newCategory"
                              value={this.state.newCategory}
                              onChange={this.editInputValue}

                            />



                            <DialogActions>
                              <Button onClick={this.handleClose1} color="primary">
                                Cancel
                                          </Button>
                              <Button onClick={() => { this.createUniformCategory() }} color="primary">
                                Create
                                          </Button>
                            </DialogActions>
                          </Grid>
                        </Grid>
                      </Grid>
                    </div>
                  </Modal>





                  {/*onClose={this.onCloseModal}*/}
                  {/* <Modal open={open2} onClose={this.onCloseModal2} center>
                                <div className={classes.root}>
                                  <Grid container spacing={16}>
                                    <Grid item xs={12}>
                                      <Grid item xs={12}>

                                        <TextField
                                          style={{width:'80%',margin:'20px'}}
                                          label="Add New Type"
                                          name="newType"
                                          value={this.state.newType}
                                          onChange={this.editInputValue}

                                        />



                                        <DialogActions>
                                          <Button  onClick={this.handleClose2} color="primary">
                                            Cancel
                                          </Button>
                                          <Button onClick={()=>{this.createUniformType()}} color="primary">
                                            Create
                                          </Button>
                                        </DialogActions>
                                      </Grid>
                                    </Grid>
                                  </Grid>
                                </div>
                              </Modal> */}










                </Grid>

              </ExpansionPanelDetails>
              <Divider />
              <ExpansionPanelActions className='expansionpanelactions' >
                <Button size="small" color="primary" onClick={() => this.postItemAllUpdate(prop.id)} >
                  Update
                            </Button>


                <Button size="small" color="secondary" onClick={() => { this.onOpenModal11() }}>
                  Delete
                            </Button>
                <Modal open={open11} onClose={this.onCloseModal11} center>
                  <div className={classes.root}>
                    <Grid container spacing={16}>
                      <Grid item xs={12}>
                        <Grid item xs={12}>

                          <h4 className="modalClassText">Do you want to delete this Item</h4>


                          <DialogActions>
                            <Button onClick={this.handleClose11} color="primary">
                              Cancel
                                        </Button>
                            <Button color="secondary" onClick={() => this.deleteUniform(prop.id)} color="primary">
                              Delete
                                        </Button>
                          </DialogActions>


                        </Grid>
                      </Grid>
                    </Grid>
                  </div>
                </Modal>
              </ExpansionPanelActions>
            </ExpansionPanel>



          );
        })}
        <Grid item xs={6} style={{ marginTop: '15px', }}>
          <div className='butt'>
            {this.state.pages <= 1 ? (null) : (<Button style={{ marginLeft: '10px' }} onClick={this.buttprevious.bind(this)} variant="outline-primary">Previous</Button>)}
          </div>
        </Grid>
        <Grid item xs={6} style={{
          float: 'right', marginRight: '68px',
          marginTop: '33px'
        }}>
          <div>
            {this.state.pages >= this.state.max ? (null) : (<span id="nam"><Button onClick={this.buttnext.bind(this)} id='possition' variant="outline-primary" style={{ marginTop: '-20px' }} >Next</Button></span>)}

          </div>
        </Grid>
      </div>
    )
  }
}
export default Uniform
