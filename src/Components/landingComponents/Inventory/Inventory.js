import React, { Component } from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import '../Packages/Packages.css'
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { ServerAddr } from '../../../NetworkConfig';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Modal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import DialogActions from '@material-ui/core/DialogActions';
import {serverip} from '../../../NetworkConfig.js';
import '../Inventory/Inventory.css';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import UploadButton from '../UploadButton/UploadButton'

const styles = theme =>  ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginTop:"20px"
  },
  icon: {
    margin: theme.spacing(2),
    float:'right',
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
  },
  table: {
  minWidth: 650,
  marginTop: "20px",
},
});


const options = [
  { value: 1, label: 'Rice' },
  { value: 2, label: 'Ginger' },
  { value: 3, label: 'Lemon' },
];


const options1 = [
  { value: 1, label: 'kg' },
  { value: 2, label: 'ltr' },
  { value: 3, label: 'nos' },
];

const options3 = [
  { value: 1, label: '200' },
  { value: 2, label: '300' },
  { value: 3, label: '400' },
];


function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}


const rows = [
  createData('Ginger', 159, 6.0, 24, 4.0),
  createData('Lemon', 237, 9.0, 37, 4.3),
  createData('Corriander', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];


class Inventory extends React.Component{
  state={
    items: [],
    showLoader: false,
    expanded: null,
    editItem: {
      'name':'',
      'images':'',
      'category':'',
      'image_type':'',
    },
    all_material:[
      {
        'id':1,
        'name':'sandesh'
      },
      {
        id:2,
        'name':'salim'
      }
    ],
    selectedOption: null,
    selectedOption1:null,
    selectedOption2:null,
    searchValue: this.props.searchresult.searchvalue,
    addNew: this.props.searchresult.createNew,
    open1: false,
    open2: false,
    startDate: new Date(),
    endDate: new Date(),
    max:5,
    pages:1,
  }

  componentDidMount(){
    this.setState({ addNew: false,
      searchValue:null,
    });
  }

  componentWillReceiveProps(nextProps) {
    // console.log('componentWillReceiveProps', nextProps);
    if (nextProps.searchresult.createNew) {
      this.setState({ addNew: nextProps.searchresult.createNew })

    }
    else {
      this.setState({ searchValue: nextProps.searchresult.searchvalue, })
      // this.getRawmaterials();
    }

  }

  clearFunction(){
    this.setState({ addNew: false });
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption.value);

  };

  handleChanges = selectedOption1 => {
    this.setState({ selectedOption1 });
    console.log(`Option selected:`, selectedOption1.value);

  };

  handleChangess = selectedOption2 => {
    this.setState({ selectedOption2 });
    console.log(`Option selected:`, selectedOption2.value);

  };



  handleDateChange = date =>{
    console.log(new Date(), date)
    this.setState({ startDate: date });
  }

  handleEndDateChange = date => {
    console.log(new Date(), date)
    this.setState({ endDate: date });
  }

  buttnext(){
    //   console.log('salimm');
    if(this.state.page === 10){
      console.log('next end here');
      return true;
    }
    else{

      console.log('next plus one');
      this.setState({
        pages: this.state.pages + 1
      });
    };
  }

  buttprevious(){
    //   console.log('salimm');

    if(this.state.pages === 1){
      console.log('previous button end here');

      return true;
    }
    else{
      console.log('previous minus one hua');
      this.setState({
        pages: this.state.pages - 1
      });
    }

  }

  getdata()
{

}

  render() {
    const { selectedOption } = this.state;
    const { selectedOption1 } =this.state;
    const{ selectedOption2 } =this.state;
    const classes = this.state;
    const styles = this.state;
    const { open1 } = this.state;
    const { open2 } = this.state;
    const { open11 } = this.state;
    const { myFile } = this.state;
    const { editItem } = this.state;
    console.log(this.state.addNew,'newadd')

    var addnew;
    if (this.state.addNew) {
      addnew =
      <Paper style={{ marginTop: '15px', marginBottom: '10px' }} >
        <Grid className="papergrid" container spacing={16}  style={{paddingLeft:'10px'}}>

          {/*<Grid item xs={3}>

          <TextField
          label="Material"
          name="name"
          value={editItem.name}
          onChange={this.editInputValue}
        />
      </Grid>

      <Grid item xs={3}>
      <select className="select" name="owner_id" value={this.state.owner_id} onChange={this.createProjectChange}>
      {
      this.state.all_material.map((ingrediant, index )=>
      <option value={ingrediant.id}>{ingrediant.name}</option>
    )}
    <option></option>
  </select>
</Grid>*/}
<Grid item xs={1}>
  <h4>Material:</h4>
</Grid>
<Grid item xs={3}>
  <Select
    className="select-fields"
    label="Material"
    value={selectedOption}
    onChange={this.handleChange}
    options={options}
  />
</Grid>

<Grid item xs={1}>
  <h4>Price:</h4>
</Grid>
<Grid item xs={3}>
  <Select
    className="select-fields"
    label="quantity"
    value={selectedOption1}
    onChange={this.handleChanges}
    options={options3}
  />
</Grid>

<Grid item xs={1}>
  <h4>Quantity:</h4>
</Grid>
<Grid item xs={3}>
  <Select
    className="select-fields"
    label="quantity"
    value={selectedOption2}
    onChange={this.handleChangess}
    options={options1}
    placeholder="Quantity"

  />
</Grid>


<Grid item xs={1}>
  <h4 class="date-text">Start Date</h4>
</Grid>
<Grid item xs={3}>
  <DatePicker
    className="date-picker-div"
    selected={this.state.startDate}
    onChange={this.handleDateChange}
  />
</Grid>

<Grid item xs={1}>
  <h4 class="date-text">End Date:</h4>
</Grid>
<Grid item xs={3}>
  <DatePicker
    className="date-picker-div"
    selected={this.state.endDate}
    onChange={this.handleEndDateChange}
  />
</Grid>








</Grid>
<Divider style={{ marginTop: '80px' }} />
<ExpansionPanelActions className='expansionpanelactions' >
  <Button size="small"  onClick={() => this.clearFunction()} >Cancel</Button>
  <Button size="small" color="primary" onClick={() => this.postItem()} >
    ADD
  </Button>
</ExpansionPanelActions>

</Paper>
} else {
  addnew = null;
}
return(
  <div>

  <Grid container>

  <Grid item lg={10} md={9} sm={8} xs={7}></Grid>
  <Grid item lg={2} md={3} sm={4} xs={5} >
    <div style={{marginTop:'-45px'}}>
      <UploadButton uploadname="inventory" disable={true} getData={this.getdata.bind(this)}/>

    </div>
  </Grid>
</Grid>
    {addnew}
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Table className={classes.table} size="small" style={{marginTop:"20px"}}>
          <TableHead>
            <TableRow>
              <TableCell>Material</TableCell>
              <TableCell align="center">Price</TableCell>
              <TableCell align="center">Qty Available</TableCell>
              <TableCell align="center">Expiry Date</TableCell>
              <TableCell align="center">End Date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(row => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="center">{row.calories}</TableCell>
                <TableCell align="center">{row.fat}</TableCell>
                <TableCell align="center">{row.carbs}</TableCell>
                <TableCell align="center">{row.protein}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
    <Grid item xs={6} style={{ marginTop: '15px',}}>
              <div className='butt'>
                {this.state.pages<=1 ? (null) : (<Button style={{marginLeft:'10px'}} onClick={this.buttprevious.bind(this)} variant="outline-primary">Previous</Button>)}
              </div>
              </Grid>
              <Grid item xs={6}  style={{float:'right' ,marginRight:'68px',
    marginTop: '33px'}}>
              <div>
                {this.state.pages>=this.state.max ? (null):(<span id="nam"><Button onClick={this.buttnext.bind(this)} id='possition' variant="outline-primary" style={{marginTop:'-20px'}} >Next</Button></span>)}

              </div>
              </Grid>
  </div>
)
}


}


export default Inventory;
