import { Home, ContentPaste, Notifications, AccountCircle } from '@material-ui/icons';

import Users from '../../landingComponents/Users/Users';
import Rawmaterials from '../../landingComponents/Rawmaterials/Rawmaterials';
import Customers from '../../landingComponents/Customers/Customers';
import Dishes from  '../../landingComponents/Dishes/Dishes';
import Packages from '../../landingComponents/Packages/Packages';
import Uniform from '../../landingComponents/Uniform/Uniform';
import Crockery from '../../landingComponents/Crockery/Crockery';
import Setup from '../../landingComponents/Setup/Setup.js';
import Inventory from '../../landingComponents/Inventory/Inventory.js';
import Calender from '../../landingComponents/Calender/Calender.js';
import Role from '../../landingComponents/Role/Role.js';

const Routes = [
    {
        path: '/dashboard/Users',
        sidebarName: 'Users',
        navbarName: 'Users',
        icon: Home,
        component: Users
    },
    {
        path: '/dashboard/Role',
        sidebarName: 'Role',
        navbarName: 'Role',
        icon: Home,
        component: Role
    },
    {
        path: '',
        sidebarName: 'Raw Materials',
        navbarName: 'Raw Materials',
        icon: AccountCircle,
        component: Rawmaterials
    },
    {
        path: '/dashboard/Dishes',
        sidebarName: 'Dishes',
        navbarName: 'Dishes',
        icon: AccountCircle,
        component: Dishes
    },
    {
        path: '',
        sidebarName: 'Packages',
        navbarName: 'Packages',
        icon: AccountCircle,
        component: Packages
    },
    {
        path: '',
        sidebarName: 'Uniform',
        navbarName: 'Uniform',
        icon: AccountCircle,
        component: Uniform,
    },
    {
        path: '',
        sidebarName: 'Crockery',
        navbarName: 'Crockery',
        icon: AccountCircle,
        component: Crockery,
    },
    {
        path: '',
        sidebarName: 'Setup',
        navbarName: 'Setup',
        icon: AccountCircle,
        component: Setup,
    },
    {
      path: '',
      sidebarName: 'Inventory',
      navbarName: 'Inventory',
      icon: AccountCircle,
      component:Inventory,
    },
    {
        path: '',
        sidebarName: 'Calender',
        navbarName: 'Calender',
        icon: AccountCircle,
        component: Calender
    },
    {
        path: '',
        sidebarName: 'Customers',
        navbarName: 'Customers',
        icon: AccountCircle,
        component: Customers
    },

];

export default Routes;
