import React, { Component } from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import './Users.css';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { ServerAddr } from '../../../NetworkConfig'
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Modal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import DialogActions from '@material-ui/core/DialogActions';
import {serverip} from '../../../NetworkConfig.js';
import '../Uniform/Uniform.css';
import Select from 'react-select';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import SvgMaterialIcons from '../Material-ui/DeleteIcon';
import UploadButton from '../UploadButton/UploadButton'

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = theme =>  ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  icon: {
    margin: theme.spacing(2),
    float:'right',
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
  },
});



const options = [
  { value: 1, label: 'Admin' },
  { value: 2, label: 'Manager' },
  { value: 3, label: 'Employee' },
];


function createData(fname,lname,name, calories, fat, carbs, protein,role) {
  return {fname,lname ,name, calories, fat, carbs, protein,role };
}

const rows = [

  createData('John','Doe' ,'Admin@gmail.com', 9922334455, 'A1239939', '08/29/2019' , 'Admin', 1),
  createData('David','Gomez','Manager@gmail.com', 8836363524, 'jfj82783I', '08/29/2019', 'Manager', 2),
  createData('Ram','Mishra','Employee@gmail.com', 7782736454, 'jfjsds83I', '08/29/2019', 'Employee', 3),
];


class Users extends React.Component {
  state = {
    selectAll:0,
    items: [],
    showLoader: false,
    expanded: null,
    checkedItems: new Map(),
    editItem: {
      'user':'',
      'phone_number':'',
      'password':'',
      'name':'',
      'images':'',
      'category':'',
      'image_type':'',
    },
    admin: true,
    manager:true,
    employee:true,
    arr:[],
    uniqueNames:[],

    searchValue: this.props.searchresult.searchvalue,
    addNew: this.props.searchresult.createNew,
    open1: false,
    open2: false,
    user:'',
    selectedOption: null,
    startDate: new Date(),
    fname:'',
    lname:'',
    name:'',
    password:'',
    role:'',
    contactno:'',
    userRows:[{
      fname:'John',
      lname:'Doe' , 
      'name':'Admin@gmail.com', 
      contactno:9922334455,
      password: 'A1239939',
      datecreated: '08/29/2019' , 
      id:1, 
      rolename:'Admin'
    },
    {
       fname:'Ram',
      lname:'Mishra' , 
      'name':'Manager@gmail.com', 
      contactno:9922334455,
      password: '225566',
      datecreated: '09/29/2019' , 
      id:2, 
      rolename:'Manager'
    },
    {
       fname:'David',
      lname:'Gomez' ,
       'name':'Employee@gmail.com', 
      contactno:9922334455,
      password: '454545',
      datecreated: '10/29/2019' , 
      id:3, 
      rolename:'Employee'

    },
    ],
    roleRows:[
    {
      roleid:1,
      rolename:'Admin'
    },
    {
      roleid:2,
      rolename:'Manager'
    },
    {
      roleid:3,
      rolename:'Employee'
    },],
    selected:{},
  };




  componentDidMount() {
    this.setState({ addNew: false,
      searchValue:null,
    });




  }
  clearFunction(){
    this.setState({ addNew: false });
  }


  componentWillReceiveProps(nextProps) {
    // console.log('componentWillReceiveProps', nextProps);
    if (nextProps.searchresult.createNew) {
      this.setState({ addNew: nextProps.searchresult.createNew })

    }
    else {
      this.setState({ searchValue: nextProps.searchresult.searchvalue, })
      // this.getRawmaterials();
    }

  }





  // editInputValue = event => {
  //   console.log('in edit input ', event);

  //   let editItem = this.state.editItem;
  //   const Key = event.target.name;
  //   const value = event.target.value;
  //   console.log(value)






  //   editItem[event.target.name] = event.target.value;
  //   this.setState({
  //     editItem
  //   });

  //   if(Key ==  'image') {
  //     this.setState({
  //       image:event.target.files[0],
  //     })
  //     console.log(this.state.image,'image')
  //   }


  //   if (Key == 'newCategory') {
  //     this.setState({
  //       newCategory:event.target.value,
  //     });
  //     return
  //   }
  //   if (Key == 'Edit') {
  //     this.setState({
  //       Edit:event.target.value,
  //     });
  //     console.log('bla',this.state.Edit);
  //     return
  //   }

  //   if (Key == 'newType') {
  //     this.setState({
  //       newType:event.target.value,
  //     });
  //     return
  //   }

  //   if (Key == 'inputImage'){
  //     this.setState({
  //       inputImage:event.target.files[0],
  //       image:this.state.inputImage,
  //     })

  //   }






  //   editItem[event.target.name] = event.target.value;
  //   this.setState({
  //     editItem
  //   });
  //   console.log(editItem,'item',event.target.name, event.target.value)

  // }











                handleClick() {
                  this.setState({setOpen:true});
                  console.log('kyabe',this.state.setOpen)
                }

                onOpenModal1 = () => {
                  this.setState({ open1: true });
                  console.log(this.state.open1,'open')
                };

                onCloseModal1 = () => {
                  this.setState({ open1: false });
                };

                handleClose1 = () => {
                  this.setState({ open1: false });
                }





                onOpenModal2 = (item) => {
                  this.setState({ open2: true });
                  
                  this.modalData(item)

                };


                modalData(item){
                  console.log(this.state.open2,'open')
                  console.log(item,'alim');
                  if(item.fname){
                    this.setState({
                      'fname': item.fname
                    });
                  }
                  if(item.lname){
                    this.setState({
                      'lname': item.lname
                    });
                  }
                  if(item.password){
                    this.setState({
                      'password': item.password
                    });
                  }
                  if(item.contactno){
                    this.setState({
                      'contactno': item.contactno
                    });
                  }
                  if(item.roleid){
                    this.setState({
                      'roleid': item.roleid
                    });
                  }
                  if(item.name){
                    this.setState({
                      'name': item.name
                    });
                  }
                }

                onCloseModal2 = () => {
                  this.setState({ open2: false });
                };

                handleClose2 = () => {
                  this.setState({ open2: false });
                }

                onOpenModal11 = () => {
                  this.setState({ open11: true });
                  console.log(this.state.open11,'open')
                };

                onCloseModal11 = () => {
                  this.setState({ open11: false });
                };

                handleClose11 = () => {
                  this.setState({ open11: false });
                }

                handleChange = panel => (event, expanded) => {
                  console.log('event', event);
                  console.log('expanded', expanded);
                  console.log('panel', panel);


                  this.setState({
                    expanded: expanded ? panel : false,
                  });
                };

                handleChange = selectedOption => {
                  this.setState({ selectedOption });
                  console.log(`Option selected:`, selectedOption.value);

                };
                handleChanges = event => {
                  // console.log('salimhan',event.target.value);
                  // const uniqueNames = Array.from(new Set(this.state.arr));
                  const uniqueNames = Array.from(new Set(this.state.arr));


                  console.log('salimhan',event.target.name);

                  if(event.target.name === 'Admin@gmail.com'){
                    this.setState({
                      admin: !this.state.admin,
                      arr: this.state.arr.concat('Admin'),
                    });
                    console.log('khan',this.state.admin);
                    console.log('khan',this.state.arr);
                    console.log('khann',uniqueNames);



                  }
                  if(this.state.admin === false){
                    // var people = ["Bob", "Sally", "Jack"]
                    // var toRemove = 'Admin';
                    if(uniqueNames){
                      var index = uniqueNames.indexOf("Admin");
                      if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
                        uniqueNames.splice(index, 1);
                      }
                      this.setState({
                        uniqueNames: uniqueNames,
                      });
                    }

                    console.log('bla bla',this.state.uniqueNames);
                    return true;
                  }
                  if(event.target.name === 'Manager@gmail.com'){
                    this.setState({
                      manager: !this.state.manager,
                      arr: this.state.arr.concat('Manager'),
                    });
                    console.log('khan',this.state.manager);
                    console.log('khan',this.state.arr);
                  }
                  if(this.state.manager === false){
                    // var people = ["Bob", "Sally", "Jack"]
                    // var toRemove = 'Manager';
                    if(uniqueNames){
                      var index = uniqueNames.indexOf("Manager");
                    if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
                      uniqueNames.splice(index, 1);
                    }
                    this.setState({
                      uniqueNames: uniqueNames,
                    });

                    }
                    console.log('bla bla',this.state.uniqueNames);
                    return true;


                  }

                  if(event.target.name === 'Employee@gmail.com'){
                    this.setState({
                      employee: !this.state.employee,
                      arr: this.state.arr.concat('Employee'),
                    });
                    console.log('khan',this.state.employee);
                    console.log('khan',this.state.arr);
                  }
                  if(this.state.employee === false){
                    // var people = ["Bob", "Sally", "Jack"]
                    // var toRemove = ;
                    if(uniqueNames){
                      var index = uniqueNames.indexOf("Employee");
                    if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
                      uniqueNames.splice(index, 1);
                      this.setState({
                        uniqueNames: uniqueNames,
                      });
                    }
                    }
                    console.log('bla bla',this.state.uniqueNames);
                    return true;


                  }

                };


                deletett = () =>{
                  console.log('delete',this.state.uniqueNames);
                  console.log('delete click hu raha hai');

                }





                setInputValue = (data) => {

                  var input = {
                    name:data.name,
                    'images':data.image,
                    'category':data.category.id,
                    'image_type':data.image_type.id,
                  }
                  this.setState({ editItem: input,
                    deleteUniform:data.uniformImageId
                  });
                  console.log(data.uniformImageId,'datato'  )
                };


getData()
{

}


inputValue(){

}


toggleSelectAllLead(){
let newSelected = {};

    if (this.state.selectAll === 0) {
      this.state.userRows.forEach(x => {
        newSelected[x.id] = true;
      });
      console.log(newSelected,'newSelected');
    }
    this.setState((prevState, props) => ({
      selected: newSelected,
      selectAll: this.state.selectAll === 0 ? 1 : 0
    }),()=>{
      var total_task= [];
      for (const prop in newSelected) {
        if (newSelected.hasOwnProperty(prop)) {
          total_task.push(`${prop}`);
          console.log(`${prop}` ,total_task,'ids',prop);
          this.state.id = total_task;
        }
      }
      console.log(this.state.selected,total_task,'selected')
    })

}



  toggleRowLead(id) {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[id] = !this.state.selected[id];
    console.log(newSelected[id],'new')
    // console.log(newSelected);
    this.setState((prevState, props) => ({
      selected: newSelected,
      selectAll: 2
    }),()=>{
      var totle_task= [];
      for (const prop in this.state.selected) {
        if (this.state.selected.hasOwnProperty(prop)) {
          totle_task.push(`${prop}`);
          console.log(totle_task,'totaltask')
          console.log(totle_task.map(Number));
          this.state.task_list = totle_task.map(Number);
          console.log(this.state.task_list,"togglerowlead");
        }
      }
      console.log(this.state.selected,'selected', )
    })

  }


                render() {
                  const {userRows} = this.state;
                  const classes = this.state;
                  const { open1 } = this.state;
                  const { open2 } = this.state;
                  const { open11 } = this.state;
                  const { myFile } = this.state;
                  const { selectedOption } = this.state;
                  console.log(this.state.items,'testing')
                  const { categories, units, expiries, editItem, expanded } = this.state
                  if (this.state.showLoader) {
                    return (
                      < CircularProgress className='loader' />
                    )
                  }

                  var addnew;
                  if (this.state.addNew) {
                    addnew =
                    <Paper style={{ marginTop: '15px', marginBottom: '10px', paddingLeft:'10px' }} >
                      <Grid container spacing={16}  >

                        <Grid item xs={2}>

                          <TextField
                            label="User Name"
                            name="user"
                            value={editItem.user}
                            onChange={this.inputValue}

                          />
                        </Grid>

                        <Grid item xs={2}>

                          <TextField
                            label="Phone Number"
                            name="phone_number"
                            value={editItem.phone_number}
                            onChange={this.inputValue}
                            type="number"
                          />
                        </Grid>

                        <Grid item xs={2}>

                          <TextField
                            label="Password"
                            name="password"
                            value={editItem.password}
                            onChange={this.inputValue}
                            type="password"
                          />
                        </Grid>



                        <Grid item xs={1}>
                          <h4 class="date-text" style={{float:'right'}} >Created Date</h4>
                        </Grid>
                        <Grid item xs={2}>

                          <DatePicker
                           style={{float:'left'}}
                            className="date-picker-div"
                            selected={this.state.startDate}
                            onChange={this.inputValue}
                          />
                        </Grid>


                        <Grid item xs={2}>
                          <Select
                            style={{width:'100%'}}
                            className="select-fields-users"
                            label="Role"
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                            placeholder="Role"

                          />
                        </Grid>
                      <Grid item xs={12} style={{marginTop:'20px'}}>
                        {/*<span className={` ${myFile.name === undefined ? '' : 'fileName'}`}>{myFile.name}</span>*/}

                      </Grid>

                    </Grid>

                    <Divider style={{ marginTop: '10px' }} />
                    <ExpansionPanelActions className='expansionpanelactions' >
                      <Button size="small"  onClick={() => this.clearFunction()} >Cancel</Button>
                      <Button size="small" color="primary" onClick={() => this.postItem()} >
                        Create
                      </Button>
                    </ExpansionPanelActions>

                  </Paper>
                } else {
                  addnew = null;
                }

                return (
                  <div>

                  <Grid container>

                        <Grid item lg={10} md={9} sm={8} xs={7}></Grid>
                        <Grid item lg={2} md={3} sm={4} xs={5} >
                          <div style={{marginTop:'-45px'}}>
                            <UploadButton uploadname="inventory" disable={true} getData={this.getData.bind(this)}/>

                          </div>
                        </Grid>
                      </Grid>
                    {addnew}

                    <div className={classes.root}>
                      <Paper className={classes.paper}>
                        <Table className={classes.table} size="small" style={{marginTop:"20px"}}>
                          <TableHead>
                            <TableRow>
                            <TableCell> <Checkbox onChange={() => this.toggleSelectAllLead()}></Checkbox>     </TableCell>
<TableCell style={{fontSize:'18px'}}>First Name</TableCell>
                              <TableCell style={{fontSize:'18px'}}>Last Name</TableCell>
                              
                              <TableCell  align="center" style={{fontSize:'18px'}}>User Name (email)</TableCell>
                              <TableCell align="center" style={{fontSize:'18px'}}>Contact Number</TableCell>
                             
                              <TableCell align="center" style={{fontSize:'18px'}} >Role</TableCell>
                              <span onClick={() => this.deletett()}>
                              <Grid open={open2} onClose={this.onCloseModal2} >
                              <div className='delt'>
                              <SvgMaterialIcons />
                              </div>
                              </Grid>
                              </span>


                            </TableRow>
                          </TableHead>
                          <TableBody  >
                            {userRows.map(row => (
                              <TableRow  key={row.name}>
                              <TableCell >  <Checkbox checked={this.state.selected[row.id] === true} onChange={() => this.toggleRowLead(row.id)}> 
</Checkbox>                </TableCell>
                                <TableCell onClick={()=>{this.onOpenModal2(row)}}  className="table-data">
                                {row.fname}
                                 </TableCell>
                                 <TableCell onClick={()=>{this.onOpenModal2(row)}}  className="table-data">
                                <span className="table-data" onClick={()=>{this.onOpenModal2(row)}}>{row.lname}</span>
                                </TableCell>
                                        <TableCell  component="th" scope="row"  align="center">
                                         <span className="table-data" onClick={()=>{this.onOpenModal2(row)}} >{row.name}</span>


                                </TableCell>
                                <TableCell className="table-data" onClick={()=>{this.onOpenModal2(row)}} align="center">{row.contactno}</TableCell>
                                <TableCell className="table-data" onClick={()=>{this.onOpenModal2(row)}} align="center">{row.rolename}</TableCell>

                              </TableRow>
                            ))}
                          </TableBody>
                        </Table>
                        <Modal open={open2} onClose={this.onCloseModal2} center>
                          <div className={classes.root} style={{width:'600px'}}>
                            <Grid container spacing={3}>
                              
                            

                                <Grid item xs={12}>
                                  <h3><b>{this.state.name}</b></h3>
                                </Grid>
                                <Grid item xs={6}>
                                  <TextField
                                    style={{width:'80%',margin:'20px'}}
                                    label="First Name"
                                    name="fname"
                                    value={this.state.fname}
                                    onChange={this.inputValue}


                                  />


                                  </Grid>
                                   <Grid item xs={6}>
                                  <TextField
                                    style={{width:'80%',margin:'20px'}}
                                    label="Last Name"
                                    name="lname"
                                    value={this.state.lname}
                                    onChange={this.inputValue}


                                  />


                                  </Grid>
                                   <Grid item xs={6}>
                                  <TextField
                                    style={{width:'80%',margin:'20px'}}
                                    label="User Id"
                                    name="name"
                                    value={this.state.name}
                                    onChange={this.inputValue}


                                  />



                                  </Grid>

                                  <Grid item xs={6}>
                                  <TextField
                                    style={{width:'80%',margin:'20px'}}
                                    label="Password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.inputValue}


                                  />


                                  </Grid>
                                   <Grid item xs={6}>
                                  <TextField
                                    style={{width:'80%',margin:'20px'}}
                                    label="Contact no"
                                    name="contactno"
                                    value={this.state.contactno}
                                    onChange={this.inputValue}


                                  />


                                  </Grid>
                                  <Grid item xs={6}>
                                  <div style={{paddingTop:'28px'}}>
                                 <Select
                            style={{width:'100%'}}
                            className="select-fields-users"
                            label="Role"
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                            placeholder="Role"

                          />
                          </div>
                                  </Grid>
                                  
                                 


                                  </Grid>
                                                                 
                              
                           <DialogActions >
                                    <Button  onClick={this.handleClose2} color="primary">
                                      Cancel
                                    </Button>
                                    <Button  color="primary">
                                      Update
                                    </Button>
                                  </DialogActions>

                          </div>
                        </Modal>
                      </Paper>
                    </div>

                  </div>
                )
              }
            }
            export default Users
