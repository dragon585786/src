import React, { Component } from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import '../Rawmaterials/Rawmaterials.css'
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { ServerAddr } from '../../../NetworkConfig'
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPaneldeletettSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Modal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import DialogActions from '@material-ui/core/DialogActions';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import { serverip } from '../../../NetworkConfig.js';


import CustomizedButtons from './buttons/ButtonNext';
import SvgMaterialIcons from '../Material-ui/DeleteIcon';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import UploadButton from '../UploadButton/UploadButton'


const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  icon: {
    margin: theme.spacing(2),
    float: 'right',
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
    modalmargin: {
      margin: '60px'
    }
  },
});

const access = window.localStorage.getItem('access');


class RawMaterial extends Component {

  state = {
    items: [],
    showLoader: true,
    expanded: null,
    selected:{},
    selectAll:0,
    editItem: {
      "ingredientId": '',
      "ingredientName": '',
      "category": '',
      "quantity": '',
      "unit": '',
      "price": '',
      "expiry": {
        "duration": '',
        "duration_term": '',
      }
    },
    categories: [],
    units: [{
      name:'Kg',
      uid:1
    },
    {
      name:'Liter',
      uid:2
    },
    {
      name:'Dozen',
      uid:3
    },],
    expirys: [{
      name:'Hrs',
      nid:1
    },
    {
      name:'Week',
      nid:2
    },
    {
      name:'Month',
      nid:3
    },],
    searchValue: this.props.searchresult.searchvalue,
    addNew: this.props.searchresult.createNew,
    open: false,
    open66: false,
    checkedLol: [],
    newUnit: '',
    newCategory: '',
    open1: false,
    open2: false,
    open3: false,
    open4: false,
    open5: false,
    idURL: '',
    open12: true,
    setOpen: true,
    deleteIngredient: '',
    pages: 1,
    max: 5,
    chkbox: false,
    checkedItems: new Map(),
    idIngredientName: '',
    idQuantity: '',
    deleteArray: [],
    deleteArrayAll: {},
    idPrice: '',
    idCategory: '',
    idUnit: '',
    userRows:[{
      category:'Vegetable',
      quantity:12 , 
      unit:'kg',
      'name':'Ginger', 
      duration:5,
      durationt: 'hrs',
      datecreated: '08/29/2019' , 
      id:1, 
      price:60
    },
    {
       category:'Grain',
      quantity:24 ,
      unit:'kg' ,
      'name':'Corriander', 
      duration:10,
      durationt: 'weeks',
      datecreated: '09/29/2019' , 
      id:2, 
      price:50
    },
    {
       category:'Masala',
      quantity:30 ,
      unit:'kg',
       'name':'Dal', 
      duration:12,
      durationt: 'days',
      datecreated: '10/29/2019' , 
      id:3, 
      price:100

    },
    ],
  }
  componentDidMount() {


    this.setState({ addNew: false, searchValue: ' ' });
    // this.getRawmaterials();
    // this.readIngredientCategories();
    // this.readIngredientUnits();
    // this.readExpiryDropdown();
    // this.handleClick();
    // this.addCategory();

  }
  searchValues = (search) => {
    console.log('salimmm search value', search);
    if (search != null) {
      var data = {
        "search_string": search,
        "page": this.state.pages,
        "entries_per_page": 10
      }
      console.log('search data', data);
      this.setState({
        showLoader: true
      })
      defaultServerAddr.get('web/read/ingredient/all/', data, {
        headers: {
          'Content-Type': "application/json;",
          'access-token': window.localStorage.getItem('access_token'),
          'refresh-token': window.localStorage.getItem('refresh_token')
        }
      })

        .then(res => {
          if (res.data.status) {
            console.log('read/ingredient/all/', res);
            this.setState({
              items: res['data']['data'],
              showLoader: false
            })
          } else {
            alert(res.data.validation);

          }
        })
      this.setState({ showLoader: false })

    }

  }


  handleChange = panel => (event, expanded) => {
    console.log('event', event);
    console.log('expanded', expanded);
    console.log('panel', panel);


    this.setState({
      expanded: expanded ? panel : false,
    });
  };
  setInputValue = (data) => {

    var input = {
      "ingredientId": data.ingredientId,
      "ingredientName": data.ingredientName,
      "category": data.category.id,
      "unit": data.unit.id,
      "quantity": '',
      "price": data.price,
      "expiry": {
        "duration": data.expiry.duration,
        "duration_term": data.expiry.duration_term
      }
    }
    this.setState({
      editItem: input,
      deleteIngredient: data.ingredientId
    });
    console.log(this.state.deleteIngredient, 'supss')
  };

  editInputValue = event => {
    console.log('in edit input ', event);

    let editItem = this.state.editItem;
    const Key = event.target.name;
    const value = event.target.value;
    if (Key == 'duration') {
      editItem['expiry'][event.target.name] = event.target.value;
      this.setState({
        editItem
      });
      return
    }
    if (Key == 'duration_term') {
      editItem['expiry'][event.target.name] = event.target.value;
      this.setState({
        editItem
      });
      return
    }

    if (Key == 'new_Category') {
      console.log(event.target.value)
      this.setState({
        newCategory: event.target.value,
      });
      return
    }
    if (Key == 'new_unit') {
      console.log(event.target.value)
      this.setState({
        newUnit: event.target.value,
      });
      return
    }
    if (Key == 'idIngredientName') {
      console.log(event.target.value)
      this.setState({
        idIngredientName: event.target.value,
      });
      return
    }
    if (Key == 'idCategory') {
      console.log(event.target.value)
      this.setState({
        idIngredientName: event.target.value,
      });
      return
    }
    if (Key == 'idPrice') {
      console.log(event.target.value)
      this.setState({
        idPrice: event.target.value,
      });
      return
    }
    if (Key == 'idUnit') {
      console.log(event.target.value)
      this.setState({
        idUnit: event.target.value,
      });
      return
    }
    if (Key == 'idQuantity') {
      console.log(event.target.value)
      this.setState({
        idQuantity: event.target.value,
      });
      return
    }




    editItem[event.target.name] = event.target.value;
    this.setState({
      editItem
    });

    console.log('state', this.state.editItem)

  }

  componentWillReceiveProps(nextProps) {

    // this.searchValues(this.state.searchValue);
    // console.log('componentWillReceiveProps', nextProps);
    if (nextProps.searchresult.createNew) {
      this.setState({ addNew: nextProps.searchresult.createNew })

      var input = {
        "ingredientId": '',
        "ingredientName": '',
        "category": '',
        "unit": '',
        "price": '',
        "quantity": '',
        "expiry": {
          "duration": '',
          "duration_term": ''
        }
      }
      this.setState({ editItem: input });
      console.log('searchdata');
    }
    else {
      this.setState({ searchValue: nextProps.searchresult.searchvalue, })
      this.searchValues(nextProps.searchresult.searchvalue);
      if (nextProps.searchresult.searchvalue == null) {
        this.getRawmaterials();
      }
    }

  }

  postItem() {
      const datas = {

      };

    defaultServerAddr.post(`${serverip}/ingredient/create/ingredient/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('create-ingredient', res);
        if (res.status) {
          console.log('create-ingredient', res);
          this.setState({ addNew: false });
          this.getRawmaterials();
        } else {
          alert(res.data.validation);
        }
      })


  }

  readIngredientCategories() {
    var datas = {
      'name': this.state.editItem['ingredientName'],
    };
    defaultServerAddr.get(`${serverip}/ingredient/read/ingredient/category/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('exp', res)
        if (res) {
          console.log('incate', res);
          this.setState({
            categories: res.data['data']
          })
          //console.log(this.state.expiries, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
  }

  readIngredientUnits() {
    var datas = {
      'name': this.state.editItem['unit'],

    };
    defaultServerAddr.get(`${serverip}/ingredient/create/unit/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        if (res) {
          console.log('web/read/ingredient/unit/', res);
          this.setState({
            units: res.data['data']
          })
        } else {
          alert(res.data.validation);
        }
        console.log(this.state.newcategorydata, 'sup')
        this.setState({ open66: false, });
        this.readIngredienUnits();
      })
  }



  readExpiryDropdown() {

    defaultServerAddr.get(`${serverip}/ingredient/read/ingredient/expiry/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('/ingredient/read/ingredient/expiry/', res)
        if (res) {
          // console.log('web/read/expiry-dropdown/', res);
          this.setState({
            //expiries: res.data['data']
          })
          console.log(this.state.expiries, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.expiries, 'sup')
  }


  getRawmaterials() {
    // var data = {
    //   "search_string": null,
    //   "page": this.state.pages,
    //   "entries_per_page": 10
    // }
    // console.log('search data', data);
    this.setState({
      showLoader: true
    });
    defaultServerAddr.get(`${serverip}/ingredient/read/ingredient/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('alare', res)
        if (res) {
          console.log('read/ingredient/all/', res);
          this.setState({
            items: res.data['data'],
            showLoader: false
          })
          console.log('katra', this.state.items)
        } else {
          alert(res.data.validation);

        }
      })
    this.setState({ showLoader: false })

  }


  clearFunction() {
    this.setState({ addNew: false });
  }

  





  addCategory() {

    const datas = {
      // "categoryDishId":"",
      "name": this.state.newCategory,
    }
    // const id = rec;


    defaultServerAddr.post(`${serverip}/ingredient/create/ingredient/category/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('respone', res)
        if (res) {
          // console.log('web/read/expiry-dropdown/', res);
          this.setState({
            categories: res.data.data['name']
          })
          console.log(this.state.newcategorydata, 'sup')
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.newcategorydata, 'sup')
    this.setState({ open: false, });
    this.readIngredientCategories();

  }

  deleteIngredient() {
    console.log(this.state.deleteIngredient, 'public')
    const datas = {
      "ingredientId": this.state.deleteIngredient,

    }

    defaultServerAddr.post(`${serverip}/dishes/modify/ingredient/{id}`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })
      .then(res => {
        console.log('exp', res)
        if (res) {
          // console.log('web/read/expiry-dropdown/', res);
          this.setState({
            newcategorydata: res.data['data']
          })
          console.log(this.state.newcategorydata, 'sup')
          this.getRawmaterials();
        } else {
          alert(res.data.validation);
        }
      })
    console.log(this.state.newcategorydata, 'sup')
    this.setState({ open: false, });
    this.readIngredientCategories();
    this.setState({ open1: false });
    this.handleClick();
  }
  onOpenModal2 = (rec) => {
    this.setState({
      open2: true,
      idURL: rec
    });
    console.log(this.state.open2, 'open')
  };





  onCloseModal2 = () => {
    this.setState({ open2: false });
  };
  



  handleClick() {
    this.setState({ setOpen: true });
    console.log('kyabe', this.state.setOpen)
  }

  handleClose2() {
    this.setState({ open2: false });

  }
  


  handleChanges = item => {



    if (item) {
      console.log('itemm', item)
      this.setState({
        deleteArray: [item]
      });
      console.log('itemm', this.state.deleteArray)

    }


  };



  deletett = () => {
    // console.log('delete',this.state.uniqueNames);
    // console.log('delete click hu raha hai');
    // const id = item;
    console.log('lop');
    console.log('lop', this.state.deleteArray);

    if (this.state.deleteArray.length != 0) {
      console.log('lop');

      this.state.deleteArray.map(option => {
        const id = option
        console.log('lop');

        defaultServerAddr.delete(`${serverip}/ingredient/modify/ingredient/${id}`, {
          headers: {
            Accept: "application/json, text/plain, */*",
            'Content-Type': "application/json;",
            "Authorization": `Bearer ${access}`
          }
        })

          .then(res => {
            // this.setState({ addNew: false, image:''});

            if (res) {
              console.log('/media/cdn/upload/image///', res)



              this.getRawmaterials();



            } else {
              alert(res.data.validation);
            }
          })

        this.setState({ open11: false, });
        this.handleClick();
        this.getRawmaterials();
        console.log('lop');

      })
    }
    else {
      console.log('looop');

      this.deleteArrayAll();
    }


  }
  deleteArrayAll() {
    this.state.deleteArrayAll.map(option => {
      const id = option.deleteArrayAll;
      console.log('looop');

      defaultServerAddr.delete(`${serverip}/ingredient/modify/ingredient/${id}`, {
        headers: {
          Accept: "application/json, text/plain, */*",
          'Content-Type': "application/json;",
          "Authorization": `Bearer ${access}`
        }
      })

        .then(res => {
          // this.setState({ addNew: false, image:''});

          if (res) {
            console.log('/media/cdn/upload/image///', res)



            this.getRawmaterials();



          } else {
            alert(res.data.validation);
          }
        })
      console.log('looop');

      this.setState({ open11: false, });
      this.handleClick();
      this.getRawmaterials();
    })

  }
  deleteAll() {
    console.log(this.state.items, 'itemm')
    this.setState({
      chkbox: !this.state.chkbox,
    })
    const newFile = this.state.items.map((file) => {
      return { ...file.id, deleteArrayAll: file.id };
    });
    this.setState({ deleteArrayAll: newFile }, () => {
      console.log('itemm', this.state.deleteArrayAll)
    });

    // this.state.items.map(option =>{
    //   // function() =>{this.handleChanges(option.id)};
    //   if(option){
    //     console.log('itemm',option.id)
    //     this.setState({
    //       deleteArray:[option.id]
    //     });
    // console.log('itemm',this.state.deleteArray)
    //   console.log(option.id,'itemm')
    // }});
  }
  buttnext() {
    //   console.log('salimm');
    if (this.state.page == 10) {
      console.log('next end here');
      return true;
    }
    else {

      console.log('next plus one');
      this.setState({
        pages: this.state.pages + 1
      });
    };
  }

  buttprevious() {
    //   console.log('salimm');

    if (this.state.pages === 1) {
      console.log('previous button end here');

      return true;
    }
    else {
      console.log('previous minus one hua');
      this.setState({
        pages: this.state.pages - 1
      });
    }

  }

  updateIngredient1() {
    console.log('idce', this.state.idIngredientName)
    var formData = new FormData();
    formData.append('name', this.state.idIngredientName);
    //  this.state.idIngredientName = null;
    formData.append('price_per_unit', this.state.idPrice);
    formData.append('category', this.state.idCategory);
    formData.append('unit', this.state.idUnit);
    formData.append('expiry', this.state.id);

    //  formData.append('price_per_unit', this.state.id);

    // //  this.state.idPrice = null;
    formData.append('quantity', this.state.idQuantity);
    // //  this.state.idQuantity = null;

    const id = this.state.idURL;

    defaultServerAddr.patch(`/ingredient/modify/ingredient/${id}`, formData, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)



          this.getRawmaterials();
          this.handleClose4();
          this.handleClose3();
          this.handleClose2();
          this.handleClose5();


        } else {
          alert(res.data.validation);
        }
      })
  }

  updateIngredient2() {
    console.log('idce', this.state.idIngredientName)
    var formData = new FormData();
    // formData.append('name', this.state.idIngredientName);
    //  this.state.idIngredientName = null;
    formData.append('price_per_unit', this.state.idPrice);
    //  this.state.idPrice = null;
    // formData.append('quantity', this.state.idQuantity);
    //  this.state.idQuantity = null;

    const id = this.state.idURL;

    defaultServerAddr.patch(`/ingredient/modify/ingredient/${id}`, formData, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)



          this.getRawmaterials();
          this.handleClose4();
          this.handleClose3();
          this.handleClose2();
          this.handleClose5();


        } else {
          alert(res.data.validation);
        }
      })
  }
  updateIngredient3() {
    console.log('idce', this.state.idIngredientName)
    var formData = new FormData();
    //   formData.append('name', this.state.idIngredientName);
    //  //  this.state.idIngredientName = null;
    //   formData.append('price_per_unit', this.state.idPrice);
    //  //  this.state.idPrice = null;
    //   formData.append('quantity', this.state.idQuantity);
    //  //  this.state.idQuantity = null;

    const id = this.state.idURL;

    defaultServerAddr.patch(`/ingredient/modify/ingredient/${id}`, formData, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)



          this.getRawmaterials();
          this.handleClose4();
          this.handleClose3();
          this.handleClose2();
          this.handleClose5();


        } else {
          alert(res.data.validation);
        }
      })
  }

  updateIngredient4() {
    console.log('idce', this.state.idIngredientName)
    var formData = new FormData();
    // formData.append('name', this.state.idIngredientName);
    //  this.state.idIngredientName = null;
    // formData.append('price_per_unit', this.state.idPrice);
    //  this.state.idPrice = null;
    formData.append('quantity', this.state.idQuantity);
    //  this.state.idQuantity = null;

    const id = this.state.idURL;

    defaultServerAddr.patch(`/ingredient/modify/ingredient/${id}`, formData, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)



          this.getRawmaterials();
          this.handleClose4();
          this.handleClose3();
          this.handleClose2();
          this.handleClose5();


        } else {
          alert(res.data.validation);
        }
      })
  }


  getData()
  {


        this.setState({ addNew: false, searchValue: ' ' });
        this.getRawmaterials();
        this.readIngredientCategories();
        this.readIngredientUnits();
        this.readExpiryDropdown();
        this.handleClick();

  }



toggleSelectAllLead(){
let newSelected = {};

    if (this.state.selectAll === 0) {
      this.state.userRows.forEach(x => {
        newSelected[x.id] = true;
      });
      console.log(newSelected,'newSelected');
    }
    this.setState((prevState, props) => ({
      selected: newSelected,
      selectAll: this.state.selectAll === 0 ? 1 : 0
    }),()=>{
      var total_task= [];
      for (const prop in newSelected) {
        if (newSelected.hasOwnProperty(prop)) {
          total_task.push(`${prop}`);
          console.log(`${prop}` ,total_task,'ids',prop);
          this.state.id = total_task;
        }
      }
      console.log(this.state.selected,total_task,'selected')
    })

}



  toggleRowLead(id) {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[id] = !this.state.selected[id];
    console.log(newSelected[id],'new')
    // console.log(newSelected);
    this.setState((prevState, props) => ({
      selected: newSelected,
      selectAll: 2
    }),()=>{
      var totle_task= [];
      for (const prop in this.state.selected) {
        if (this.state.selected.hasOwnProperty(prop)) {
          totle_task.push(`${prop}`);
          console.log(totle_task,'totaltask')
          console.log(totle_task.map(Number));
          this.state.task_list = totle_task.map(Number);
          console.log(this.state.task_list,"togglerowlead");
        }
      }
      console.log(this.state.selected,'selected', )
    })

  }



  render() {

    const classes = this.state;
    const { open } = this.state;
    const { open1 } = this.state;
    const { open2 } = this.state;
    const { open3 } = this.state;
    const { open4 } = this.state;
    const { open5 } = this.state;

    const { open66 } = this.state;


    const { open12 } = this.state;
    const { setOpen } = this.state;
    // console.log('salimm searvaluve',this.state.searchValue);

    const { categories, units, expiries, editItem, expanded } = this.state
    // if (this.state.showLoader) {
    //   return (
    //     < CircularProgress className='loader' />
    //   )
    // }

    var addnew;
    if (this.state.addNew) {
      addnew =
        <Paper  style={{ marginTop: '25px', marginBottom: '10px', paddingRight:'50px', paddingTop:'20px'}} >
          <Grid container spacing={16}  >
            <Grid item xs={3} >

              <TextField
                style={{width:'65%',paddingBottom:'15px'}}
                label="Ingredient Name"
                name="ingredientName"
                value={editItem.ingredientName}
                onChange={this.editInputValue}

              />
            </Grid>
            <Grid item xs={3}>
              <TextField
              type='number'
              style={{width:'65%'}}
                label="Price"
                name="price"
                value={editItem.price}
                onChange={this.editInputValue}

              />
            </Grid>
            <Grid item xs={3}>
              <TextField

                style={{ width: '65%' }}
                id="category"
                select
                label="Category"
                name='category'
                value={editItem.category}
                onChange={this.editInputValue}
              >

                <MenuItem onClick={() => { this.onOpenModal() }} >
                  <span onClick={this.onOpenModal}><span style={{ fontSize: '18px' }}>Create category</span>   &nbsp;&nbsp;
                                <Icon className={classes.icon} color="primary">
                      add_circle
                                </Icon></span>


                </MenuItem>
                {categories.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}


              </TextField>
            </Grid>
            {/* onClose={this.onCloseModal} */}
            <Modal open={open} onClose={this.onCloseModal} center>
              <div className={classes.root}>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Grid item xs={12}>
                      <Grid item xs={12}>
                        {this.state.idIngredientName}
                      </Grid>


                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Category"
                        name="new_Category"
                        value={this.state.newCategory}
                        onChange={this.editInputValue}

                      />

                      <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                          Cancel
                                      </Button>
                        <Button onClick={() => { this.addCategory() }} color="primary">
                          Create
                                      </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal>


            <Grid item xs={2}>
              <TextField
              style={{width:'90%'}}
              type="number"
                label="Quantity"
                name="quantity"
                value={editItem.quantity}
                onChange={this.editInputValue}

              />
            </Grid>

            <Grid item xs={1}>
              <TextField
                style={{ width: '70%' }}
                id="standard-select-currency"
                select
                label="Unit"
                name='unit'
                value={editItem.unit}
                onChange={this.editInputValue}
              >

                {units.map(option => (
                  <MenuItem key={option.name} value={option.uid}>
                    {option.name}
                  </MenuItem>
                ))}
                {/*<Modal open={open1} onClose={this.onCloseModal11} center>
                  <div className={classes.root}>
                    <Grid container spacing={16}>
                      <Grid item xs={12}>
                        <Grid item xs={12}>
                          <Grid item xs={12}>
                            {this.state.unit}
                          </Grid>


                          <TextField
                            style={{ width: '80%', margin: '20px' }}
                            label="Add New unit"
                            name="new_unit"
                            value={this.state.newUnit}
                            onChange={this.editInputValue}

                          />

                          <DialogActions>
                            <Button onClick={() => { this.handleClose11() }} color="primary">
                              Cancel
                                      </Button>
                            <Button onClick={() => { this.readIngredientUnits() }} color="primary">
                              Create
                                      </Button>
                          </DialogActions>
                        </Grid>
                      </Grid>
                    </Grid>
                  </div>
                </Modal>*/}

              </TextField>
            </Grid>

            <Grid item xs={3}>
              <TextField
            style={{width:'65%'}}
            type='number'
                label="Duration"
                name="duration"
                value={editItem.expiry.duration}
                onChange={this.editInputValue}
              />
            </Grid>
            


            <Grid item xs={3}>
            <TextField
                style={{ width: '65%' }}
                id="standard-select-currency"
                select
                label="Duration Term"
                name='duration_term'
                value={editItem.expiry.duration_term}
                onChange={this.editInputValue}
              >

                {this.state.expirys.map(option => (
                  <MenuItem key={option.name} value={option.nid}>
                    {option.name}
                  </MenuItem>
                ))}
                </TextField>
            </Grid>
          </Grid>

          <Divider style={{ marginTop: '80px' }} />
          <ExpansionPanelActions className='expansionpanelactions' >
            <Button size="small" onClick={() => this.clearFunction()} >Cancel</Button>
            <Button size="small" color="primary" onClick={() => this.postItem()} >
              Create
                              </Button>
          </ExpansionPanelActions>

        </Paper>
    } else {
      addnew = null;
    }

    return (
      <div>
      <Grid container>

                                <Grid item lg={10} md={9} sm={8} xs={7}></Grid>
                                <Grid item lg={2} md={3} sm={4} xs={5} >
                                  <div style={{marginTop:'-45px'}}>
                                    <UploadButton uploadname="ingredient" disable={false} getData={this.getRawmaterials.bind(this)}/>

                                  </div>
                                </Grid>
                              </Grid>

        {addnew}
      {/* <input onClick={() => { this.deleteAll() }} type="checkbox" name='All' defaultChecked={this.state.chkbox} checked={this.state.checkedItems.get()}

                    style={{ marginTop: '10px', width: '20px', height: '15px' }} >
                  </input>*/}
        <div className={classes.root}>
          <Paper className={classes.paper}>
            <Table className={classes.table} size="small" style={{ marginTop: "20px" }}>
              <TableHead>
                <TableRow>
                <TableCell><Checkbox onChange={() => this.toggleSelectAllLead()}></Checkbox> </TableCell>
                  <TableCell className="tabelcellhead">Ingredient Names</TableCell>
                  <TableCell className="tabelcellhead">Category</TableCell>

                  <TableCell align="center" className="tabelcellhead">Price</TableCell>
                  {/* <TableCell align="left">Category</TableCell> */}
                  <TableCell align="center" className="tabelcellhead">Quantity</TableCell>

                  <TableCell align="center" className="tabelcellhead" >Unit</TableCell>
                  <TableCell align="center" className="tabelcellhead">Duration</TableCell>
                  <TableCell align="center" className="tabelcellhead">Duration Term</TableCell>

                  {/* <TableCell align="left">Active</TableCell> */}
                  <span onClick={() => this.deletett()}>
                    <Grid open={open2} onClose={this.onCloseModal2} >
                      <div className='delt'>
                        <SvgMaterialIcons />
                      </div>
                    </Grid>
                  </span>
                </TableRow>
              </TableHead>
              <TableBody  >
                {this.state.userRows.map((prop, key) => (
                  <TableRow key={key}>


                    {/* <TableCell  align="left"><input type="checkbox"  name={row.name} checked={this.state.checkedItems.get(row.name)}
                                       onChange={this.handleChanges}
                                       style={{marginTop:'10px',width:'20px', height:'15px'}} >
      </input>{row.dishName}</TableCell> */}
    {/*  onClick={() => { this.onOpenModal2(prop.id) }}*/}
  {/* <input onChange={() => { this.handleChanges(prop.id) }} type="checkbox" name={prop.name} Checkbox={this.state.chkbox ? this.state.chkbox : this.state.checkedItems.get(prop.name)}

                      style={{ marginTop: '10px', width: '20px', height: '15px' }} >
                    </input>*/}
                    <TableCell> <Checkbox checked={this.state.selected[prop.id] === true} onChange={() => this.toggleRowLead(prop.id)}> 
</Checkbox></TableCell>
                    <TableCell onClick={() => { this.onOpenModal2(prop.id) }}  align="left" className="tabelcellbody">{prop.name}</TableCell>
                    <TableCell onClick={() => { this.onOpenModal2(prop.id) }}  align="left" className="tabelcellbody">{prop.category}</TableCell>

                    <TableCell onClick={() => { this.onOpenModal2(prop.id) }} align="center" className="tabelcellbody">{prop.price}</TableCell>
                    <TableCell onClick={() => { this.onOpenModal2(prop.id) }} align="center" className="tabelcellbody">{prop.quantity}</TableCell>

                    <TableCell onClick={() => { this.onOpenModal2(prop.id) }} align="center" className="tabelcellbody">{prop.unit}</TableCell>
                    <TableCell onClick={() => { this.onOpenModal2(prop.id) }} align="center" className="tabelcellbody">{prop.duration}</TableCell>
                    <TableCell onClick={() => { this.onOpenModal2(prop.id) }} align="center" className="tabelcellbody">{prop.durationt}</TableCell>

                    {/* <TableCell  align="left">{row.cuisine.name}</TableCell> */}
                    {/* <TableCell  align="left">
                                       <Switch
              checked={this.state.checkedB}
              onChange={this.handleChange6}
              value={checkedB}
              color="primary"
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
            </TableCell> */}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <span style={{ align: 'center', marginLeft: '80%' }} >
              <Modal open={open2} onClose={this.onCloseModal2} center style={{ align: 'center', marginLeft: '30%' }}>
                <div className={classes.root} >

                  <Grid container spacing={16}
                    style={{ marginTop: '2px', marginBottom: '-12px', height: "61px", width: '746px', marginRight: '-18px' }}>

                    {/* <Grid item xs={12}> */}
                    <Grid item xs={3} >

                      <TextField
                        label="Edit Ingredient Name"
                        name="idIngredientName"
                        value={this.state.idIngredientName}
                        onChange={this.editInputValue}

                      />
                    </Grid>
                    <Grid item xs={3}>
                      <TextField
                        label="Edit Price"
                        name="idPrice"
                        value={this.state.idPrice}
                        onChange={this.editInputValue}

                      />
                    </Grid>
                    <Grid item xs={3}>
                      <TextField
                        style={{ width: '90%' }}
                        id="category"
                        select
                        label="Edit Category"
                        name='idCategory'     // wwwwwwwwwwoooooorkkkkkkkkk iiiis pendinngeeeeeeeee
                        value={editItem.category}
                        onChange={this.editInputValue}
                      >

                        <MenuItem onClick={() => { this.onOpenModal() }} >
                          <span onClick={this.onOpenModal}><span style={{ fontSize: '18px' }}>Create category</span>   &nbsp;&nbsp;
                              <Icon className={classes.icon} color="primary">
                              add_circle
                              </Icon></span>


                        </MenuItem>
                        {categories.map(option => (
                          <MenuItem key={option.name} value={option.id}>
                            {option.name}
                          </MenuItem>
                        ))}


                      </TextField>
                    </Grid>
                    {/* onClose={this.onCloseModal} */}
                    <Modal open={open} onClose={this.onCloseModal} center>
                      <div className={classes.root}>
                        <Grid container spacing={16}>
                          <Grid item xs={12}>
                            <Grid item xs={12}>
                              <Grid item xs={12}>
                                {this.state.idIngredientName}
                              </Grid>


                              <TextField
                                style={{ width: '80%', margin: '20px' }}
                                label="Add New Category"
                                name="new_Category"
                                value={this.state.newCategory}
                                onChange={this.editInputValue}

                              />

                              <DialogActions>
                                <Button onClick={this.handleClose} color="primary">
                                  Cancel
                                    </Button>
                                <Button onClick={() => { this.addCategory() }} color="primary">
                                  Create
                                    </Button>
                              </DialogActions>
                            </Grid>
                          </Grid>
                        </Grid>
                      </div>
                    </Modal>


                    <Grid item xs={2}>
                      <TextField
                        label="Quantity"
                        name="idQuantity"
                        value={this.state.idQuantity}
                        onChange={this.editInputValue}

                      />
                    </Grid>

                    <Grid item xs={1}>
                      <TextField
                        style={{ width: '100%' }}
                        id="unit"
                        select
                        label="Unit"
                        name='idUnit'
                        value={this.state.idUnit}
                        onChange={this.editInputValue}
                      >

                        <MenuItem onClick={() => { this.onOpenModal11() }} >
                          <span onClick={this.onOpenModal11}><span style={{ fontSize: '18px' }}>Create unit</span>   &nbsp;&nbsp; <Icon className={classes.icon} color="primary">
                            add_circle
                              </Icon></span>


                        </MenuItem>
                        {units.map(option => (
                          <MenuItem key={option.name} value={option.id}>
                            {option.name}
                          </MenuItem>
                        ))}
                        <Modal open={open1} onClose={this.onCloseModal11} center>
                          <div className={classes.root}>
                            <Grid container spacing={16}>
                              <Grid item xs={12}>
                                <Grid item xs={12}>
                                  <Grid item xs={12}>
                                    {this.state.unit}
                                  </Grid>


                                  <TextField
                                    style={{ width: '80%', margin: '20px' }}
                                    label="Add New unit"
                                    name="new_unit"
                                    value={this.state.newUnit}
                                    onChange={this.editInputValue}

                                  />

                                  <DialogActions>
                                    <Button onClick={() => { this.handleClose11() }} color="primary">
                                      Cancel
                                    </Button>
                                    <Button onClick={() => { this.readIngredientUnits() }} color="primary">
                                      Create
                                    </Button>
                                  </DialogActions>
                                </Grid>
                              </Grid>
                            </Grid>
                          </div>
                        </Modal>

                      </TextField>
                    </Grid>

                    <Grid item xs={3}>
                      <TextField
                        label="edit Duration"
                        name="duration"
                        value={editItem.expiry.duration}
                        onChange={this.editInputValue}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <TextField
                        style={{ width: '80%' }}
                        id="standard-select-currency"
                        // select
                        style={{ width: '90%' }}
                        label="Duration Term"
                        name='duration_term'
                        value={editItem.expiry.duration_term}
                        onChange={this.editInputValue}
                      >
                        {this.state.expirys.map(option => (
                          <MenuItem key={option.name} value={option.name}>
                            {option.name}
                          </MenuItem>
                        ))}
                      </TextField>

                    </Grid>
                  </Grid>
                </div>
                <Divider style={{ marginTop: '80px' }} />
                <ExpansionPanelActions className='expansionpanelactions' >
                  <Button size="small" onClick={() => this.handleClose2} >Cancel</Button>
                  <Button size="small" color="primary" onClick={() => this.updateIngredient1()} >
                    Create
                              </Button>
                </ExpansionPanelActions>
              </Modal>
            </span>

           
          </Paper>
        </div>




        <Grid item xs={6} style={{ marginTop: '15px', }}>
          <div className='butt'>
            {this.state.pages <= 1 ? (null) : (<Button style={{ marginLeft: '10px' }} onClick={this.buttprevious.bind(this)} variant="outline-primary">Previous</Button>)}
          </div>
        </Grid>
        <Grid item xs={6} style={{
          float: 'right', marginRight: '68px',
          marginTop: '33px'
        }}>
          <div>
            {this.state.pages >= this.state.max ? (null) : (<span id="nam"><Button onClick={this.buttnext.bind(this)} id='possition' variant="outline-primary" style={{ marginTop: '-20px' }} >Next</Button></span>)}

          </div>
        </Grid>
      </div>
    )
  }
}
export default RawMaterial

      // onClick={() => this.deleteIngredient(prop.ingredientId)}
