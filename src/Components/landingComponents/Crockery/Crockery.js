import React, { Component } from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import '../Packages/Packages.css'
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { ServerAddr } from '../../../NetworkConfig';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Modal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import DialogActions from '@material-ui/core/DialogActions';
import { serverip } from '../../../NetworkConfig.js';
import '../Uniform/Uniform.css';
import { Slide } from 'react-slideshow-image';
import { SketchPicker } from 'react-color';

import UploadButton from '../UploadButton/UploadButton'



const access = window.localStorage.getItem('access');



const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  icon: {
    margin: theme.spacing(2),
    float: 'right',
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
  },
});

const properties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  arrows: true,
  onChange: (oldIndex, newIndex) => {
    console.log(`slide transition from ${oldIndex} to ${newIndex}`);
  }
}


class Crockery extends React.Component {
  state = {
    items: [],
    showLoader: false,
    expanded: null,
    editItem: {
      'name': '',
      'max_dishes': '',
      'image': '',
      'category': '',
      'image_type': '',
      'video': '',
      'description': '',


    },
    searchValue: this.props.searchresult.searchvalue,
    addNew: this.props.searchresult.createNew,
    open: false,
    open1: false,
    open2: false,
    uniformCategory: [],
    uniformType: [],
    uniform: [],
    uniforms: [],
    myFile: [],
    createUniform: [],
    newCategory: '',
    addNewCategory: [],
    newType: '',
    deleteUniform: '',
    max: 5,
    pages: 1,
  };

  componentDidMount() {
    this.setState({
      addNew: false,
      searchValue: null,

    });
    //    this.createUniformType();
    this.readUniformType();
    this.readUnifrom();
    this.readUniformCategory();




  }
  getData() {
    this.setState({
      addNew: false,
      searchValue: null,
    });
    this.readUniformType();
    this.readUnifrom();
    this.readUniformCategory();
  }
  clearFunction() {
    this.setState({ addNew: false });
  }


  componentWillReceiveProps(nextProps) {
    // console.log('componentWillReceiveProps', nextProps);
    if (nextProps.searchresult.createNew) {
      this.setState({ addNew: nextProps.searchresult.createNew })

    }
    else {
      this.setState({ searchValue: nextProps.searchresult.searchvalue, })
      // this.getRawmaterials();
    }

  }
  Slideshow = (items) => {
    return (
      <div className="slide-container">
        {console.log(items, 'slidee')}
        <Slide {...properties}>
          {
            items.map((paigee, key) => {

              return (<div className="each-slide">
                <div style={{ 'backgroundImage': `url(${paigee.thumbnail})` }}>
                  <span>{key + 1}</span>
                </div>
              </div>)
            })}

        </Slide>
      </div>
    )
  }



  editInputValue = event => {
    console.log('in edit input ', event);

    let editItem = this.state.editItem;
    const Key = event.target.name;
    const value = event.target.value;
    console.log(value)






    editItem[event.target.name] = event.target.value;
    this.setState({
      editItem
    });

    if (Key == 'image[]') {
      const data = new FormData();

      // for (var x = 0; x < event.target.files.length; x++) {

      for (const file of event.target.files) {
        data.append('image', file, file.name);
        console.log(...data, 'formdatatata');
        this.postItem(data);

      }
    }

    if (Key == 'image[[]]') {

      // this.setState({
      //   image: event.target.files[0],
      // })
      // console.log(event.target.files[0], 'imageee')
      // console.log(event.target.files[1], 'imageee')

      // console.log(event.target.files.length, 'imageee')
      const data = new FormData();

      // for (var x = 0; x < event.target.files.length; x++) {

      for (const file of event.target.files) {
        data.append('image', file, file.name);
        console.log(file.name, 'formdatatata');
        this.postItemUpdate(data);

      }
      console.log(...data, 'finalformdatta ');

      // }
      //add to list
      // var li = document.createElement('li');
      // li.innerHTML = 'File ' + (x + 1) + ':  ' + input.files[x].name;
      // list.append(li);
      // console.log(event.target.files[x], 'imageee')

      // this.setState({
      //   image: [event.target.files[x]],
      // }, () => { console.log(this.state.image, 'imageeeimage') });
      // let val = event.target.files[x];
      // var joined = this.state.image.concat(val);
      //  this.setState({ image: joined })




      // }

      console.log(this.state.image, 'imageeeimage')

    }
    if (Key == 'video') {
      this.setState({
        video: event.target.files[0],
      })
      console.log(this.state.image, 'image')
    }

    if (Key == 'newCategory') {
      this.setState({
        newCategory: event.target.value,
      });
      return
    }
    if (Key == 'description') {
      this.setState({
        description: event.target.value,
      });
      return
    }

    if (Key == 'newType') {
      this.setState({
        newType: event.target.value,
      });
      return
    }






    editItem[event.target.name] = event.target.value;
    this.setState({
      editItem
    });
    console.log(editItem, 'item', event.target.name, event.target.value)

  }









  createUniformCategory() {
    const datas = {
      // "uniformCategoryId": null,
      "name": this.state.newCategory,
      //  "name":'salim',
    }
    defaultServerAddr.post(`${serverip}/crockery/create/crockery/category/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log(' ', res)
        if (res) {
          console.log('web/create_update_category_crockery/', res);
          this.setState({
            uniform: res.data.data['name']

          })

          this.readUniformCategory();


        } else {
          alert(res.data.validation);
        }
      })
    this.handleClose1()

  }




  readUniformCategory() {
    const datas = {
      // "uniformCategoryId": null,
      "name": ""
    }
    defaultServerAddr.get(`${serverip}/crockery/read/crockery/category/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/crockery/read/crockery////', res)
        if (res) {
          console.log('web/read-crockery-category/', res);
          this.setState({
            uniformCategory: res.data['data']
          })

        } else {
          alert(res.data.validation);
        }
      })
  }




  createUniformType() {
    const datas = {
      // "uniformTypeId": null,
      "name": this.state.newType,

    }
    defaultServerAddr.post(`${serverip}/crockery/create/crockery/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('exp', res)
        if (res) {
          console.log('web/create-update-crockery-type/', res);
          this.setState({
            uniforms: res.data['data']
          })
          this.readUniformType();


        } else {
          alert(res.data.validation);
        }
      })
  }

  readUniformType() {
    const datas = {
      // "uniformTypeId": null,
      "name": ""
    }
    defaultServerAddr.get(`${serverip}/crockery/read/crockery/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('exp', res)
        if (res) {
          console.log('web/read-crockery-type/', res);
          this.handleClose2();

          this.setState({
            uniformType: res.data['data']
          })

        } else {
          alert(res.data.validation);
        }
      })
  }


  readUnifrom() {


    defaultServerAddr.get(`${serverip}/crockery/read/crockery/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('exp', res)
        if (res) {
          console.log('web/read-crockery-images/', res);
          this.setState({
            items: res.data['data']
          })

        } else {
          alert(res.data.validation);
        }
      })
  }



  postItem(data) {
    // console.log(this.state, 'salimm');
    // var formData = new FormData();
    // formData.append('image', this.state.image);
    // formData.append('type',this.state.editItem['image_type']);
    // formData.append('category',this.state.editItem['category']);
    // formData.append('title',this.state.editItem['name']);
    // formData.append('description',this.state.editItem['description']);



    // console.log(...formData, 'read formdata')
    // const datas ={
    //   "image":this.state.editItem['image'],
    //   "category":1,
    //   "image_type":1,
    //   "name":'test'
    // }
    // web/create-update-uniform-image/
    // console.log(...formData, 'formdata')


    defaultServerAddr.post(`http://34.93.7.13:8002/media/cdn/upload/image/`, data, {
      headers: {
        'Content-Type': "multipart/form-data;",
        // Accept: "multipart/form-data",

        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('exp', res)
        if (res) {
          console.log('web/create-update-crockery-image/', res);
          this.setState(prevState => ({
            createUniform: [...prevState.createUniform, res.data.data['id']]
          }))
          // this.postItemAll();
        } else {
          alert(res.data.validation);
        }
      })
  }

  postItemAll() {

    const data = {
      'image': this.state.createUniform,
      'type': this.state.editItem['image_type'],
      'category': this.state.editItem['category'],
      'name': this.state.editItem['name'],
      // 'description': this.state.editItem['Description'],

    }
    defaultServerAddr.post(`${serverip}/crockery/create/crockery/`, data, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)

          this.setState({
            image: null,

          });


          this.readUnifrom();


        } else {
          alert(res.data.validation);
        }
      })
  }

  postItemUpdate(data) {
    // var formData = new FormData();
    // formData.append('image', this.state.image);
    // formData.append('image_type',this.state.editItem['image_type']);
    // formData.append('category',this.state.editItem['category']);
    // formData.append('name',this.state.editItem['name']);
    // formData.append('description',this.state.editItem['Description']);
    // formData.append('video',this.state.editItem['video']);




    // web/create-update-uniform-image/
    // console.log(...formData, 'formdata')
    // http://34.93.7.13:8002/media/cdn/upload/image/

    defaultServerAddr.post(`http://34.93.7.13:8002/media/cdn/upload/image/`, data, {
      headers: {
        'Content-Type': "multipart/form-data;",
        // Accept: "multipart/form-data",

        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/media/cdn/upload/image/', res)
        if (res) {
          // console.log('web/create-update-uniform-image/', res.data.data.id);
          this.setState(prevState => ({
            createUniform: [...prevState.createUniform, res.data.data['id']]
          }))
          console.log('web/create-update-uniform-image/', this.state.createUniform);

          // this.postItemAllUpdate(item);
          // this.readUnifrom();


        } else {
          alert(res.data.validation);
        }
      })



  }

  postItemAllUpdate(rec) {
    const data = {
      'image': this.state.createUniform,
      'type': this.state.editItem['image_type'],
      'category': this.state.editItem['category'],
      'name': this.state.editItem['name'],
      // 'description': this.state.editItem['Description'],

    }
    console.log('runbhua', rec);
    const id = rec;

    defaultServerAddr.patch(`/crockery/modify/crockery/${id}`, data, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)

          this.setState({ image: null })


          this.readUnifrom();


        } else {
          alert(res.data.validation);
        }
      })

  }

  deleteUniform(item) {

    const id = item;
    defaultServerAddr.delete(`/crockery/modify/crockery/${id}`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        // this.setState({ addNew: false, image:''});

        if (res) {
          console.log('/media/cdn/upload/image///', res)



          this.readUnifrom();


        } else {
          alert(res.data.validation);
        }
        this.setState({ open11: false, });
        this.handleClick();
        this.readUnifrom();
      })

  }
  handleClick() {
    this.setState({ setOpen: true });
    console.log('kyabe', this.state.setOpen)
  }

  onOpenModal1 = () => {
    this.setState({ open1: true });
    console.log(this.state.open1, 'open')
  };

  onCloseModal1 = () => {
    this.setState({ open1: false });
  };

  handleClose1 = () => {
    this.setState({ open1: false });
  }


  onOpenModal2 = () => {
    this.setState({ open2: true });
    console.log(this.state.open2, 'open')
  };

  onCloseModal2 = () => {
    this.setState({ open2: false });
  };

  handleClose2 = () => {
    this.setState({ open2: false });
  }
  onOpenModal11 = () => {
    this.setState({ open11: true });
    console.log(this.state.open11, 'open')
  };

  onCloseModal11 = () => {
    this.setState({ open11: false });
  };

  handleClose11 = () => {
    this.setState({ open11: false });
  }

  handleChange = panel => (event, expanded) => {
    console.log('event', event);
    console.log('expanded', expanded);
    console.log('panel', panel);


    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  setInputValue = (data) => {

    var input = {
      name: data.name,
      images: data.image,
      category: data.category,
      image_type: data.type,
    }
    this.setState({
      editItem: input,
      deleteUniform: data.crockeryImageId

    });
    console.log(data.crockeryImageId, 'datato')
  };

  buttnext() {
    //   console.log('salimm');
    if (this.state.page === 10) {
      console.log('next end here');
      return true;
    }
    else {

      console.log('next plus one');
      this.setState({
        pages: this.state.pages + 1
      });
    };
  }
  // colorpicker(event){
  //   console.log('color',event.target.value);
  // }

  buttprevious() {
    //   console.log('salimm');

    if (this.state.pages === 1) {
      console.log('previous button end here');

      return true;
    }
    else {
      console.log('previous minus one hua');
      this.setState({
        pages: this.state.pages - 1
      });
    }

  }





  render() {
    const classes = this.state;
    const { open } = this.state;
    const { open1 } = this.state;
    const { open2 } = this.state;
    const { open11 } = this.state;
    const { myFile } = this.state;



    const { categories, units, expiries, editItem, expanded } = this.state
    if (this.state.showLoader) {
      return (
        < CircularProgress className='loader' />
      )
    }

    var addnew;
    if (this.state.addNew) {
      addnew =
        <Paper style={{ marginTop: '15px', marginBottom: '10px' }} >
          <Grid container spacing={16}  >

            <Grid item xs={4}>

              <TextField
                label="Crockery Name"
                name="name"
                value={editItem.name}
                onChange={this.editInputValue}

              />
            </Grid>


            {/* <Modal open={open2} onClose={this.onCloseModal2} center>
                        <div className={classes.root}>
                          <Grid container spacing={16}>
                            <Grid item xs={12}>
                              <Grid item xs={12}>

                                <TextField
                                  style={{width:'80%',margin:'20px'}}
                                  label="Add New Type"
                                  name="newType"
                                  value={this.state.newType}
                                  onChange={this.editInputValue}

                                />



                                <DialogActions>
                                  <Button  onClick={this.handleClose2} color="primary">
                                    Cancel
                                  </Button>
                                  <Button onClick={()=>{this.createUniformType()}} color="primary">
                                    Create
                                  </Button>
                                </DialogActions>
                              </Grid>
                            </Grid>
                          </Grid>
                        </div>
                      </Modal> */}

            <Grid item xs={3}>

              <TextField
                style={{ width: '100%' }}
                id="Crockery Category"
                select
                label="Crockery Category"
                name="category"
                value={editItem.category}
                onChange={this.editInputValue}

              >
                <MenuItem onClick={() => { this.onOpenModal1() }} >
                  <span onClick={this.onOpenModal1}><span style={{ fontSize: '18px' }}>Create Category</span>   &nbsp;&nbsp;
                              <Icon className={classes.icon} color="primary">
                      add_circle
                              </Icon>
                  </span>


                </MenuItem>
                {this.state.uniformCategory.map(option => (
                  <MenuItem key={option.name} value={option.id}>
                    {option.name}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>

            <Modal open={open1} onClose={this.onCloseModal1} center>
              <div className={classes.root}>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Grid item xs={12}>

                      <TextField
                        style={{ width: '80%', margin: '20px' }}
                        label="Add New Category"
                        name="newCategory"
                        value={this.state.newCategory}
                        onChange={this.editInputValue}

                      />



                      <DialogActions>
                        <Button onClick={this.handleClose1} color="primary">
                          Cancel
                                  </Button>
                        <Button onClick={() => { this.createUniformCategory() }} color="primary">
                          Create
                                  </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </div>
            </Modal>


            <Grid item xs={3} >
              <input
                // accept="image/*"
                // className={classes.input}
                style={{ display: 'none' }}
                id="contained-button-file"
                multiple
                // multiple=""
                type="file"
                name='image[]'
                value={editItem.image}
                onChange={this.editInputValue}
              /><label htmlFor="contained-button-file">
                <Button variant="contained" component="span"
                  // className={classes.button}
                  style={{ width: '65%', padding: '10px' }}
                >
                  Upload
    </Button>
              </label>
            </Grid>





          </Grid>

          <Divider style={{ marginTop: '45px' }} />
          <ExpansionPanelActions className='expansionpanelactions' >
            <Button size="small" onClick={() => this.clearFunction()} >Cancel</Button>
            <Button size="small" color="primary" onClick={() => this.postItemAll()} >
              Create
                    </Button>
          </ExpansionPanelActions>

        </Paper>
    } else {
      addnew = null;
    }

    return (
      <div>
        {addnew}
        <Grid container>

          <Grid item lg={10} md={9} sm={8} xs={7}></Grid>
          <Grid item lg={2} md={3} sm={4} xs={5} >
            <div style={{ marginTop: '-45px' }}>
              <UploadButton uploadname="crockery" disable={false} getData={this.getData.bind(this)} />

            </div>
          </Grid>
        </Grid>
        {/* <p>{this.props.searchresult}</p> */}
        {this.state.items.map((prop, key) => {
          return (
            <ExpansionPanel key={key} expanded={expanded === key} onChange={this.handleChange(key)} >
              <ExpansionPanelSummary onClick={() => this.setInputValue(prop)} style={{ marginTop: '10px' }} expandIcon={<ExpandMoreIcon />}
              >
                {/*<Grid container spacing={16}  >
                          <Grid item xs={6}>
                          <p className='parass' > {prop.dishName}</p>
                          <div className='categorystyle' >
                          <Chip style={{ height: '22px' }} label={prop.typeDish.name} variant="outlined" />
                        </div>
                      </Grid>
                      <Grid item xs={3}>
                      <Paper className='dayspaper' >
                      <p>{prop.cuisine.name}  </p> </Paper>
                    </Grid>
                    <Grid item xs={3}>
                    <p>  {prop.category.name}  </p>
                  </Grid>
                  <Grid item xs={3}>
                </Grid>
              </Grid>*/}
                <Grid container spacing={16}  >
                  <Grid item xs={3} className="imgClass">
                    {

                      this.Slideshow(prop.image)

                    }


                  </Grid>

                  <Grid item xs={4}>
                    <h4>Name</h4>

                    <p>{prop.name}  </p>
                  </Grid>

                  <Grid item xs={4}>
                    <h4>Category</h4>
                    <p>{prop.category['name']}  </p>
                  </Grid>




                </Grid>
              </ExpansionPanelSummary>
              <Divider />

              <ExpansionPanelDetails>




                <Grid container spacing={16} >

                  <Grid item xs={3}>

                    {/* <button className="upload"><input style={{width:'100%', padding:'10px'}} type="file" name="image" value={this.state.Image} onChange={this.editInputValue}/>
</button> */}
                    <input
                      // accept="image/*"
                      // className={classes.input}
                      style={{ display: 'none' }}
                      id="contained-button-file"
                      multiple
                      // multiple=""
                      type="file"
                      name='image[[]]'
                      value={editItem.image}
                      onChange={this.editInputValue}
                    /><label htmlFor="contained-button-file">
                      <Button variant="contained" component="span"
                        // className={classes.button}
                        style={{ width: '65%', padding: '10px' }}
                      >
                        Upload
    </Button>
                    </label>

                  </Grid>
                  <Grid item xs={3}>

                    <TextField
                      label="Crockery Name"
                      name="name"
                      value={editItem.name}
                      onChange={this.editInputValue}

                    />
                  </Grid>

                  <Grid item xs={3}>
                    <TextField
                      style={{ width: '100%' }}
                      id="Uniform Type"
                      select
                      label="Crockery Category"
                      name='category'
                      value={editItem.category}
                      onChange={this.editInputValue}
                    >

                      <MenuItem onClick={() => { this.onOpenModal1() }} >
                        <span onClick={this.onOpenModal1}><span style={{ fontSize: '18px' }}>Create Category</span>   &nbsp;&nbsp;
      <Icon className={classes.icon} color="primary">
                            add_circle
      </Icon>
                        </span>


                      </MenuItem>
                      {this.state.uniformCategory.map(option => (
                        <MenuItem key={option.name} value={option.id}>
                          {option.name}
                        </MenuItem>
                      ))}
                    </TextField>
                  </Grid>
                  {/*onClose={this.onCloseModal}*/}
                  <Modal open={open1} onClose={this.onCloseModal1} center>
                    <div className={classes.root}>
                      <Grid container spacing={16}>
                        <Grid item xs={12}>
                          <Grid item xs={12}>

                            <TextField
                              style={{ width: '80%', margin: '20px' }}
                              label="Add New Category"
                              name="newCategory"
                              value={this.state.newCategory}
                              onChange={this.editInputValue}

                            />



                            <DialogActions>
                              <Button onClick={this.handleClose1} color="primary">
                                Cancel
          </Button>
                              <Button onClick={() => { this.createUniformCategory() }} color="primary">
                                Create
          </Button>
                            </DialogActions>
                          </Grid>
                        </Grid>
                      </Grid>
                    </div>
                  </Modal>





                  {/*onClose={this.onCloseModal}*/}
                  {/* <Modal open={open2} onClose={this.onCloseModal2} center>
<div className={classes.root}>
  <Grid container spacing={16}>
    <Grid item xs={12}>
      <Grid item xs={12}>

        <TextField
          style={{width:'80%',margin:'20px'}}
          label="Add New Type"
          name="newType"
          value={this.state.newType}
          onChange={this.editInputValue}

        />



        <DialogActions>
          <Button  onClick={this.handleClose2} color="primary">
            Cancel
          </Button>
          <Button onClick={()=>{this.createUniformType()}} color="primary">
            Create
          </Button>
        </DialogActions>
      </Grid>
    </Grid>
  </Grid>
</div>
</Modal> */}










                </Grid>

              </ExpansionPanelDetails>
              <Divider />
              <ExpansionPanelActions className='expansionpanelactions' >
                <Button size="small" color="primary" onClick={() => this.postItemAllUpdate(prop.id)} >
                  Update
                            </Button>


                <Button size="small" color="secondary" onClick={() => { this.onOpenModal11() }}>
                  Delete
                            </Button>
                <Modal open={open11} onClose={this.onCloseModal11} center>
                  <div className={classes.root}>
                    <Grid container spacing={16}>
                      <Grid item xs={12}>
                        <Grid item xs={12}>

                          <h4 className="modalClassText">Do you want to delete this Item</h4>


                          <DialogActions>
                            <Button onClick={this.handleClose11} color="primary">
                              Cancel
                                        </Button>
                            <Button color="secondary" onClick={() => this.deleteUniform(prop.id)} color="primary">
                              Delete
                                        </Button>
                          </DialogActions>


                        </Grid>
                      </Grid>
                    </Grid>
                  </div>
                </Modal>
              </ExpansionPanelActions>
            </ExpansionPanel>



          );
        })}
        <Grid item xs={6} style={{ marginTop: '15px', }}>
          <div className='butt'>
            {this.state.pages <= 1 ? (null) : (<Button style={{ marginLeft: '10px' }} onClick={this.buttprevious.bind(this)} variant="outline-primary">Previous</Button>)}
          </div>
        </Grid>
        <Grid item xs={6} style={{
          float: 'right', marginRight: '68px',
          marginTop: '33px'
        }}>
          <div>
            {this.state.pages >= this.state.max ? (null) : (<span id="nam"><Button onClick={this.buttnext.bind(this)} id='possition' variant="outline-primary" style={{ marginTop: '-20px' }} >Next</Button></span>)}

          </div>
        </Grid>
      </div>
    )
  }
}
export default Crockery
