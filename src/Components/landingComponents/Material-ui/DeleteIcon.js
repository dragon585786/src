import React from 'react';
import Grid from '@material-ui/core/Grid';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import { spacing } from '@material-ui/system';



const useStyles = theme => ({
  root: {
    // color: theme.palette.text.primary,
    color: red[800]
  },
  icon: {
    // margin: theme.spacing(1),
    fontSize: 32,
    spacing: 6, 
  },
});

export default function SvgMaterialIcons() {
  const classes = useStyles();

  return (
    <Grid container className={classes.root}>
     
      <Grid item xs={8} mt={2}>
        <DeleteIcon  className={classes.icon} />
      </Grid>
     
      
    </Grid>
  );
}
