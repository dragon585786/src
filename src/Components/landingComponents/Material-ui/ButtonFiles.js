import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

// const styles = theme => ({
//     button: {
//         margin: theme.spacing(1),
//     },
//     input: {
//         display: 'none',
//     },
// });

export default function ContainedButtons() {
    // const classes = styles();

    return (
        <div>
            <input
                accept="image/*"
                // className={classes.input}
                style={{ display: 'none' }}
                id="contained-button-file"
                multiple
                type="file"
            />
            <label htmlFor="contained-button-file">
                <Button variant="contained" component="span"
                // className={classes.button}
                >
                    Upload
        </Button>
            </label>
        </div>
    );
}
