import React, { Component } from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import './Role.css'
import { serverip } from '../../../NetworkConfig.js';

import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { ServerAddr } from '../../../NetworkConfig'
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Modal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import DialogActions from '@material-ui/core/DialogActions';
// import { serverip } from '../../../NetworkConfig.js';
import '../Uniform/Uniform.css';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';

import Checkboxes from '../Material-ui/Checked';
import SvgMaterialIcons from '../Material-ui/DeleteIcon';

import UploadButton from '../UploadButton/UploadButton'

const access = window.localStorage.getItem('access');



const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  icon: {
    margin: theme.spacing(2),
    float: 'right',
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
    modalmargin: {
      margin: '60px'
    }
  },
});


//
// function createData(name, calories, fat, carbs, protein) {
//   return { name, calories, fat, carbs, protein };
// }

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData("Admin", "this is Admin"),
  createData("Manager", "this is Manager"),
  createData("Employee", "this is Employee"),




];
// const idName = rows.name;
const permissionss = [
  // createData('users', 1, 2, 2),
  // createData('role', "2", "2", "4"),
  createData('ingredient', "3", "2", "4"),
  createData('dishes', "3", "2", "4"),
  createData('packages', "4", "2", "4"),
  createData('uniform', "4", "2", "4"),
  createData('crockery', "4", "2", "4"),
  createData('setup', "5", "2", "4"),
  createData('inventory', "5", "2", "4"),
  createData('icalender', "6", "2", "4"),
  // createData('customers', "7", "2", "4"),
  createData('event', "7", "2", "4"),



];
let foo = 0;

class Users extends React.Component {
  state = {
    items: [],
    selectAll:0,
    showLoader: false,
    expanded: null,
    editItem: {
      'user': '',
      'name': '',
      'images': '',
      'category': '',
      'image_type': '',


    },
    selected :{},
    selectedModal:{},
    selectedModal1:{},
    selectedModal2:{},
    getRole: [],
    permissionss: [],
    searchValue: this.props.searchresult.searchvalue,
    addNew: this.props.searchresult.createNew,
    open1: false,
    open2: false,
    user: '',
    'role': '',
    'description': '',
    'idName': '',
    checkedItems: new Map(),
    admin: true,
    manager: true,
    employee: true,
    arr: [],
    uniqueNames: [],
    moduleList:[{
      modulename:'Ingredient',
      id:1
    },
    {
    modulename:'Dishes',
    id:2
    },
    {
    modulename:'Packages',
    id:3
    },
    {
    modulename:'Uniform',
    id:4
    },{
    modulename:'Crockery',
    id:5
    },{
    modulename:'Setup',
    id:6
    },{
    modulename:'Inventory',
    id:7
    },
    {
    modulename:'Calender',
    id:8
    },
    {
    modulename:'Event',
    id:9
    },
    ],


  };

  componentDidMount() {
    this.setState({
      addNew: false,
      searchValue: null,
    });
    this.readUniformCategory();
  }
  clearFunction() {
    this.setState({ addNew: false });
  }


  componentWillReceiveProps(nextProps) {
    // console.log('componentWillReceiveProps', nextProps);
    if (nextProps.searchresult.createNew) {
      this.setState({ addNew: nextProps.searchresult.createNew })

    }
    else {
      this.setState({ searchValue: nextProps.searchresult.searchvalue, })
      // this.getRawmaterials();
    }

  }



  editInputValue = event => {
    console.log('in edit input ', event);

    let editItem = this.state.editItem;
    const Key = event.target.name;
    const value = event.target.value;
    console.log(value)






    editItem[event.target.name] = event.target.value;
    this.setState({
      editItem
    });
    console.log('role', this.state.editItem);

    if (Key == 'image') {
      this.setState({
        image: event.target.files[0],
      })
      console.log(this.state.image, 'image')
    }




    if (Key == 'newCategory') {
      this.setState({
        newCategory: event.target.value,
      });

      return
    }
    if (Key == 'role') {
      this.setState({
        role: event.target.value,
      });
      console.log(this.state.role, 'role')

      return
    }
    if (Key == 'discrption') {
      this.setState({
        discription: event.target.value,
      });
      console.log(this.state.discription, 'discription')

      return
    }

    if (Key == 'newType') {
      this.setState({
        newType: event.target.value,
      });
      return
    }



    if (Key == 'inputImage') {
      this.setState({
        inputImage: event.target.files[0],
        image: this.state.inputImage,
      })

    }






    editItem[event.target.name] = event.target.value;
    this.setState({
      editItem
    });
    console.log(editItem, 'item', event.target.name, event.target.value)

  }





  createUniformCategory() {

    // const value = this.state.editItem
    const datas = {
      "id": '',
      "name": this.state.role,
      'permissions': this.state.permissionss,
    }
    defaultServerAddr.post(`/profile/create/group/`, datas, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/profile/create/group/', res)
        if (res) {
          // console.log('web/create-update-uniform-category/', res);
          // this.setState({
          //   read_role: res.data['data'],
          //   read_discription: 2,
          // })
          // console.log(this.state.editItem, 'hellojs')

          this.readUniformCategory();


        } else {
          alert(res.data.validation);
        }
      })
    this.handleClose1()

  }



  readUniformCategory() {
    // const datas={
    //   // "uniformCategoryId": null,
    //   "name":""
    // }
    defaultServerAddr.get(`/profile/read/group/`, {
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        "Authorization": `Bearer ${access}`
      }
    })

      .then(res => {
        console.log('/setup/read/setup/mn', res)
        if (res) {
          console.log('web/read-uniform-category/', res);
          //   res.data.data.map((prop,id) =>{
          //     console.log(prop,'/setup/read/setup/')

          //   })
          this.setState({
            getRole: res['data']
          })
          console.log('/setup/hellojs/setup/', this.state.getRole)

        } else {
          alert(res.data.validation);
        }
      })
  }






  handleClick() {
    this.setState({ setOpen: true });
    console.log('kyabe', this.state.setOpen)
  }

  onOpenModal1 = () => {
    this.setState({ open1: true });
    console.log(this.state.open1, 'open')
  };

  onCloseModal1 = () => {
    this.setState({ open1: false });
  };

  handleClose1 = () => {
    this.setState({ open1: false });
  }


  onOpenModal2 = (item) => {
    this.setState({ open2: true });
    console.log(this.state.open2, 'open')
    console.log(item.name, 'alim');
    this.setState({
      'idName': item.name
    });

  };

  onCloseModal2 = () => {
    this.setState({ open2: false });
  };

  handleClose2 = () => {
    this.setState({ open2: false });
  }

  onOpenModal11 = () => {
    this.setState({ open11: true });
    console.log(this.state.open11, 'open')
  };

  onCloseModal11 = () => {
    this.setState({ open11: false });
  };

  handleClose11 = () => {
    this.setState({ open11: false });
  }

  handleChange = panel => (event, expanded) => {
    console.log('event', event);
    console.log('expanded', expanded);
    console.log('panel', panel);


    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  handleCheckbox1 = pro => {
    console.log('kyahaibe', pro.name)
    console.log('view')
    // console.log('foo', foo)




    // let recipesCopy = [...this.state.permissionss]
    // //make changes to ingredients
    // recipesCopy[foo].modules = pro.name
    // recipesCopy[foo].code = 'view'
    // console.log(recipesCopy, 'permisiondone')

    // this.setState({
    //   permissionss: recipesCopy
    // })



    // const newFile = this.state.permissionss.map((file) => {
    //   return { ...file.modules, modules: pro.name };
    // });
    // console.log(newFile, 'permision')

    // this.setState({ permissionss: newFile });

    let newl = { module: pro.name, code: 'view' }
    let datal = [...this.state.permissionss, newl]
    this.setState({
      permissionss: datal
    })
    console.log(this.state.permissionss, 'permision')

    newl = { module: pro.name, code: 'add' }
    datal = [...this.state.permissionss, newl]
    this.setState({
      permissionss: datal
    })
    console.log(this.state.permissionss, 'permision')

    // foo++;

    // let recipesCopy = this.state.permissionss
    //make changes to ingredients
    // recipesCopy[foo].modules = pro.name
    // recipesCopy[foo].code = 'add'
    // console.log(recipesCopy, 'permisiondone')

    // this.setState({
    //   permissionss: recipesCopy
    // })


    // console.log(this.state.permissionss, 'permision')


    // foo++;



  }
  handleCheckbox2 = pro => {
    console.log('kyahaibe', pro.name)
    console.log('edit')
    // console.log('foo', foo)


    let newl = { module: pro.name, code: 'edit' }
    let datal = [...this.state.permissionss, newl]
    this.setState({
      permissionss: datal
    })
    console.log(this.state.permissionss, 'permision')
  }
  handleCheckbox3 = pro => {
    let newl = { module: pro.name, code: 'delete' }
    let datal = [...this.state.permissionss, newl]
    this.setState({
      permissionss: datal
    })
    console.log(this.state.permissionss, 'permision')
  }
  handleChanges = event => {
    // console.log('salimhan',event.target.value);
    // const uniqueNames = Array.from(new Set(this.state.arr));
    const uniqueNames = Array.from(new Set(this.state.arr));


    console.log('salimhan', event.target.name);

    if (event.target.name === 'Admin') {
      this.setState({
        admin: !this.state.admin,
        arr: this.state.arr.concat('Admin'),
      });
      console.log('khan', this.state.admin);
      console.log('khan', this.state.arr);
      console.log('khann', uniqueNames);



    }
    if (this.state.admin === false) {
      // var people = ["Bob", "Sally", "Jack"]
      // var toRemove = 'Admin';
      if (uniqueNames) {
        var index = uniqueNames.indexOf("Admin");
        if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
          uniqueNames.splice(index, 1);
        }
        this.setState({
          uniqueNames: uniqueNames,
        });
      }

      console.log('bla bla', this.state.uniqueNames);
      return true;
    }
    if (event.target.name === 'Manager') {
      this.setState({
        manager: !this.state.manager,
        arr: this.state.arr.concat('Manager'),
      });
      console.log('khan', this.state.manager);
      console.log('khan', this.state.arr);
    }
    if (this.state.manager === false) {
      // var people = ["Bob", "Sally", "Jack"]
      // var toRemove = 'Manager';
      if (uniqueNames) {
        var index = uniqueNames.indexOf("Manager");
        if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
          uniqueNames.splice(index, 1);
        }
        this.setState({
          uniqueNames: uniqueNames,
        });

      }
      console.log('bla bla', this.state.uniqueNames);
      return true;


    }

    if (event.target.name === 'Employee') {
      this.setState({
        employee: !this.state.employee,
        arr: this.state.arr.concat('Employee'),
      });
      console.log('khan', this.state.employee);
      console.log('khan', this.state.arr);
    }
    if (this.state.employee === false) {
      // var people = ["Bob", "Sally", "Jack"]
      // var toRemove = ;
      if (uniqueNames) {
        var index = uniqueNames.indexOf("Employee");
        if (index > -1) { //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
          uniqueNames.splice(index, 1);
          this.setState({
            uniqueNames: uniqueNames,
          });
        }
      }
      console.log('bla bla', this.state.uniqueNames);
      return true;


    }

  };

  deletett = () => {
    console.log('delete', this.state.uniqueNames);
    console.log('delete click hu raha hai');

  }


  setInputValue = (data) => {

    var input = {
      name: data.name,
      'images': data.image,
      'category': data.category.id,
      'image_type': data.image_type.id,
    }
    this.setState({
      editItem: input,
      deleteUniform: data.uniformImageId
    });
    console.log(data.uniformImageId, 'datato')
  };


  getData()
  {

  }

toggleSelectAllLead(){
let newSelected = {};

    if (this.state.selectAll === 0) {
      this.state.getRole.forEach(x => {
        newSelected[x.id] = true;
      });
      console.log(newSelected,'newSelected');
    }
    this.setState((prevState, props) => ({
      selected: newSelected,
      selectAll: this.state.selectAll === 0 ? 1 : 0
    }),()=>{
      var total_task= [];
      for (const prop in newSelected) {
        if (newSelected.hasOwnProperty(prop)) {
          total_task.push(`${prop}`);
          console.log(`${prop}` ,total_task,'ids',prop);
          this.state.id = total_task;
        }
      }
      console.log(this.state.selected,total_task,'selected')
    })

}



  toggleRowLead(id) {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[id] = !this.state.selected[id];
    console.log(newSelected[id],'new')
    // console.log(newSelected);
    this.setState((prevState, props) => ({
      selected: newSelected,
      selectAll: 2
    }),()=>{
      var totle_task= [];
      for (const prop in this.state.selected) {
        if (this.state.selected.hasOwnProperty(prop)) {
          totle_task.push(`${prop}`);
          console.log(totle_task,'totaltask')
          console.log(totle_task.map(Number));
          this.state.task_list = totle_task.map(Number);
          console.log(this.state.task_list,"togglerowlead");
        }
      }
      console.log(this.state.selected,'selected', )
    })

  }


    toggleRowLeadModal(id) {
    const newSelected = Object.assign({}, this.state.selectedModal);
    newSelected[id] = !this.state.selectedModal[id];
    console.log(newSelected[id],'new')
    // console.log(newSelected);
    this.setState((prevState, props) => ({
      selectedModal: newSelected,
      selectAll: 2
    }),()=>{
      var totle_task= [];
      for (const prop in this.state.selectedModal) {
        if (this.state.selectedModal.hasOwnProperty(prop)) {
          totle_task.push(`${prop}`);
          console.log(totle_task,'totaltask')
          console.log(totle_task.map(Number));
          this.state.task_list = totle_task.map(Number);
          console.log(this.state.task_list,"togglerowlead");
        }
      }
      console.log(this.state.selectedModal,'selected', )
    })

  }


  toggleRowLeadModal1(id) {
    const newSelected = Object.assign({}, this.state.selectedModal1);
    newSelected[id] = !this.state.selectedModal1[id];
    console.log(newSelected[id],'new')
    // console.log(newSelected);
    this.setState((prevState, props) => ({
      selectedModal1: newSelected,
      selectAll: 2
    }),()=>{
      var totle_task= [];
      for (const prop in this.state.selectedModal1) {
        if (this.state.selectedModal1.hasOwnProperty(prop)) {
          totle_task.push(`${prop}`);
          console.log(totle_task,'totaltask')
          console.log(totle_task.map(Number));
          this.state.task_list = totle_task.map(Number);
          console.log(this.state.task_list,"togglerowlead");
        }
      }
      console.log(this.state.selectedModal1,'selected', )
      this.toggleRowLeadModal(id);
    })

  }


  toggleRowLeadModal2(id){
const newSelected = Object.assign({}, this.state.selectedModal2);
    newSelected[id] = !this.state.selectedModal2[id];
    console.log(newSelected[id],'new')
    // console.log(newSelected);
    this.setState((prevState, props) => ({
      selectedModal2: newSelected,
      selectAll: 2
    }),()=>{
      var totle_task= [];
      for (const prop in this.state.selectedModal2) {
        if (this.state.selectedModal2.hasOwnProperty(prop)) {
          totle_task.push(`${prop}`);
          console.log(totle_task,'totaltask')
          console.log(totle_task.map(Number));
          this.state.task_list = totle_task.map(Number);
          console.log(this.state.task_list,"togglerowlead");
        }
      }
      console.log(this.state.selectedModal2,'selected', )
      this.toggleRowLeadModal(id);
    })
  }


  render() {
    const classes = this.state;
    const { open1 } = this.state;
    const { open2 } = this.state;
    const { open11 } = this.state;
    const { myFile } = this.state;
    //console.log(this.state.selectedModal,this.state.selectedModal1,this.state.selectedModal2, 'testing')

    const { categories, units, expiries, editItem, expanded } = this.state
    if (this.state.showLoader) {
      return (
        < CircularProgress className='loader' />
      )
    }

    var addnew;
    if (this.state.addNew) {
      addnew =
        <Paper style={{ marginTop: '15px', marginBottom: '10px' }} >
          <Grid container spacing={16}  >

            <Grid item xs={3}>

              <TextField
                label="Role"
                margin="normal"
                name="role"
                value={editItem.role}
                onChange={this.editInputValue}

              />
            </Grid>

            <Grid item xs={9}>
              <TextField
                style={{ width: '90%' }}
                // id="outlined-multiline-static"
                label="Description"
                // multiline
                // rows="1"
                defaultValue=""
                className={classes.textField}
                margin="normal"
                name="description"
                onChange={this.editInputValue}

              // variant="outlined"
              />
            </Grid>
            <Table className={classes.table} size="small" style={{ marginTop: "20px" }}>
              {/* <SvgMaterialIcons /> */}
              <TableHead >
                <TableRow>
                  <TableCell align='center' style={{fontSize:'18px'}}>Module</TableCell>
                  <TableCell align="center" style={{fontSize:'18px'}}>View</TableCell>
                  <TableCell align="center" style={{fontSize:'18px'}}>Edit</TableCell>
                  <TableCell align="center" style={{fontSize:'18px'}}>Delete</TableCell>

                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.moduleList.map(row => (
                  <TableRow key={row.modulename}>
                    <TableCell align='center' component="th" scope="row">
                      {/* <Checkboxes />  */}
                      {/* <input type="checkbox"  name="vehicle1" value="Bike" style={{marginTop:'10px',width:'20px', height:'15px'}} >
</input> */}

                      <span>{row.modulename}</span>

                    </TableCell>

                    <TableCell align="center"><Checkbox checked={this.state.selectedModal[row.id] === true} onChange={() => this.toggleRowLeadModal(row.id)}  ></Checkbox></TableCell>
                    <TableCell align="center"><Checkbox  checked={this.state.selectedModal1[row.id] === true} onChange={() => this.toggleRowLeadModal1(row.id)}>
                          </Checkbox></TableCell>
                    <TableCell align="center"><Checkbox checked={this.state.selectedModal2[row.id] === true} onChange={() => this.toggleRowLeadModal2(row.id)}>
                          </Checkbox></TableCell>

                  </TableRow>
                ))}
              </TableBody>
            </Table>





          </Grid>

          <Divider style={{ marginTop: '50px' }} />
          <ExpansionPanelActions className='expansionpanelactions' >
            <Button size="small" onClick={() => this.clearFunction()} >Cancel</Button>
            <Button size="small" color="primary" onClick={() => this.createUniformCategory()} >
              Create
            </Button>
          </ExpansionPanelActions>

        </Paper>
    } else {
      addnew = null;
    }

    return (
      <div>
      <Grid container>

            <Grid item lg={10} md={9} sm={8} xs={7}></Grid>
            <Grid item lg={2} md={3} sm={4} xs={5} >
              <div style={{marginTop:'-45px'}}>
                <UploadButton uploadname="inventory" disable={true} getData={this.getData.bind(this)}/>

              </div>
            </Grid>
          </Grid>

        {addnew}
        {/* <p>{this.props.searchresult}</p> */}
        <div className={classes.root}>
          <Paper className={classes.paper}>
            <Table className={classes.table} size="small" style={{ marginTop: "20px" }}>
              {/* <SvgMaterialIcons /> */}
              <TableHead>
                <TableRow>
                <TableCell>{/*<input type="checkbox"  name="allinput" //checked={this.state.checkedItems.get(row.name)}
                                 //onChange={this.handleChanges}
                                 style={{marginTop:'10px',width:'20px', height:'15px'}} >

</input>*/}

<Checkbox onChange={() => this.toggleSelectAllLead()}>
</Checkbox>
 </TableCell>
                  <TableCell align='left' style={{fontSize:'18px'}}>Role</TableCell>
                  <TableCell align="center" style={{fontSize:'18px'}}>Discription</TableCell>

                  <span onClick={() => this.deletett()}>
                    <Grid open={open2} onClose={this.onCloseModal2} >
                      <div className='delt'>
                        <SvgMaterialIcons />
                      </div>
                    </Grid>
                  </span>



                  {/* <span className='delete-iconss'><SvgMaterialIcons  /></span> */}
                  {/*<TableCell align="center">Password</TableCell>
                    <TableCell align="center">Created Date</TableCell>
                    <TableCell align="center">Role</TableCell>*/}
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.getRole.map(row => (
                  <TableRow key={row.name}>
                  <TableCell>  <Checkbox checked={this.state.selected[row.id] === true} onChange={() => this.toggleRowLead(row.id)}>
</Checkbox>        </TableCell>
                
                    <TableCell className="tableContent" align='left'>
                      {/* <Checkboxes />  */}
                                                <span  onClick={() => { this.onOpenModal2(row) }}>{row.name}</span>


                    </TableCell>
                    <TableCell className="tableContent" align="center">{row.calories}</TableCell>
                    {/*<TableCell align="center">{row.fat}</TableCell>
                      <TableCell align="center">{row.carbs}</TableCell>
                      <TableCell align="center">{row.protein}</TableCell>*/}
                  </TableRow>
                ))}
              </TableBody>
            </Table>

          </Paper>
        </div>
        <div className={classes.modalmargin}>
          <Modal open={open2} onClose={this.onCloseModal2} center style={{ marginTop: "50px !important" }}>
            <div className={classes.root} style={{ marginTop: '10px' }}>
              <Grid container spacing={16}
                style={{ marginTop: '50px', marginBottom: '10px',  width: '500px' }}>
                <Grid item xs={12}>
                  <Grid item xs={12}>

                    <h4 style={{paddingLeft:'10px'}}>{this.state.idName}</h4>
                    <hr />
                  </Grid>

                  <Grid item xs={12}>

                    {/*{this.state.description}*/}

                  </Grid>


                  <Table className={classes.table} size="small" style={{ marginTop: "20px" }}>
                    {/* <SvgMaterialIcons /> */}
                    <TableHead>
                      <TableRow>
                        <TableCell align='left' style={{fontSize:'16px'}}>Module</TableCell>
                        <TableCell align="center" style={{fontSize:'16px'}}>View</TableCell>
                        <TableCell align="center" style={{fontSize:'16px'}}>Edit</TableCell>
                        <TableCell align="center" style={{fontSize:'16px'}}>Delete</TableCell>

                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {this.state.moduleList.map(row => (
                        <TableRow key={row.id}>
                          <TableCell align='left' component="th" scope="row">
                            {/* <Checkboxes />  */}
                            {/* <input type="checkbox"  name="vehicle1" value="Bike" style={{marginTop:'10px',width:'20px', height:'15px'}} >
</input> */}

                            <span className="moduleNames">{row.modulename}</span>

                          </TableCell>

                          <TableCell align="center"><Checkbox checked={this.state.selectedModal[row.id] === true} onChange={() => this.toggleRowLeadModal(row.id)}  >
                          </Checkbox></TableCell>
                          <TableCell align="center"><Checkbox  checked={this.state.selectedModal1[row.id] === true} onChange={() => this.toggleRowLeadModal1(row.id)}>
                          </Checkbox></TableCell>
                          <TableCell align="center"><Checkbox checked={this.state.selectedModal2[row.id] === true} onChange={() => this.toggleRowLeadModal2(row.id)}>
                          </Checkbox></TableCell>

                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>


                  <DialogActions>
                    <Button onClick={this.handleClose2} color="primary">
                      Cancel
                                    </Button>
                    <Button color="primary">
                      Create
                                    </Button>
                  </DialogActions>

                </Grid>
              </Grid>
            </div>
          </Modal>
        </div>
      </div>
    )
  }
}
export default Users
