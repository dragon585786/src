//jshint esversion:6
import React, { Component, Fragment } from 'react'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import classNames from 'classnames';
import '../Rawmaterials/Rawmaterials.css';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { defaultServerAddr } from '../../../NetworkConfig.js';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import '../Customers/Customers.css';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import { red } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import DatePicker from "react-datepicker";
import { serverip } from '../../../NetworkConfig.js';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import UploadButton from '../UploadButton/UploadButton'

const access = window.localStorage.getItem('access');


const styles = theme =>  ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
        '& p':{
          color:'red',
        },
      },
  });


const ranges1 = [
    {
      value: 'Male',
      label: 'Male',
    },
    {
      value: 'Female',
      label: 'Female',
    },
    {
      value: 'Other',
      label: 'Other',
    },
  ];

class Customers extends Component {

    state = {
        items: [],
        showLoader: true,
        expanded: null,
        editItem: {
            "personId": null,
            "firstName": "",
            "lastName": "",
            "dob": "",
            "doa": "",
            "gender": "",
            "emailId": " ",
            "contact1": " ",
            "contact2": "",
            "referenceName": " ",
            "referencePhone": " ",
            "fieldBusiness": " ",
            "address1": " ",
            "address2": " ",
            "area": " ",
            "city": "  ",
            "pincode": " ",
            "notes": " ",
            "state": " ",
            "country": "  ",

        },
        lnameerror:'',
        emailerror:'',
        phone1error:'',
        pincerror:'',
        phone2error:'',
        fnameerror:'',
        lnameerror:'',
        refperror:'',
        units: [],
        expiries: [],
        searchValue: this.props.searchresult.searchvalue,
        addNew: this.props.searchresult.createNew,
        selectedDate:new Date(),
        max:5,
        pages:1,
        f1name:'',
            l1name:'',
            dob1:'',
            doa1:'',
            gender1:'',
            email1:'',
            primecon1:'',
            altcon1:'',
            refname1:'',
            refphone1:'',
            fieldbus:'',
            add1:'',
            add2:'',
            area1:'',
            city1:'',
            poscode:'',
            note1:'',
            state1:'',
            country1:'',
            getcust:[],
            datePick: new Date(),

    }
    componentDidMount() {
        this.getCustomer();
        this.handleChange();

    }







    clearFunction(){
        this.setState({ addNew: false });
      }

    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };
    setInputValue = (data) => {
        var input = {
            // "ingredientId": data.ingredientId,
            // "ingredientName": data.ingredientName,
            // "category": data.category.id,
            // "unit": data.unit.id,
            // "price": data.price,
            // "expiry": {
            //     "duration": data.expiry.duration,
            //     "duration_term": data.expiry.duration_term
            // }
        }
        this.setState({ editItem: input });
    };

    // editInputValue = event => {





    //     let editItem = this.state.editItem;





    //     editItem[event.target.name] = event.target.value;
    //     this.setState({
    //         editItem
    //     });

    //     console.log('state', this.state.editItem);

    // }


    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps', nextProps);
        if (nextProps.searchresult.createNew) {
            this.setState({ addNew: nextProps.searchresult.createNew })

        }
        else {
            this.setState({ searchValue: nextProps.searchresult.searchvalue, })
            // this.getAllCustomers();
        }

    }


    buttnext = () => {
        //   console.log('salimm');
        if(this.state.page === 10){
          console.log('next end here');
          return true;
        }
        else{

          console.log('next plus one');
          this.setState({
            pages: this.state.pages + 1
          });
        };
      }

      buttprevious = () => {
        //   console.log('salimm');

        if(this.state.pages === 1){
          console.log('previous button end here');

          return true;
        }
        else{
          console.log('previous minus one hua');
          this.setState({
            pages: this.state.pages - 1
          });
        }

      }


      createCustomer = event => {
          console.log('apihit',this.state.firstName);
        var datas={
            "first_name":this.state.firstName,
            "last_name":this.state.lastName,
            "date_of_birth":this.state.dob,
            "date_of_anniversary":this.state.doa,
            "gender":this.state.gender,
            "email_id":this.state.emailId,
            "primary_contact":this.state.contact1,
            "alternate_contact":this.state.contact2,
            "reference_name":this.state.referenceName,
            "reference_phone":this.state.referencePhone,
            "field_business":this.state.fieldBusiness,
            "address1":this.state.address1,
            "address2":this.state.address2,
            "area":this.state.area,
            "city":this.state.city,
            "postal_code":this.state.pincode,
            "note":this.state.note,
            "state":this.state.state,
            "country":this.state.country,
        }
        defaultServerAddr.post(`${serverip}/client/create/customer/`,datas,{
          headers: {

            Accept: "application/json, text/plain, */*",
          'Content-Type': "application/json;",
          "Authorization" : `Bearer ${access}`
        },
    })
       .then(res => {
        console.log('exp4', res);


          this.clearFunction();

      });


    }

    getCustomer(){
      const datas8 = {

      }
      defaultServerAddr.get(`${serverip}/client/read/customer/`, datas8 , {
          headers: {
          Accept: "application/json, text/plain, */*",
          'Content-Type': "application/json;",
          "Authorization" : `Bearer ${access}`
          },
      })
      .then(res => {
        const getcust = res.data.data;
        this.setState({
          getcust
        })
        console.log(this.state.getcust,'custd');
      })


    }

    postItem = (event,type) => {

        event.preventDefault();
        let editItem = this.state.editItem;
        const Key = event.target.name;
        const value = event.target.value;

        if (Key == 'firstName') {
            this.setState({
              firstName:event.target.value,
            });
            return;
          }
         console.log('apihit2',this.state.firstName)
          if (Key == 'lastName') {
            this.setState({
              lastName:event.target.value,
            });
            return;
          }

          if (Key == 'gender') {
            this.setState({
              gender:event.target.value,
            });
            return;
          }

          if (Key == 'emailId') {
            this.setState({
              emailId:event.target.value,
            });
            return;
          }

          if (Key == 'contact1') {
            this.setState({
              contact1:event.target.value,
            });
            return;
          }

          if (Key == 'contact2') {
            this.setState({
              contact2:event.target.value,
            });
            return;
          }

          if (Key == 'referenceName') {
            this.setState({
              referenceName:event.target.value,
            });
            return;
          }

          if (Key == 'referencePhone') {
            this.setState({
              referencePhone:event.target.value,
            });
            return;
          }

          if (Key == 'fieldBusiness') {
            this.setState({
              fieldBusiness:event.target.value,
            });
            return;
          }

          if (Key == 'address1') {
            this.setState({
              address1:event.target.value,
            });
            return;
          }

          if (Key == 'address2') {
            this.setState({
              address2:event.target.value,
            });
            return;
          }

          if (Key == 'area') {
            this.setState({
              area:event.target.value,
            });
            return;
          }

          if (Key == 'city') {
            this.setState({
              city:event.target.value,
            });
            return;
          }

          if (Key == 'pincode') {
            this.setState({
              pincode:event.target.value,
            });
            return;
          }

          if (Key == 'notes') {
            this.setState({
              notes:event.target.value,
            });
            return;
          }

          if (Key == 'state') {
            this.setState({
              state:event.target.value,
            });
            return;
          }

          if (Key == 'country') {
            this.setState({
              country:event.target.value,
            });
            return;
          }

          if(Key == 'dob'){
            this.setState({
              dob:event.target.value,
            });
            return;
          }


          if(Key == 'doa'){
            this.setState({
              doa:event.target.value,
            });
            return;
          }

        editItem[event.target.name] = event.target.value;
        this.setState({
            editItem
        });

        if(type == 'fname'){
          let fregex=/^[A-Za-z]+$/;
          var fname=this.state.editItem.firstName;
          console.log(fname,'fNAME');
          // if((fname >64  && fname < 91) || (fname > 96 && fname < 123) ){
              if(fname.match(fregex)){
              this.setState({
                  fnameerror:""
              });

          }else{
              this.setState({
                  fnameerror:"Firstname not allowed"
              });
              console.log('fnameer',this.state.fnameerror);
          }

          return;
        }




        // this.setState({
        //     f1name:this.state.editItem.firstName,
        //     l1name:this.state.editItem.lastName,
        //     dob1:this.state.editItem.dob,
        //     doa1:this.state.editItem.doa,
        //     gender1:this.state.editItem.gender,
        //     email1:this.state.editItem.emailId,
        //     primecon1:this.state.editItem.contact1,
        //     altcon1:this.state.editItem.contact2,
        //     refname1:this.state.editItem.referenceName,
        //     refphone1:this.state.editItem.referencePhone,
        //     fieldbus:this.state.editItem.fieldBusiness,
        //     add1:this.state.editItem.address1,
        //     add2:this.state.editItem.address2,
        //     area1:this.state.editItem.area,
        //     city1:this.state.editItem.city,
        //     poscode:this.state.editItem.pincode,
        //     note1:this.state.editItem.note,
        //     state1:this.state.editItem.state,
        //     country1:this.state.editItem.country,

        // })


       if(type == 'lname'){
        let fregex=/^[A-Za-z]+$/;
        var lname=this.state.editItem.lastName;
        console.log(lname,'lNAME');
        // if((fname >64  && fname < 91) || (fname > 96 && fname < 123) ){
            if(lname.match(fregex)){
            this.setState({
                lnameerror:""
            });

        }else{
            this.setState({
                lnameerror:"Lastname not allowed"
            });
            console.log('fnameer',this.state.lnameerror);
        }
         return;
       }

       if(type == 'email'){
        let email = this.state.editItem.emailId;
        console.log(email,'valid')
        if(email.indexOf("@") === -1 ){
            this.setState({
                emailerror:"Not a valid E-mail ID"
            });
           }else{
               this.setState({
                   emailerror:""
               });

           }
         return;
       }



       if(type == 'pcon1'){
        let phoneno = /^\d{10}$/;
        let phone1 = this.state.editItem.contact1;
        if(phone1.match(phoneno)){
            this.setState({
                phone1error:""
            });
            console.log(this.state.editItem.contact1,'phone1');
        }else{
            this.setState({
                phone1error:"Not a valid phone number"
            });

        }
         return;
       }


        if(type == 'pcon2'){
          let phoneno2 = /^\d{10}$/;
            let phone2 = this.state.editItem.contact2;
            if(phone2.match(phoneno2)){
                this.setState({
                    phone2error:""
                });
                console.log(this.state.editItem.contact2,'phone2')
            }else{
                this.setState({
                    phone2error:"Not a valid phone number"
                });
            }
          return;
        }

        if(type == 'phone1'){
          let phoneno3 = /^\d{10}$/;
          let phone3 = this.state.editItem.referencePhone;
          if(phone3.match(phoneno3)){
              this.setState({
                  refperror:""
              });

              console.log(this.state.editItem.referencePhone,'ref1');
          }else{
              this.setState({
                  refperror:"Not a valid phone number"
              });
          }
          return;
        }


        if(type == 'pinc'){
          let pinc = /^[1-9][0-9]{5}$/;
          let pincode1 = this.state.editItem.pincode;
          if(pincode1.match(pinc)){
              this.setState({
                pincerror:""

              });

          }else{
              this.setState({
                  pincerror:"Pincode not valid"
              });
          }
          return;
        }


        this.createCustomer();

   }


    // readIngredientCategories() {
    //     defaultServerAddr.get('web/read/ingredient/category/')
    //         .then(res => {
    //             if (res.status) {
    //                 // console.log('web/read/ingredient/category//', res);
    //                 this.setState({
    //                     categories: res.data['data']
    //                 })

    //             } else {
    //                 alert(res.data.validation);
    //             }
    //         })

    // }

    // readIngredientUnits() {
    //     defaultServerAddr.get('web/read/ingredient/unit/')
    //         .then(res => {
    //             if (res.data.status) {
    //                 // console.log('web/read/ingredient/unit/', res);
    //                 this.setState({
    //                     units: res.data['data']
    //                 })
    //             } else {
    //                 alert(res.data.validation);
    //             }
    //         })
    // }


    handleChange2(date){
        this.setState({
          selectedDate: date
        });
      }


      handleDateChange = date =>{
        console.log(new Date(), date,'date')
        this.setState({ startDate: date });
      }


    // readExpiryDropdown() {
    //     defaultServerAddr.get('web/read/expiry-dropdown/')
    //         .then(res => {
    //             console.log('exp', res)
    //             if (res) {
    //                 // console.log('web/read/expiry-dropdown/', res);
    //                 this.setState({
    //                     expiries: res.data['data']
    //                 })
    //             } else {
    //                 alert(res.data.validation);
    //             }
    //         })
    // }
    // getAllCustomers() {
    //     var data = {
    //         "search_string": this.state.searchValue,
    //         "pagel": "",
    //         "entriles_per_page": ""
    //     }
    //     console.log('search data', data);
    //     this.setState({
    //         showLoader: true
    //     })
    //     defaultServerAddr.post('web/read/client/all/', data)
    //         .then(res => {
    //             if (res.data.status) {
    //                 console.log('web/read/ingredient/all/', res);
    //                 this.setState({
    //                     items: res['data']['data'],
    //                     showLoader: false
    //                 })
    //             } else {
    //                 alert(res.data.validation);
    //             }
    //         });
    // }

    getData()
{

}


    render() {

       const classes=this.state;
        const { categories, units, expiries, editItem, expanded,firstName,lastName,getcust } = this.state

             var addnew;
        if (this.state.addNew) {
            addnew =
            <form onSubmit={(event) => this.postItem(event)}>
                <Paper style={{ marginTop: '15px', marginBottom: '10px' }} >

                    <Grid container spacing={16}  >

                        <Grid item xs={3}>

                            <TextField
                                style={{width:'80%'}}
                                label="First name"
                                name="firstName"
                                value={this.state.firstName}
                                onChange={(event) => this.postItem(event,'fname')}

                            />
                            <div className="error1">{this.state.fnameerror}</div>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField
                            style={{width:'80%'}}
                                label="Last name"
                                name="lastName"
                                value={this.state.lastName}
                                onChange={(event) => this.postItem(event,'lname')}

                            /><div className="error1">{this.state.lnameerror}</div>
                        </Grid>
                        <Grid item xs={3}>

                         <TextField
                        style={{width:'80%'}}
        id="date"
        label="Date of Birth"
        type="date"
        defaultValue="2017-05-24"
        name="dob"
        value={this.state.dob}
        InputLabelProps={{
          shrink: true,
        }}
         onChange={(event) => this.postItem(event)}
      />
      </Grid>
                         <Grid item xs={3}><TextField
                           style={{width:'80%'}}
                           id="date"
                           label="Date of Anniversary"
                           type="date"
                           defaultValue="2017-05-24"
                           name="doa"
                           value={this.state.doa}
                           InputLabelProps={{
                           shrink: true,
                           }}
                           //onChange={(event) => this.postItem(event)}
                          /></Grid>
                        <Grid item xs={3}>
                        <TextField
                                style={{width:'80%'}}
                                label="EmailId"
                                name="emailId"
                                value={this.state.emailId}
                                onChange={(event) => this.postItem(event,'email')}
                                className={classes.textField}


                            />
                         <div className="error1">{this.state.emailerror}</div>
                        </Grid>
                        <Grid item xs={3}>
                        <TextField
        select
        label="Gender"
        style={{width:'80%'}}
        name="gender"
        value={this.state.gender}
        onChange={(event) => this.postItem(event)}
      >
        {ranges1.map(option => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
                        </Grid>
                        <Grid item xs={3}><TextField
                                style={{width:'80%'}}
                                type="number"
                                label="Primary Contact"
                                name="contact1"
                                value={this.state.contact1}
                                onChange={(event) => this.postItem(event,'pcon1')}

                            />
                             <div className="error1">{this.state.phone1error}</div></Grid>
                        <Grid item xs={3}><TextField
                                style={{width:'80%'}}

                                label="Alternate Contact"
                                name="contact2"
                                value={this.state.contact2}
                                onChange={(event) => this.postItem(event,'pcon2')}

                            />
                            <div className="error1">{this.state.phone2error}</div></Grid>

                        <Grid item xs={3}><TextField
               style={{width:'80%'}}
              label="Reference Name"
name="referenceName"
value={this.state.referenceName}
onChange={(event) => this.postItem(event)}


/></Grid>

<Grid item xs={3}><TextField
style={{width:'80%'}}
type="number"
label="Reference Phone"
name="referencePhone"
value={this.state.referencePhone}
onChange={(event) => this.postItem(event,'phone1')}
/>
<div className="error1">{this.state.refperror}</div></Grid>



<Grid item xs={3}>              <TextField
style={{width:'80%'}}
label="Country"
name="country"
value={this.state.country}
onChange={(event) => this.postItem(event)}
/></Grid>

<Grid item xs={3}><TextField
style={{width:'80%'}}
label="State"
name="state"
value={this.state.state}
onChange={(event) => this.postItem(event)}
/></Grid>

<Grid item xs={3}>            <TextField
style={{width:'80%'}}
label="City"
name="city"
value={this.state.city}
onChange={(event) => this.postItem(event)}

/></Grid>



<Grid item xs={3}><TextField
style={{width:'80%'}}
label="Business Field"
name="fieldBusiness"
multiline={true}
value={this.state.fieldBusiness}
onChange={(event) => this.postItem(event)}


/></Grid>

<Grid item xs={3}><TextField
style={{width:'80%'}}
label="Area"
name="area"
multiline={true}
value={this.state.area}
onChange={(event) => this.postItem(event)}

/></Grid>

<Grid item xs={3}><TextField
style={{width:'80%'}}
label="Pincode"
name="pincode"
type="number"
onChange={(event) => this.postItem(event,'pinc')}
value={this.state.pincode}

/>
<div className="error1">{this.state.pincerror}</div></Grid>
<Grid item xs={3}><TextField
style={{width:'80%'}}
label="Address 1"
name="address1"
rows={3}
rowsMax={6}
value={this.state.address1}
onChange={(event) => this.postItem(event)}

/></Grid>

<Grid item xs={3}><TextField
style={{width:'80%'}}
label="Address 2"
name="address2"
rows={3}
rowsMax={6}
value={this.state.address2}
onChange={(event) => this.postItem(event)}

/></Grid>

<Grid item xs={3}>              <TextField
 style={{width:'80%'}}
label="Notes"
name="notes"
multiline={true}
rows={3}
rowsMax={6}
value={this.state.notes}
onChange={(event) => this.postItem(event)}

/></Grid>







                        {/* <Grid item xs={2}>
                            <TextField
                                label="duration"
                                name="duration"
                                value={editItem.expiry.duration}
                                onChange={this.editInputValue}
                            />
                        </Grid>
                        <Grid item xs={2}>
                            <TextField
                                id="standard-select-currency"
                                select
                                label="duration term"
                                name='duration_term'
                                value={editItem.expiry.duration_term}
                                onChange={this.editInputValue}
                            >
                                {expiries.map(option => (
                                    <MenuItem key={option} value={option}>
                                        {option}
                                    </MenuItem>
                                ))}
                            </TextField>

                        </Grid> */}

                    </Grid>

                    <Divider style={{marginTop:'50px' }} />
                    <ExpansionPanelActions className='expansionpanelactions'  >
                        <Button size="small" onClick={() => this.clearFunction()}   >Cancel</Button>
                        <Button size="small" color="primary"  onClick={(event) => this.postItem(event)} >
                            Create
                                </Button>
                    </ExpansionPanelActions>

                </Paper>
                </form>
        } else {
            addnew = null;
        }

        return (
            <div>

            <Grid container >

            <Grid item lg={10} md={9} sm={8} xs={7}  ></Grid>
            <Grid item lg={2} md={3} sm={4} xs={5}>
              <div  style={{marginTop:'-45px'}}>
                <UploadButton uploadname="" disable={true} getData={this.getData.bind(this)}/>

              </div>
            </Grid>
          </Grid>
                {addnew}
                {this.state.getcust.map((item,k) => {
                  return (
                    <Paper className={classes.root} style={{marginTop:'20px',height:'600px'}}>
                <Grid container spacing={16}>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Name:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.first_name}&nbsp;{item.last_name}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Date of Birth:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}></Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Date of Anniversary:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}></Grid>

                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>E-mail:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.email_id}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Gender:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.gender}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Primary Contact:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.primary_contact}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Alternate Contact:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.alternate_contact}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Reference Name:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.reference_name}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Reference Phone:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.reference_phone}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Country:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.country}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>State:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.state}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>City:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.city}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Business Field:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.field_business}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Area:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.area}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Pincode:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.postal_code}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Address 1:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.address1}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Address 2:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.address2}</Grid>
                <Grid item xs={2} align="left" style={{padding:'15px'}}><b>Notes:</b></Grid>
                <Grid item xs={4} align="left" style={{padding:'15px'}}>{item.note}</Grid>
                <Grid item xs={8}></Grid>
                <Grid item xs={4} align="right" style={{float:"right",right:'7px',bottom:'7px',marginTop:'72px'}}>
                <Button size="small" color="primary"  >
            Update
          </Button>


          <Button size="small" color="secondary" >
            Delete
          </Button>
          </Grid>
                </Grid>

      </Paper>
                  );

                })}


                 {/* <p>{this.props.searchresult}</p>  */}
                {/* {this.state.items.map((prop, key) => {
                    return (
                        <ExpansionPanel key={key} expanded={expanded === key} onChange={this.handleChange(key)} >
                            <ExpansionPanelSummary onClick={() => this.setInputValue(prop)} style={{ marginTop: '10px' }} expandIcon={<Fab color="secondary" className='editbutton' aria-label="Edit" >
                             <Icon>edit_icon</Icon>
                            </Fab>} >




                            </ExpansionPanelSummary>
                            <Divider />

                            <ExpansionPanelDetails>
                                <div>






                                </div>
                            </ExpansionPanelDetails>
                            <Divider />
                             <ExpansionPanelActions className='expansionpanelactions' >
                                <Button size="small" onClick={() => this.handleChange(key)} >Delete</Button>
                                <Button size="small" color="primary"  >
                                    Update
                                </Button>
                            </ExpansionPanelActions>
                        </ExpansionPanel>





                    );
                })} */}
                <Grid item xs={6} style={{ marginTop: '15px',}}>
              <div className='butt'>
                {this.state.pages<=1 ? (null) : (<Button style={{marginLeft:'10px'}} onClick={this.buttprevious.bind(this)} variant="outline-primary">Previous</Button>)}
              </div>
              </Grid>
              <Grid item xs={6}  style={{float:'right' ,marginRight:'68px',
    marginTop: '33px'}}>
              <div>
                {this.state.pages>=this.state.max ? (null):(<span id="nam"><Button onClick={this.buttnext.bind(this)} id='possition' variant="outline-primary" style={{marginTop:'-20px'}} >Next</Button></span>)}

              </div>
              </Grid>
            </div>
        )
    }
}
export default Customers
