import React,{Fragment,Component} from 'react';


//import libraries

import axios from 'axios';

import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import {Grid,Paper} from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Popper from '@material-ui/core/Popper';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';


// Import Icons
import ArrowDownward from '@material-ui/icons/ArrowDownward'





//Import Components
import { ServerAddr } from '../../../NetworkConfig'




//import css files
import './UploadButton.css'



class UploadButton extends Component{
constructor(props)
{
  super(props);
  this.state={
          file:'',
          doneuploading:true,
          open:false,
          event1:''
          }

          // refs

          this._inputRef = React.createRef();
          this.button_ref = React.createRef();


          this.handleUpload = this.handleUpload.bind(this);
          this.handleFileInput = this.handleFileInput.bind(this);
          this.handleClose = this.handleClose.bind(this)
}



componentDidMount(){
  console.log("CDM is running");
}

handleUpload(event)
{
  event.preventDefault();
  this.setState({
    event1:event.target
  })
  this._inputRef.current.click();
  console.log("UploadButton post to link",this.props.uploadname);

}



async handleFileInput(event){

event.preventDefault();
this.setState({
  doneuploading:false
})
  const access = window.localStorage.getItem('access');
// var reader = new FileReader();

// var readdata =reader.readAsText(event.target.files[0]);
//
// console.log("ReadFileData",readdata);
// reader.onload = function(e) {
//     // Use reader.result
//     console.log("ReadFileData e",e.target.result);
//
//     }
      var formData  =new FormData();

      formData.set(this.props.uploadname,event.target.files[0])

        await  axios({
            method: 'post',
            url: `http://34.93.7.13:8002/${this.props.uploadname}/import/`,
            data: formData,
           headers: {'Authorization':`Bearer ${access}` ,
          'Content-Type': 'multipart/form-data' ,
         }
          })
          .then(response=> {
            //handle success
            console.log("Upload response ",response);
            console.log("getting Data in upload calling");
            this.setState({
              error_msg:''
            })
            this.props.getData();

          })
          .catch(error=> {
            //handle error

            this.setState({
              open:true
            })
                console.log("Upload Error Open is ",this.state.open);
                console.log('Upload Error',error.response);
              if(error.response.status === 406)
              {
                this.setState({
                  error_msg:error.response.data.exception
                })
              }
              else if(error.response.status === 422)
              {
                this.setState({
                  error_msg:error.response.data.exception
                })
              }  else if(error.response.status === 404)
                {
                  this.setState({
                    error_msg:"Not Found."
                  })
                }
              else {
                this.setState({
                  error_msg:''
                })
              }


              console.log("Upload error state after error ",this.state.error_msg);
                        });


          this.setState({
            doneuploading:true,

          })
console.log("crockery refreshfucntion");
          // this.props.refreshfucntion()

        }



handleClose(){

this.setState({
  open:false
})
}


download(e)
{
  e.preventDefault();

window.open(`http://34.93.7.13:8002/${this.props.uploadname}/export/`)


}



  render(){



    return(

      <Grid>



{this.state.doneuploading ? (  <Grid>


  <Button size="small" className="uploadbtn_UB" ref={this.button_ref} onClick={(e)=>{this.handleUpload(e)}} variant="contained" color="primary" disabled={this.props.disable}>
  Import
  </Button>
<Tooltip title="Get file format." aria-label="Get file format.">
  <Button  size="small" color="primary" variant="contained" className="Fab_UB" onClick={(e)=>{this.download(e)}} disabled={this.props.disable}>

   Export

  </Button>
</Tooltip>
  <input type="file" ref={this._inputRef} className="fileInput_UB" onChange={(event)=>{this.handleFileInput(event)}} ></input>
</Grid>) : (<Grid >

  <CircularProgress className="CircularProgress_css" />
</Grid>
)}

  <Dialog
  open={this.state.open}
  onClose={this.handleClose}>

  <DialogTitle id="alert-dialog-title">
    {"Error while uploading..."}
  </DialogTitle>
  <DialogContent>
    <DialogContentText id="alert-dialog-description">
      {this.state.error_msg}
    </DialogContentText>
  </DialogContent>
  <DialogActions>

    <Button onClick={this.handleClose} color="primary" variant="contained" autoFocus>
      Close
    </Button>
  </DialogActions>
</Dialog>

</Grid>

    )
  }
}


export default UploadButton;
