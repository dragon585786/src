import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper';
import './Login.css'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'
import { withRouter } from 'react-router-dom'
import { ServerAddr } from '../../../NetworkConfig'
import { BrowserRouter as Router, Route, Link, Redirect, Switch } from "react-router-dom";

// import {
//   Redirect,

// } from 'react-router-dom';




class Login extends Component {

  state = {
    name: 'catermunim',
    password: 'cater123',
    routeToDashoard: false

  };
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleChange = password => event => {
    this.setState({
      [password]: event.target.value,
    });
  };

  login = () => {

    // console.log('clicked login');
    // var data = {
    //     "username": "catermunium",
    //     "password": "random321"
    // }

    // ServerAddr.post('login/user/', data)
    //     .then(res => {
    //         console.log('res', res)
    //         if (res.data.status) {
    //             console.log('data', res);

    //             this.setState({
    //                 routeToDashoard: true
    //             })
    //             // this.context.router.history.push(`/dashboard`);

    //         } else {
    //             alert(res.data.validation);
    //         }
    //     })
    // admin

    // const datas ={
    //   "username":"catermunim",
    //   "password":"cater123"
    // }
    // console.log(datas,'username password')
    // console.log()
    ServerAddr.post('/auth/login/', {
      username: this.state.name,
         password: this.state.password,
      headers: {
        Accept: "application/json, text/plain, */*",
        'Content-Type': "application/json;",
        
        // "Authorization" : `Bearer ${access}`
      }
    })
    .then(res => {
      if(res.data.data.access){
        console.log('salimm',res.data)
        localStorage.setItem('access', res.data.data.access);
        localStorage.setItem('refresh', res.data.data.refresh);
        const access = window.localStorage.getItem('access');
        const refresh = window.localStorage.getItem('refresh');
        console.log("refresh", refresh,access);
        this.setState({
          routeToDashoard: true
        })
        // this.context.router.history.push(`/dashboard`);
      }else{
        alert(res.data.data);
      }
    })


  };

  //
  // login = () => {
  //   var datas = {
  //     username: "catermunium",
  //     password: "random321"
  //   };
  //   ServerAddr.post('auth/login/',  datas,{
  //     headers: {
  //       'Content-Type': "application/json;",
  //       'Access-Control-Allow-Origin' : '*',
  //    'Access-Control-Allow-Methods' : 'post',
  //     }})
  //     .then(res => {
  //       console.log('exp', res)
  //       if (res) {
  //
  //         this.setState({
  //           categories: res.data['data']
  //         })
  //         console.log(this.state.expiries,'sup')
  //       } else {
  //         alert(res.data.validation);
  //       }
  //     })
  //
  //
  // };



  render() {


    if (this.state.routeToDashoard === true) {
      return (
        <Redirect to='/dashboard' />
      )
    }


    return (
<span className="app-header">

      <div className='mainposition' >

        <Paper className='mainpaper' elevation={1}>
        <h4 className="innertexts">CATERMUNIM</h4>
          <Paper className='innerpaper' elevation={2}>
            <div>

              <div>
                <TextField
                  id="standard-name"
                  label="Username"

                  value={this.state.name}
                  onChange={this.handleChange('name')}
                  margin="normal"
                />
              </div>

              <div>
                <TextField
                  id="standard-password-input"
                  label="Password"

                  type="password"
                  autoComplete="current-password"
                  value={this.state.password}
                  onChange={this.handleChange('password')}
                  margin="normal"
                />
              </div>
              <div>
              <Link to="/resetPassword">Forgot password?</Link>
              </div>

              <Button className='martop' onClick={() => this.login()} variant="contained" color="primary" > Login </Button>


            </div>
          </Paper>
        </Paper>

      </div>
    </span>

    )
  }
}

export default Login



// "username": "jalgaon",
//             "password": "random321"
